package daytrader;

import daytrader.datamodel.*;
import daytradertasks.LoadRealtimeDataTask;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;

/**
 * Created by elken on 8/30/2016.
 */
public class RealtimeTest {
    String[] tickers = {
        "AAAP",
        "AAL",
        "AAME",
        "AAOI",
        "AAON",
        "AAPC",
        "AAPL",
        "AAWW",
        "AAXJ",
        "ABAC",
        "ABAX",
        "ABCB",
        "ABCD",
        "ABCO",
        "ABDC",
        "ABEO",
        "ABEOW",
        "ABIL",
        "ABIO",
        "ABMD",
        "ABTL",
        "ABTX",
        "ABUS",
        "ABY",
        "ACAD",
        "ACAS",
        "ACAT",
        "ACBI",
        "ACET",
        "ACFC",
        "ACGL",
        "ACHC",
        "ACHN",
        "ACIA",
        "ACIW",
        "ACLS",
        "ACNB",
        "ACOR",
        "ACPW",
        "ACRS",
        "ACRX",
        "ACSF",
        "ACST",
        "ACTA",
        "ACTG",
        "ACTS",
        "ACTX",
        "ACUR",
        "ACWI",
        "ACWX",
        "ACXM",
        "ADAP",
        "ADBE",
        "ADES",
        "ADHD",
        "ADI",
        "ADMA",
        "ADMP",
        "ADMS",
        "ADP",
        "ADRA",
        "ADRD",
        "ADRE",
        "ADRO",
        "ADRU",
        "ADSK",
        "ADTN",
        "ADUS",
        "ADVM",
        "ADXS",
        "ADXSW",
        "AEGN",
        "AEGR",
        "AEHR",
        "AEIS",
        "AEMD",
        "AEPI",
        "AERI",
        "AETI",
        "AEY",
        "AEZS",
        "AFAM",
        "AFH",
        "AFMD",
        "AFSI",
        "AGEN",
        "AGFS",
        "AGFSW",
        "AGII",
        "AGIIL",
        "AGIO",
        "AGLE",
        "AGNC",
        "AGNCB",
        "AGNCP",
        "AGND",
        "AGRX",
        "AGTC",
        "AGYS",
        "AGZD",
        "AHGP"};

    @Test
    public void RT_test() {
        List<Putup> putupList = new ArrayList<>();

        for (String ticker : tickers) {
            putupList.add(new Putup(ticker, PutupTypeEnum.LONGS, 13.37, AtrClassEnum.UU, false));
        }

        List<Callable<BarPointGraph>> callableList = new ArrayList<>();
        callableList.addAll(putupList.stream().map(LoadRealtimeDataTask::new).collect(Collectors.toList()));

        for (Putup putup : putupList) {
            try {
                BarPointGraph points = new BarPointGraph();
                points.setPutup(putup);
                GlobalPoints.getInstance().addGraph(points);
                BarPointGraph graph = new LoadRealtimeDataTask(putup).call();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
//        for (int i = 0; i < 10; i++) {
//            while (true) {
//                System.out.println(GlobalPoints.getInstance().getSize());
//                try {
//                    TimeUnit.SECONDS.sleep(5);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//        }
    }
}
