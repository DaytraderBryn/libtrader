package daytrader;

import daytrader.annotations.TWS;
import daytrader.api.DaytraderController;
import daytrader.api.exceptions.TWSException;
import daytrader.datamodel.*;
import daytrader.interfaces.IGraphLine;
import daytrader.utils.DTUtil;
import daytrader.utils.TimeRange;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import rules.GenerateCELine;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;

/**
 * beanemcbean providing you with the fresh MeiMei's.
 */
@Category(TWS.class)
public class GenerateCELineTest {

    BarPointGraph dataGraph;
    GenerateCELine<BarPoint> rule;

    public void setUp() throws Exception {
        Putup putup = new Putup("JO", PutupTypeEnum.LONGS, 25.63, AtrClassEnum.PA, true);

        LocalDate date = LocalDate.of(2017, 2, 23);
        LocalTime startTime = DTUtil.getExchOpeningLocalTime();
        LocalTime endTime = LocalTime.of(12, 0);

        try {
            DaytraderController.getInstance().connect();
        } catch (TWSException e) {
            AbstractDataRequest logger = null;
            logger.error(e);
        }
        dataGraph = DTUtil.generateHistoricPoints(putup, date, new TimeRange(startTime, endTime), false);
        date = date.minus(1, ChronoUnit.DAYS);
        dataGraph.addPreviousGraph(27062016, DTUtil.generateHistoricPoints(putup, date, new TimeRange(DTUtil.getExchOpeningLocalTime(), DTUtil.getExchClosingLocalTime()), false));
        dataGraph.setPrevDayClose(dataGraph.getPrevDayGraph().last());
        rule = new GenerateCELine<>();
    }

    @Test
    public void generateCELine() throws Exception {
        //Test area
        //Duration displacement = Duration.ofHours(24).minus(Duration.between(DTUtil.getExchOpeningLocalTime(), DTUtil.getExchClosingLocalTime()));

        //Test area
        setUp();
        Long startTime = System.nanoTime();
        if (dataGraph.size() >= 10) {
            System.out.println(dataGraph.size() + " bars");
            ArrayList<IGraphLine> graphLines = rule.generateCELine(dataGraph, LocalTime.of(10, 0));
            for (IGraphLine line : graphLines) {
                System.out.println(
                        "Elapsed time: " + ((System.nanoTime() - startTime)/1000000 ) + "ms \r\n" +
                        "C-Point: " + "Time: " + line.getCurrentC().getTime() + " High: " + line.getCurrentC().getHigh() + " Low: " + line.getCurrentC().getLow() + "\r\n" +
                        "E-Point: " + "Time: " + line.getCurrentE().getTime() + " High: " + line.getCurrentE().getHigh() + " Low: " + line.getCurrentE().getLow() + "\r\n" +
                        "Gradient: " + line.getGradient() + "\r\n" +
                        "Price at Lotd: " + line.getPriceAtTime(line.getGraph().getLowestPointSoFar().getTime()));
            }
        } else {
            System.out.println("not enough data :( only " + dataGraph.size() + " bars");
        }

    }

}
