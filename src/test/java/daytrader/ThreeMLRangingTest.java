package daytrader;

import com.ib.client.Types;
import daytrader.annotations.TWS;
import daytrader.api.DaytraderController;
import daytrader.api.exceptions.TWSException;
import daytrader.datamodel.*;
import daytradertasks.LoadHistoricDataTask;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import rules.RuleResult;
import rules.ThreeMLRanging;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Map;

import static org.junit.Assert.assertTrue;

@Category(TWS.class)
@RunWith(Parameterized.class)
public class ThreeMLRangingTest extends RangesParameterized {

    private static Logger logger = LogManager.getLogger();

    public ThreeMLRangingTest(String ticker, String type, double price, String putupClass, LocalDateTime endTime, boolean f, LocalTime threeMLEnterTime, LocalTime threeMLExitTime, LocalTime ftgEnterTime) {
        super(ticker, type, price, putupClass, endTime, f, threeMLEnterTime, threeMLExitTime, ftgEnterTime);
    }

    @Test
    public void ThreeML_test() {
        try {
            DaytraderController.getInstance().connect();
        } catch (TWSException e) {
            logger.error(e);
        }
        Putup putup = new Putup(ticker, PutupTypeEnum.valueOf(type), price, AtrClassEnum.valueOf(putupClass), f);
        RuleResult result = new RuleResult<>(false, null);
        BarPointGraph points = new BarPointGraph();
        boolean hasEntered = false;
        LocalTime enterTimeGuess = LocalTime.of(0, 0, 0);
        LocalTime exitTimeGuess = LocalTime.of(0, 0, 0);

        for (Map.Entry<LocalTime, Integer> entry : dayOfTimes.entrySet()) {
            LocalDateTime dateTime = LocalDateTime.of(endTime.toLocalDate(), entry.getKey());
            try {
                points.addAll(new LoadHistoricDataTask(putup, Types.DurationUnit.SECOND, dateTime, entry.getValue()).run(true));
            } catch (TWSException e) {
                logger.error(e);
            }
            ThreeMLRanging<BarPoint> threeMLRanging = new ThreeMLRanging<>();
            BarPointGraph graphPointsAdded = new BarPointGraph();
            graphPointsAdded.setPutup(putup);
            for (BarPoint point : points) {
                if (point.getDateTime() == dateTime) break;
                graphPointsAdded.add(point);
                result = threeMLRanging.isIn3MLRange(graphPointsAdded, putup.getPriceTimeTreeSet().last().getPriceTimeMap());

                if (result.isPassed() && !hasEntered && threeMLEnterTime != null) {
                    enterTimeGuess = graphPointsAdded.last().getTime();
                    hasEntered = true;
                } else if (hasEntered && !result.isPassed() && threeMLExitTime != null) {
                    exitTimeGuess = graphPointsAdded.last().getTime();
                    hasEntered = false;
                }
            }
        }

        if (threeMLEnterTime != null) {
            assertTrue(enterTimeGuess.equals(threeMLEnterTime));
        } else {
            assertTrue(enterTimeGuess.equals(LocalTime.of(0, 0, 0)));
        }

        if (threeMLExitTime != null) {
            assertTrue(exitTimeGuess.equals(threeMLExitTime));
        } else {
            assertTrue(exitTimeGuess.equals(LocalTime.of(0, 0, 0)));
        }
    }
}
