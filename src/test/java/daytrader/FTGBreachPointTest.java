package daytrader;

import com.ib.client.Types;
import daytrader.annotations.TWS;
import daytrader.api.DaytraderController;
import daytrader.api.exceptions.TWSException;
import daytrader.datamodel.*;
import daytradertasks.LoadHistoricDataTask;
import daytradertasks.LoadPrevDayClose;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import rules.FTGBreachPoint;
import rules.RuleResult;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Map;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@Category(TWS.class)
@RunWith(Parameterized.class)
public class FTGBreachPointTest extends RangesParameterized {

    private static Logger logger = LogManager.getLogger();

    public FTGBreachPointTest(String ticker, String type, double price, String putupClass, LocalDateTime endTime, boolean f, LocalTime threeMLEnterTime, LocalTime threeMLExitTime, LocalTime ftgEnterTime) {
        super(ticker, type, price, putupClass, endTime, f, threeMLEnterTime, threeMLExitTime, ftgEnterTime);
    }

    @Test
    public void FTG_test() {
        try {
            DaytraderController.getInstance().connect();
        } catch (TWSException e) {
            logger.error(e);
        }
        Putup putup = new Putup(ticker, PutupTypeEnum.valueOf(type), price, AtrClassEnum.valueOf(putupClass), f);
        BarPoint prevClose = null;
        RuleResult result = new RuleResult<>(false, null);
        BarPointGraph points = new BarPointGraph();

        try {
            prevClose = new LoadPrevDayClose(putup, endTime.toLocalDate()).run(true);
        } catch (TWSException e) {
            logger.error(e);
        }

        points.setPrevDayClose(prevClose);
        points.setPutup(putup);

        days:
        for (Map.Entry<LocalTime, Integer> entry : dayOfTimes.entrySet()) {
            LocalDateTime dateTime = LocalDateTime.of(endTime.toLocalDate(), entry.getKey());
            try {
                points.addAll(new LoadHistoricDataTask(putup, Types.DurationUnit.SECOND, dateTime, entry.getValue()).run(true));
            } catch (TWSException e) {
                logger.error(e);
            }

            FTGBreachPoint<BarPoint> ftgBreachPoint = new FTGBreachPoint<>();
            BarPointGraph graphPointsAdded = new BarPointGraph();
            graphPointsAdded.setPutup(putup);
            graphPointsAdded.setPrevDayClose(prevClose);
            for (BarPoint point : points) {
                if (point.getDateTime() == dateTime) break;
                graphPointsAdded.add(point);
                result = ftgBreachPoint.getFTGBreachPoint(graphPointsAdded);
                if (result.isPassed()) break days;
            }
        }

        if (f) {
            assertTrue(result.isPassed() && result.getPoints() == null);
        } else {
            if (ftgEnterTime == null) {
                assertFalse(result.isPassed());
            } else {
                assertTrue(result.isPassed() && result.getPoints().first().getTime().equals(ftgEnterTime));
            }
        }
    }
}
