package daytrader;

import daytrader.annotations.Slow;
import daytrader.api.DaytraderController;
import daytrader.api.exceptions.TWSException;
import daytrader.datamodel.AtrClassEnum;
import daytrader.datamodel.Putup;
import daytrader.datamodel.PutupTypeEnum;
import daytrader.utils.DTUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by elken on 10/09/2016.
 */
public class CacheHammer {
    private Logger logger = LogManager.getLogger();

    private String[] tickers = {
            "AAAP",
            "AAON",
            "ABAX",
            "ABDC",
            "ABEO",
            "ABIL",
            "ABMD",
            "ACAD",
            "ACET",
            "ACHC",
            "ACHN",
            "ACIW",
            "ACNB",
            "ACOR",
            "ACPW",
            "ACRS",
            "ACRX",
            "ACST",
            "ACTA",
            "ACTG",
            "ACUR",
            "ACXM",
            "ADAP",
            "ADBE",
            "ADHD",
            "ADMA",
            "ADMP",
            "ADMS",
            "ADRO",
            "ADTN",
            "ADUS",
            "ADVM",
            "ADXS",
            "AEGN",
            "AEGR",
            "AEIS",
            "AEMD",
            "AEPI",
            "AERI",
            "AEZS",
            "AFMD",
            "AGEN",
            "AGFS",
            "AGIO",
            "AGRX",
            "AGYS",
            "AIMT",
            "AIRM",
            "AIXG",
            "AKAM",
            "AKAO",
            "AKBA",
            "AKRX",
            "ALCO",
            "ALDR",
            "ALDX",
            "ALGN",
            "ALRM",
            "ALSK",
            "ALXN",
            "AMCN",
            "AMD",
            "AMRI",
            "ANCX",
            "ARAY",
            "ATSG",
            "ATVI",
            "AVAV",
            "AVHI",
            "AXAS",
            "AXDX",
            "CAFD",
            "EGHT",
            "FCCY",
            "FLWS",
            "IBM",
            "JOBS",
            "MSFT",
            "NVDA",
            "SHLM",
            "SRCE",
            "TWOU",
            "V",
            "XLF",
            "XLRN"
    };

    /**
     * The Putup list.
     */
    List<Putup> putupList = new ArrayList<>();

    /**
     * Setup the putup list and connect to TWS.
     */
    @Before
    public void setup() {
        for (String ticker : tickers) {
            putupList.add(new Putup(ticker, PutupTypeEnum.LONGS, 13.37, AtrClassEnum.UU, false));
        }

        try {
            DaytraderController.getInstance().connect();
        } catch (TWSException e) {
            logger.error(e);
        }
    }

    /**
     * Fill the cache with a day's bars for every ticker.
     */
    @Test
    @Category(Slow.class)
    public void FillWithAll() {
        for(Putup putup : putupList) {
            try {
                DTUtil.generateHistoricPoints(putup, LocalDate.of(2016, 4, 5), DTUtil.getExchTimes(), true);
            } catch (TWSException e) {
                logger.error(e);
            }
        }
    }

    /**
     * Fill the cache with a day's bars for the first ticker.
     */
    @Test
    public void FillWithOne() {
        try {
            DTUtil.generateHistoricPoints(putupList.get(0), LocalDate.of(2016, 4, 5), DTUtil.getExchTimes(), true);
        } catch (TWSException e) {
            logger.error(e);
        }
    }
}
