package daytrader;

import daytrader.utils.DTUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.runners.Parameterized;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Collection;
import java.util.TreeMap;

/**
 * Wrapper class to parameterize the data for tests
 */
public class RangesParameterized {

    private static Logger logger = LogManager.getLogger();

    static TreeMap<LocalTime, Integer> dayOfTimes = new TreeMap<LocalTime, Integer>() {{
        put(LocalTime.of(11, 30, 0), 7200);
        put(LocalTime.of(13, 30, 0), 7200);
        put(LocalTime.of(15, 30, 0), 7200);
        put(DTUtil.getExchClosingLocalTime(), 1800);
    }};

    String ticker;
    String type;
    double price;
    String putupClass;
    LocalDateTime endTime;
    LocalTime threeMLEnterTime;
    LocalTime threeMLExitTime;
    LocalTime ftgEnterTime;
    boolean f;

    public RangesParameterized(String ticker, String type, double price, String putupClass, LocalDateTime endTime, boolean f, LocalTime threeMLEnterTime, LocalTime threeMLExitTime, LocalTime ftgEnterTime) {
        this.ticker = ticker;
        this.type = type;
        this.price = price;
        this.putupClass = putupClass;
        this.endTime = endTime;
        this.f = f;
        this.ftgEnterTime = ftgEnterTime;
        this.threeMLEnterTime = threeMLEnterTime;
        this.threeMLExitTime = threeMLExitTime;
    }

    @Parameterized.Parameters(name = " {0} \t {4} \t {3} \t {2} ")
    public static Collection<Object[]> data() {
        String line;
        int lines = 0;
        try (BufferedReader reader = new BufferedReader(new FileReader(FTGBreachPointTest.class.getResource("/rangesData.csv").getFile()))) {
            while (reader.readLine() != null) lines++;
            reader.close();
        } catch (IOException e) {
            logger.error(e);
        }

        Object[][] array = new Object[lines][9];
        try (BufferedReader reader = new BufferedReader(new FileReader(FTGBreachPointTest.class.getResource("/rangesData.csv").getFile()))) {
            int current = 0;
            while ((line = reader.readLine()) != null) {
                String[] values = line.split(",");
                DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");
                array[current][0] = values[0];
                array[current][1] = values[1];
                array[current][2] = Double.valueOf(values[2]);
                array[current][3] = values[3];
                array[current][4] = LocalDateTime.parse(values[4], dateFormat);
                array[current][5] = Boolean.valueOf(values[5]);
                try {
                    array[current][6] = LocalTime.parse(values[6]);
                } catch (Exception ignored) {}
                try {
                    array[current][7] = LocalTime.parse(values[7]);
                } catch (Exception ignored) {}
                try {
                    array[current][8] = LocalTime.parse(values[8]);
                } catch (Exception ignored) {}
                current++;
            }
        } catch (IOException e) {
            logger.error(e);
        }

        return Arrays.asList(array);
    }
}
