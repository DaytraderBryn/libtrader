package daytrader;

import daytrader.annotations.NonTWS;
import daytrader.datamodel.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.TreeMap;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@Category(NonTWS.class)
public class PutupTest {
    private Putup putup;
    private static final double price = 35.60;

    @Before
    public void setUp() throws Exception {
        putup = new Putup("JCI", PutupTypeEnum.LONGS, price, AtrClassEnum.UU, false);
    }

    @Test
    public void isF() throws Exception {
        assertFalse(putup.isF());
    }

    @Test
    public void setF() throws Exception {
        putup.setF(true);
        assertTrue(putup.isF());
    }

    @Test
    public void generatePriceTimeMap() throws Exception {
        TreeMap<Double, LocalTime> priceTimeMap = putup.generatePriceTimeMap(price, false);
        assertTrue(priceTimeMap.containsKey(price));
        assertTrue(priceTimeMap.get(price).equals(LocalTime.of(9, 32, 0)));
        assertFalse(priceTimeMap.containsKey(35.45));
    }

    @Test
    // TODO: Implement this when method is implemented
    public void getEntryTime() throws Exception {
        assertTrue("Add this method", true);
    }

    @Test
    public void isActive() throws Exception {
        assertTrue(putup.isActive());
    }

    @Test
    public void setActive() throws Exception {
        putup.setActive(false);
        assertFalse(putup.isActive());
    }

    @Test
    public void getTickerCode() throws Exception {
        assertTrue(putup.getTickerCode().equals("JCI"));
    }

    @Test
    public void setTickerCode() throws Exception {
        putup.setTickerCode("IBM");
        assertTrue(putup.getTickerCode().equals("IBM"));
    }

    @Test
    public void getAtrClass() throws Exception {
        assertTrue(putup.getAtrClass() == AtrClassEnum.UU);
    }

    @Test
    public void setAtrClass() throws Exception {
        putup.setAtrClass(AtrClassEnum.PA);
        assertTrue(putup.getAtrClass() == AtrClassEnum.PA);
        assertTrue(AtrClassEnum.PA.order() == putup.getAtrClass().order());
    }

    @Test
    public void getMarket() throws Exception {
        assertTrue(putup.getMarket() == MarketEnum.SMART);
    }

    @Test
    public void setMarket() throws Exception {
        putup.setMarket(MarketEnum.NASDAQ);
        assertTrue(putup.getMarket() == MarketEnum.NASDAQ);
    }

    @Test
    public void getPriceTimeTreeSet() throws Exception {
        assertTrue(putup.getPriceTimeTreeSet().size() == 1);
        PriceTimeTree first = putup.getPriceTimeTreeSet().first();
        assertTrue(first.getPrice().equals(price));
        assertTrue(first.getPriceTimeMap().containsKey(price));
        assertTrue(first.getPriceTimeMap().get(price).equals(LocalTime.of(9, 32, 0)));
        assertFalse(first.isF());
    }

    @Test
    public void getTodaysDate() throws Exception {
        assertTrue(putup.getTodaysDate().isEqual(LocalDate.now()));
    }

    @Test
    public void setTodaysDate() throws Exception {
        putup.setTodaysDate(putup.getTodaysDate().plusDays(1));
        assertTrue(putup.getTodaysDate().isAfter(LocalDate.now()));
    }

    @Test
    public void getPutupType() throws Exception {
        assertTrue(putup.getPutupType() == PutupTypeEnum.LONGS);
    }

    @Test
    public void setPutupType() throws Exception {
        putup.setPutupType(PutupTypeEnum.SHORTS);
        assertTrue(putup.getPutupType() == PutupTypeEnum.SHORTS);
    }

    @Test
    public void getPriceList() throws Exception {
        assertTrue(putup.getPriceList().contains(price));
        assertTrue(putup.getPriceList().size() == 1);
    }
}
