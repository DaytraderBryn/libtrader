 package daytrader;

import daytrader.annotations.TWS;
import daytrader.api.DaytraderController;
import daytrader.api.exceptions.TWSException;
import daytrader.datamodel.*;
import daytrader.utils.DTUtil;
import daytrader.utils.TimeRange;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import rules.RuleResult;
import rules.TEntry;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

/**
 * beanemcbean providing you with the fresh MeiMei's.
 */
@Category(TWS.class)
public class TEntryTest {

    BarPointGraph dataGraph;
    TEntry<AbstractGraphPoint> rule;

    public void setUp() throws Exception {
        Putup putup = new Putup("FII", PutupTypeEnum.LONGS, 25.63, AtrClassEnum.UU, true);
        /*
        FII april 18th 1530.
         */


        LocalDate date = LocalDate.of(2017, 4, 18);
        LocalTime startTime = DTUtil.getExchOpeningLocalTime();
        LocalTime endTime = LocalTime.of(15,30);

        try {
            DaytraderController.getInstance().connect();
        } catch (TWSException e) {
            AbstractDataRequest logger = null;
            logger.error(e);
        }
        dataGraph = DTUtil.generateHistoricPoints(putup, date, new TimeRange(startTime, endTime), false);
        date = date.minus(1, ChronoUnit.DAYS);
        dataGraph.addPreviousGraph(27062016, DTUtil.generateHistoricPoints(putup, date, new TimeRange(DTUtil.getExchOpeningLocalTime(), DTUtil.getExchClosingLocalTime()), false));
        dataGraph.setPrevDayClose(dataGraph.getPrevDayGraph().last());
        rule = new TEntry<>();
    }

    @Test
    public void TEntryTest() throws Exception {
        setUp();
        if (dataGraph.size() >= 10) {
            System.out.println(dataGraph.size() + " bars");
            RuleResult result = rule.runPrimaryRule(dataGraph, null);
            System.out.println(result.isPassed());
        } else {
            System.out.println("not enough data :( only " + dataGraph.size() + " bars");
        }

    }

}
