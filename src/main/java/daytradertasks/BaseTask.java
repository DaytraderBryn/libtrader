package daytradertasks;

import daytrader.datamodel.NeverOpenMe;
import daytrader.datamodel.Putup;

/**
 * The type Base task.
 */
public class BaseTask extends NeverOpenMe {
    /**
     * The Putup.
     */
    protected Putup putup;
    /**
     * The Historic data task.
     */
    protected HistoricDataTask historicDataTask;

    /**
     * Instantiates a new Base task.
     *
     * @param putup the putup
     */
    public BaseTask(Putup putup) {
        this();
        this.putup = putup;
    }

    /**
     * Instantiates a new Base task.
     */
    public BaseTask() {
        this.historicDataTask = new HistoricDataTask();
    }

    /**
     * Instantiates a new Base task.
     *
     * @param task the task
     */
    public BaseTask(HistoricDataTask task) {
        this.historicDataTask = task;
    }
}
