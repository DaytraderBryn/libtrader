package daytradertasks;

import daytrader.datamodel.BarPointGraph;
import daytrader.api.DaytraderController;
import daytrader.datamodel.Putup;

import java.util.concurrent.Callable;


/**
 * Do the heavy lifting for realtime data
 */
public class LoadRealtimeDataTask extends BaseTask implements Callable<BarPointGraph> {
    /**
     * Instantiates a new Load realtime data task.
     *
     * @param putup the putup
     */
    public LoadRealtimeDataTask(Putup putup) {
        this.putup = putup;
    }

    @Override
    public BarPointGraph call() throws Exception {
        return DaytraderController.getInstance().run(this.putup);
    }
}