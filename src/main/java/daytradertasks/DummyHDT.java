/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package daytradertasks;

import com.ib.client.Contract;
import com.ib.client.DeltaNeutralContract;
import daytrader.datamodel.BarPointGraph;
import daytrader.historicRequestSystem.AbstractHDTCallable;

import java.util.Set;

/**
 * This class is used to fill the processing queue of a TWSAccount that receives a
 * pacing violation forcing that account to wait 10 min before accepting other requests.
 * <p>
 * NOT IT SHOULD NEVER EXECUTE ITS CALL METHOD and an exception will be thrown if
 * it does. The purpose of this class is simply to mark the "processed in the last
 * 10 min queue" of a TWSAccount as full.
 *
 * @author Roy
 */
public class DummyHDT extends AbstractHDTCallable {

    @Override
    public BarPointGraph call() throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void createContract() {
        //Create a standard contract to use when issuing the API call
        //As this is a Dummy Item I just borrowed Microsofts details for this as their is
        //no real putup
        this.objContract = new Contract();
        objContract.symbol("IBKR");
        objContract.secType("STK");
        objContract.currency("USD");
        objContract.exchange("SMART");
//        objContract.m_conId = DTConstants.getConId();
//        objContract.m_symbol = "MSFT";
//        objContract.m_secType = "STK";
//        objContract.m_exchange = "SMART";
//        objContract.m_currency = "USD";
//        objContract.m_primaryExch = "NYSE";
    }


    @Override
    public void orderStatus(int i, String s, double v, double v1, double v2, int i1, int i2, double v3, int i3, String s1) {

    }

    @Override
    public void updatePortfolio(Contract contract, double v, double v1, double v2, double v3, double v4, double v5, String s) {

    }

    @Override
    public void deltaNeutralValidation(int i, DeltaNeutralContract deltaNeutralContract) {

    }

    @Override
    public void position(String s, Contract contract, double v, double v1) {

    }

    @Override
    public void verifyMessageAPI(String s) {

    }

    @Override
    public void verifyCompleted(boolean b, String s) {

    }

    @Override
    public void verifyAndAuthMessageAPI(String s, String s1) {

    }

    @Override
    public void verifyAndAuthCompleted(boolean b, String s) {

    }

    @Override
    public void displayGroupList(int i, String s) {

    }

    @Override
    public void displayGroupUpdated(int i, String s) {

    }

    @Override
    public void connectAck() {

    }

    @Override
    public void positionMulti(int i, String s, String s1, Contract contract, double v, double v1) {

    }

    @Override
    public void positionMultiEnd(int i) {

    }

    @Override
    public void accountUpdateMulti(int i, String s, String s1, String s2, String s3, String s4) {

    }

    @Override
    public void accountUpdateMultiEnd(int i) {

    }

    @Override
    public void securityDefinitionOptionalParameter(int i, String s, int i1, String s1, String s2, Set<String> set, Set<Double> set1) {

    }

    @Override
    public void securityDefinitionOptionalParameterEnd(int i) {

    }
}
