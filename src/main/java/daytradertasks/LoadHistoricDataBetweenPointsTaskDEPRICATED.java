/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package daytradertasks;

import com.ib.client.*;
import daytrader.datamodel.*;
import daytrader.utils.DTUtil;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.Callable;

/**
 * DEPRACATED MOVING OVER TO HISTORICAL REQUEST QUEUE SYSTEM - REMOVE WHEN DATAGRAPHLOADER IS DEPRICATED
 *
 * @author Roy
 */
public class LoadHistoricDataBetweenPointsTaskDEPRICATED implements Callable<LoadHistoricDataPointBatchResult>, EWrapper {

    private AbstractGraphPoint startPoint;
    private AbstractGraphPoint endPoint;
    private Putup putup;
    private Contract objContract;
    private EJavaSignal m_signal;
    private EClientSocket m_client;
    private boolean loadComplete;
    private boolean abort;
    private String strAbortMsg;
    private ArrayList<String> batches;
    //This attribute will hold the final loadedPoints.
    private TreeSet<AbstractGraphPoint> loadedPoints;
    //Attribute to count completed jobs
    private int intJobs;
    private static final long MSPERBATCH = 30 * 60 * 1000;                          //Each batch is 30 min
    //These are needed by API and are set with data but are not used in this historicDataTask
    private boolean m_bIsFAAccount;
    private String m_FAAcctCodes;

    public LoadHistoricDataBetweenPointsTaskDEPRICATED(AbstractGraphPoint newStart, AbstractGraphPoint newEnd, Putup newPutup) {
        this.startPoint = newStart;
        this.endPoint = newEnd;
        this.putup = newPutup;

        //Create a standard contract to use when issuing the API call
        Contract objContract = new Contract();
        objContract.conid(DTConstants.getConId());
        objContract.symbol(this.putup.getTickerCode());
        objContract.secType("STK");
        objContract.exchange("SMART");
        objContract.currency("USD");
        objContract.primaryExch(this.putup.getMarket().toString());

        //Create socket connection
        this.m_client = new EClientSocket(this, m_signal);

        this.generateBatches();
    }

    @Override
    public LoadHistoricDataPointBatchResult call() throws Exception {
        this.loadedPoints = new TreeSet<AbstractGraphPoint>();
        LoadHistoricDataPointBatchResult finalResult = null;
        //Make the connection
        this.connect();
        if (this.isConnected()) {
            //Variables for timeout (allows 5 sec per a request with 2 secs of sleep between requests so 7 secs total)
            long startedRequestsAt = System.currentTimeMillis();
            long timeOutAt = startedRequestsAt + (7000 * this.batches.size());
            this.loadComplete = false;
            this.abort = false;
            this.strAbortMsg = "";
            this.intJobs = 0;
            for (String currBatchTime : this.batches) {
                if (this.isConnected()) {
                    this.intJobs++;
                    this.m_client.reqHistoricalData(this.objContract.conid(),
                            objContract,
                            currBatchTime,
                            DTDurationEnum.S1800.toString(),
                            BarSizeSettingEnum.SEC1.toString(),
                            WhatToShowEnum.TRADES.toString(),
                            0,
                            1,
                            null);
                    //To avoid pacing violations the thread MUST now sleep (BLOCK) for 2 secs
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException ex) {
                        throw new IOException("Thread interrupted while transmitting data request");
                    }
                } else {
                    throw new IOException("Connection to stock broker lost");
                }
            }
            //Wait for data to be delivered
            while (!this.loadComplete && !abort) {
                if (System.currentTimeMillis() < timeOutAt) {
                    Thread.yield();
                } else {
                    throw new IOException("Timed out waiting for stockbroker server LoadHistoricDataBetweenPointsTaskDEPRICATED");
                }
            }
            //Ensure we have not had to abort because of an error
            if (this.abort) {
                throw new IOException(this.strAbortMsg);
            }
            //If we reach this point all data was loaded and the loadedPoints will be returned
            //However we can sometimes get data Bryn does not want (both before market opening and after market close)
            //Bryn wants this data removed
            this.filterData();
            //Now build the final result
            finalResult = new LoadHistoricDataPointBatchResult(putup, loadedPoints);
        } else {
            throw new IOException("No connection to stock broker is availiable");
        }
        return finalResult;
    }

    public void connect() throws IOException {
        if (!this.isConnected()) {
            int clientId = this.objContract.conid();
            this.m_client.eConnect("", DTConstants.CONNECTION_PORT, clientId);
            if (!m_client.isConnected()) {
                throw new IOException("Could not connect to stock broker");
            }
        }
    }

    public boolean isConnected() {
        boolean result = false;
        if (null != this.m_client) {
            result = this.m_client.isConnected();
        }
        return result;
    }

    private void filterData() {
        TreeSet<AbstractGraphPoint> tempData = new TreeSet<AbstractGraphPoint>();
        for (AbstractGraphPoint currPoint : this.loadedPoints) {
            if (currPoint.getTime().isBefore(DTUtil.getExchClosingLocalTime()) && currPoint.getTime().isAfter(DTUtil.getExchOpeningLocalTime())) {
                tempData.add(currPoint);
            }
        }
        this.loadedPoints = tempData;
    }

    private void generateBatches() {
        this.batches = new ArrayList<String>();
        long currTime = this.endPoint.getTimestamp() + LoadHistoricDataBetweenPointsTaskDEPRICATED.MSPERBATCH;
        long startTime = this.startPoint.getTimestamp();
        boolean abort = false;
        while (!abort) {
            currTime -= LoadHistoricDataBetweenPointsTaskDEPRICATED.MSPERBATCH;
            Calendar cal = Calendar.getInstance(DTConstants.EXCH_TIME_ZONE);
            cal.setTimeInMillis(currTime);
            String batchTime = DTUtil.convertCalToBrokerTime(cal);
            this.batches.add(batchTime);
            if (currTime - LoadHistoricDataBetweenPointsTaskDEPRICATED.MSPERBATCH <= startTime) {
                abort = true;
            }
        }
    }

    @Override
    public void tickPrice(int tickerId, int field, double price, int canAutoExecute) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void tickSize(int tickerId, int field, int size) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void tickOptionComputation(int tickerId, int field, double impliedVol, double delta, double optPrice, double pvDividend, double gamma, double vega, double theta, double undPrice) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void tickGeneric(int tickerId, int tickType, double value) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void tickString(int tickerId, int tickType, String value) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void tickEFP(int tickerId, int tickType, double basisPoints, String formattedBasisPoints, double impliedFuture, int holdDays, String futureExpiry, double dividendImpact, double dividendsToExpiry) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void orderStatus(int i, String s, double v, double v1, double v2, int i1, int i2, double v3, int i3, String s1) {

    }

//    @Override
//    public void orderStatus(int orderId, String status, int filled, int remaining, double avgFillPrice, int permId, int parentId, double lastFillPrice, int clientId, String whyHeld) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }

    @Override
    public void openOrder(int orderId, Contract contract, Order order, OrderState orderState) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void openOrderEnd() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void updateAccountValue(String key, String value, String currency, String accountName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void updatePortfolio(Contract contract, double v, double v1, double v2, double v3, double v4, double v5, String s) {

    }

//    @Override
//    public void updatePortfolio(Contract contract, int position, double marketPrice, double marketValue, double averageCost, double unrealizedPNL, double realizedPNL, String accountName) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }

    @Override
    public void updateAccountTime(String timeStamp) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void accountDownloadEnd(String accountName) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void nextValidId(int orderId) {
        //The next valid order ID is irrelevent for this historicDataTask. ignore this callback.
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void contractDetails(int reqId, ContractDetails contractDetails) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void bondContractDetails(int reqId, ContractDetails contractDetails) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void contractDetailsEnd(int reqId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void execDetails(int reqId, Contract contract, Execution execution) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void execDetailsEnd(int reqId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void updateMktDepth(int tickerId, int position, int operation, int side, double price, int size) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void updateMktDepthL2(int tickerId, int position, String marketMaker, int operation, int side, double price, int size) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void updateNewsBulletin(int msgId, int msgType, String message, String origExchange) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void managedAccounts(String accountsList) {
        //This just stores the account list on connection (need to code something for this callback and I can see it working with this)
        this.m_bIsFAAccount = true;
        this.m_FAAcctCodes = accountsList;
    }

    @Override
    public void receiveFA(int faDataType, String xml) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void historicalData(int reqId, String date, double open, double high, double low, double close, int volume, int count, double WAP, boolean hasGaps) {
        if (date.contains("finished")) {
            this.intJobs--;
            if (0 == this.intJobs) {
                //All data has arrived
                this.loadComplete = true;
            }
        } else {
            //This is a new point to add to the results
            HistoricDataGraphPoint newItem = new HistoricDataGraphPoint(reqId, LocalDateTime.parse(date), open, high, low, close, volume, count, WAP, hasGaps);
            this.loadedPoints.add(newItem);
        }
    }

    @Override
    public void scannerParameters(String xml) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void scannerData(int reqId, int rank, ContractDetails contractDetails, String distance, String benchmark, String projection, String legsStr) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void scannerDataEnd(int reqId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void realtimeBar(int reqId, long time, double open, double high, double low, double close, long volume, double wap, int count) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void currentTime(long time) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void fundamentalData(int reqId, String data) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void deltaNeutralValidation(int i, DeltaNeutralContract deltaNeutralContract) {

    }

//    @Override
//    public void deltaNeutralValidation(int reqId, UnderComp underComp) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }

    @Override
    public void tickSnapshotEnd(int reqId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void marketDataType(int reqId, int marketDataType) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void commissionReport(CommissionReport commissionReport) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void position(String s, Contract contract, double v, double v1) {

    }

    @Override
    public void error(Exception e) {
        e.printStackTrace();
        System.err.println("Error invoked: " + e.getMessage());
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void error(String str) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void error(int id, int errorCode, String errorMsg) {
        switch (errorCode) {
            case 162:
                this.abort = true;
                this.strAbortMsg = "Historical Service Error: " + errorMsg;
                break;
            case 502:
                this.abort = true;
                this.strAbortMsg = "Not Connected to stock broker: " + errorMsg;
                break;
            case 2104:
                //This indicates that you have connected not sure why thats an error but ignore it
                break;
            case 2106:
                //This indicates that you have connected not sure why thats an error but ignore it
                break;
            default:
                this.abort = true;
                this.strAbortMsg = errorMsg;
        }
    }

    @Override
    public void connectionClosed() {
        this.abort = true;
        this.strAbortMsg = "Connection to stock brokers server was closed";
    }

    @Override
    public void connectAck() {

    }

    @Override
    public void positionMulti(int i, String s, String s1, Contract contract, double v, double v1) {

    }

    @Override
    public void positionMultiEnd(int i) {

    }

    @Override
    public void accountUpdateMulti(int i, String s, String s1, String s2, String s3, String s4) {

    }

    @Override
    public void accountUpdateMultiEnd(int i) {

    }

    @Override
    public void securityDefinitionOptionalParameter(int i, String s, int i1, String s1, String s2, Set<String> set, Set<Double> set1) {

    }

    @Override
    public void securityDefinitionOptionalParameterEnd(int i) {

    }

//    @Override
//    public void position(String account, Contract contract, int pos, double avgCost) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }

    @Override
    public void positionEnd() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void accountSummary(int reqId, String account, String tag, String value, String currency) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void accountSummaryEnd(int reqId) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void verifyMessageAPI(String s) {

    }

    @Override
    public void verifyCompleted(boolean b, String s) {

    }

    @Override
    public void verifyAndAuthMessageAPI(String s, String s1) {

    }

    @Override
    public void verifyAndAuthCompleted(boolean b, String s) {

    }

    @Override
    public void displayGroupList(int i, String s) {

    }

    @Override
    public void displayGroupUpdated(int i, String s) {

    }
}
