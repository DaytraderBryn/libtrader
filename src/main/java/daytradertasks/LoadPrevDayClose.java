/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package daytradertasks;

import daytrader.api.exceptions.TWSException;
import daytrader.datamodel.BarPoint;
import daytrader.datamodel.BarPointGraph;
import daytrader.datamodel.Putup;
import daytrader.utils.DTUtil;

import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * This class will load the data for the close of the previous day. This data
 * will cover the last 30 minutes of trading on the previous trading day
 *
 * @author Roy
 */
public class LoadPrevDayClose {

    private LocalDateTime previousDay;
    private Putup putup;

    /**
     * Constructor that performs initialisation and accepts the minimum additional
     * data to make an historic data request (ie a putup (the market security the
     * request is for) and a date / time representing the time the data is needed
     * up to). In addition it also accepts an object to callback to with the loaded
     * data. This object should 'do something' with the data that has been loaded.
     *
     * @param putup      - A Putup representing a stock market security
     * @param currentDay the current day
     */
    public LoadPrevDayClose(Putup putup, LocalDate currentDay) {
        this.previousDay = LocalDateTime.of(currentDay.plusDays(-1), DTUtil.getExchClosingLocalTime());
        this.putup = putup;
    }

    /**
     * Instantiates a new Load prev day close.
     *
     * @param putup the putup
     */
    public LoadPrevDayClose(Putup putup) {
        this.previousDay = LocalDateTime.of(LocalDate.now().plusDays(-1), DTUtil.getExchClosingLocalTime());
        this.putup = putup;
    }

    /**
     * Run bar point.
     *
     * @param useCache the use cache
     * @return the bar point
     * @throws TWSException the tws exception
     */
    public BarPoint run(boolean useCache) throws TWSException {
        LoadHistoricDataTask task = new LoadHistoricDataTask(this.putup, this.previousDay, 30);
        BarPointGraph graph = task.run(useCache);
        return graph.last();
    }
}
