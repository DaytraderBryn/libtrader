package daytradertasks;

import com.ib.client.Types;

import java.time.LocalDateTime;

/**
 * A task for historic data
 */
public class HistoricDataTask {

    /**
     * The Duration to use.
     */
    protected Types.DurationUnit durationToUse;
    /**
     * The Bar size.
     */
    protected Types.BarSize barSize;
    /**
     * The End date time.
     */
    protected LocalDateTime endDateTime;
    /**
     * The Length.
     */
    protected int length;

    /**
     * Instantiates a new Historic data task.
     *
     * @param length        the length
     * @param durationToUse the duration to use
     * @param barSize       the bar size
     * @param endDateTime   the end date time
     */
    public HistoricDataTask(int length, Types.DurationUnit durationToUse, Types.BarSize barSize, LocalDateTime endDateTime) {
        this.durationToUse = durationToUse;
        this.barSize = barSize;
        this.endDateTime = endDateTime;
        this.length = length;
    }

    /**
     * Instantiates a new Historic data task.
     */
    public HistoricDataTask() {
        this.durationToUse = Types.DurationUnit.SECOND;
        this.barSize = Types.BarSize._5_secs;
        this.endDateTime = LocalDateTime.now();
        this.length = 1;
    }

    /**
     * Gets length.
     *
     * @return the length
     */
    public int getLength() {
        return length;
    }

    /**
     * Sets length.
     *
     * @param length the length
     */
    public void setLength(int length) {
        this.length = length;
    }

    /**
     * Sets duration to use.
     *
     * @param durationToUse the duration to use
     */
    public void setDurationToUse(Types.DurationUnit durationToUse) {
        this.durationToUse = durationToUse;
    }

    /**
     * Sets bar size.
     *
     * @param barSize the bar size
     */
    public void setBarSize(Types.BarSize barSize) {
        this.barSize = barSize;
    }

    /**
     * Sets end date time.
     *
     * @param endDateTime the end date time
     */
    public void setEndDateTime(LocalDateTime endDateTime) {
        this.endDateTime = endDateTime;
    }

    /**
     * Gets duration to use.
     *
     * @return the duration to use
     */
    public Types.DurationUnit getDurationToUse() {
        return durationToUse;
    }

    /**
     * Gets bar size.
     *
     * @return the bar size
     */
    public Types.BarSize getBarSize() {
        return barSize;
    }

    /**
     * Gets end date time.
     *
     * @return the end date time
     */
    public LocalDateTime getEndDateTime() {
        return endDateTime;
    }

    /**
     * Gets start date time.
     *
     * @return the start date time
     */
    public LocalDateTime getStartDateTime() {
        switch (durationToUse) {
            case SECOND:
                return endDateTime.minusSeconds(length);
            case DAY:
                return endDateTime.minusDays(length);
            default:
                return endDateTime;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        HistoricDataTask o2 = (HistoricDataTask) o;
        return durationToUse.equals(o2.durationToUse) && barSize.equals(o2.barSize) && endDateTime.equals(o2.endDateTime);
    }
}
