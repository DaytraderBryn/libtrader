package daytradertasks;

import com.ib.client.Types;
import daytrader.api.DaytraderController;
import daytrader.api.exceptions.TWSException;
import daytrader.api.exceptions.request.IdenticalRequestException;
import daytrader.api.exceptions.request.TWSRequestException;
import daytrader.datamodel.BarPointGraph;
import daytrader.datamodel.Putup;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalDateTime;

/**
 * Do the heavy lifting
 * TODO: Cleanup the 50 billion constructors
 */
public class LoadHistoricDataTask extends BaseTask {

    /**
     * The Logger.
     */
    Logger logger = LogManager.getLogger();

    /**
     * Instantiates a new Load historic data task.
     *
     * @param putup the putup
     */
    public LoadHistoricDataTask(Putup putup) {
        super(putup);
    }

    /**
     * Instantiates a new Load historic data task.
     *
     * @param putup  the putup
     * @param length the length
     */
    public LoadHistoricDataTask(Putup putup, int length) {
        super(putup);
        this.historicDataTask.setLength(length);
    }

    /**
     * Instantiates a new Load historic data task.
     *
     * @param putup    the putup
     * @param duration the duration
     */
    public LoadHistoricDataTask(Putup putup, Types.DurationUnit duration) {
        super(putup);
        this.historicDataTask.setDurationToUse(duration);
    }

    /**
     * Instantiates a new Load historic data task.
     *
     * @param putup   the putup
     * @param barSize the bar size
     * @param length  the length
     */
    public LoadHistoricDataTask(Putup putup, Types.BarSize barSize, int length) {
        super(putup);
        this.historicDataTask.setBarSize(barSize);
        this.historicDataTask.setLength(length);
    }

    /**
     * Instantiates a new Load historic data task.
     *
     * @param putup    the putup
     * @param duration the duration
     * @param barSize  the bar size
     */
    public LoadHistoricDataTask(Putup putup, Types.DurationUnit duration, Types.BarSize barSize) {
        super(putup);
        this.historicDataTask.setDurationToUse(duration);
        this.historicDataTask.setBarSize(barSize);
    }

    /**
     * Instantiates a new Load historic data task.
     *
     * @param putup       the putup
     * @param duration    the duration
     * @param endDateTime the end date time
     */
    public LoadHistoricDataTask(Putup putup, Types.DurationUnit duration, LocalDateTime endDateTime) {
        super(putup);
        this.historicDataTask.setDurationToUse(duration);
        this.historicDataTask.setEndDateTime(endDateTime);
    }

    /**
     * Instantiates a new Load historic data task.
     *
     * @param putup       the putup
     * @param duration    the duration
     * @param endDateTime the end date time
     * @param length      the length
     */
    public LoadHistoricDataTask(Putup putup, Types.DurationUnit duration, LocalDateTime endDateTime, int length) {
        super(putup);
        this.historicDataTask.setDurationToUse(duration);
        this.historicDataTask.setEndDateTime(endDateTime);
        this.historicDataTask.setLength(length);
    }

    /**
     * Instantiates a new Load historic data task.
     *
     * @param putup       the putup
     * @param endDateTime the end date time
     */
    public LoadHistoricDataTask(Putup putup, LocalDateTime endDateTime) {
        super(putup);
        this.historicDataTask.setEndDateTime(endDateTime);
    }

    /**
     * Instantiates a new Load historic data task.
     *
     * @param putup       the putup
     * @param endDateTime the end date time
     * @param length      the length
     */
    public LoadHistoricDataTask(Putup putup, LocalDateTime endDateTime, int length) {
        super(putup);
        this.historicDataTask.setLength(length);
        this.historicDataTask.setEndDateTime(endDateTime);
    }

    /**
     * Instantiates a new Load historic data task.
     *
     * @param putup       the putup
     * @param barSize     the bar size
     * @param endDateTime the end date time
     * @param length      the length
     */
    public LoadHistoricDataTask(Putup putup, Types.BarSize barSize, LocalDateTime endDateTime, int length) {
        super(putup);
        this.historicDataTask.setLength(length);
        this.historicDataTask.setBarSize(barSize);
        this.historicDataTask.setEndDateTime(endDateTime);
    }

    /**
     * Run bar point graph.
     *
     * @param useCache the use cache
     * @return the bar point graph
     * @throws TWSException the tws exception
     */
    public BarPointGraph run(boolean useCache) throws TWSException {
        BarPointGraph graph = new BarPointGraph();
            try {
                graph = DaytraderController.getInstance().run(this.putup, this.historicDataTask, this.historicDataTask.length, useCache);
            } catch (IdenticalRequestException e) {
                logger.error(e);
            } catch (TWSRequestException e) {
                logger.error(e);
            }
        return graph;
    }
}
