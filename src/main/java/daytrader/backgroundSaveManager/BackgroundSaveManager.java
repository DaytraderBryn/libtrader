/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package daytrader.backgroundSaveManager;

import daytrader.datamodel.DTConstants;
import daytrader.datamodel.RealTimeRunManager;
import daytrader.utils.DTUtil;

import java.io.File;
import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.*;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This callable class will manage the background saving of data so that it can
 * be recovered during the day It will save any running put ups data every 5 min
 * by default
 *
 * @author Roy
 */
public class BackgroundSaveManager implements Callable<Void> {

    private long backupEveryXs;
    private ReentrantLock lock;
    private LocalTime nextBackup;
    private File recoveryFolder;
    private ExecutorService threadPool;
    private boolean isShutdown;

    /**
     * Default Constructor - Initialises this class to perform a background save
     * every 5 minutes of all downloaded data.
     */
    public BackgroundSaveManager() {
        this.backupEveryXs = (5 * 60);                                      //By default backup every 5 mins
        this.lock = new ReentrantLock();
        this.threadPool = Executors.newCachedThreadPool();
        this.isShutdown = false;
    }

    @Override
    public Void call() throws Exception {
        this.makeRecoveryDir();
        //Set initial backup time
        this.nextBackup = LocalTime.now().plusSeconds(backupEveryXs);
        while (!this.isShutdown) {
            try {
                if (LocalTime.now().isBefore(this.nextBackup)) {
                    //Do Backup operation
                    this.runBackup();
                    //Advance to next backup time
                    lock.lock();
                    try {
                        this.nextBackup = LocalTime.now().plusSeconds(backupEveryXs);
                    } finally {
                        lock.unlock();
                    }
                } else {
                    //Sleep until at least the expected backup time
                    long sleepTime = this.nextBackup.getNano() - LocalTime.now().getNano();
                    if (0 < sleepTime) {
                        Thread.sleep(sleepTime * 1000);
                    }
                }
            } catch (InterruptedException ex) {
                this.shutdown();
            } catch (Exception ex) {
                //Fail silently for any other exception and advance to next backup time
                if (this.nextBackup.isBefore(LocalTime.now())) {
                    //Advance to next backup time
                    lock.lock();
                    try {
                        this.nextBackup = LocalTime.now().plusSeconds(backupEveryXs);
                    } finally {
                        lock.unlock();
                    }
                }
            }
        }
        return null;
    }

    /**
     * Accessor to retrieve the time between backups in milliseconds
     *
     * @return the backupEveryXs
     */
    public long getBackupEveryXs() {
        long result = 0;
        lock.lock();
        try {
            result = this.backupEveryXs;
        } finally {
            lock.unlock();
        }
        return result;
    }

    /**
     * Accessor to set the time between backups in milliseconds
     *
     * @param backupEveryXs the backupEveryXs to set
     */
    public void setBackupEveryXs(long backupEveryXs) {
        if (0 < backupEveryXs) {
            lock.lock();
            try {
                this.backupEveryXs = backupEveryXs;
                long timeToNext = Duration.between(this.nextBackup, LocalTime.now()).getSeconds();
                if (timeToNext > this.backupEveryXs) {
                    this.nextBackup = LocalTime.now();
                }
            } finally {
                lock.unlock();
            }
        }
    }

    /**
     * Performs a controlled shutdown of the backup system. A backup is performed for
     * all running putups and then the thread pool is shutdown and the 5 min backup
     * loop is terminated.
     */
    public void shutdown() {
        ArrayList<RealTimeRunManager> runningPutups = DTConstants.getRunningRecords2();
        int count = 0;
        CompletionService serv = new ExecutorCompletionService(this.threadPool);
        if (null != runningPutups && 0 < runningPutups.size()) {
            for (RealTimeRunManager manager : runningPutups) {
                BackupRealTimeRunManager task = new BackupRealTimeRunManager(manager, recoveryFolder);
                serv.submit(task);
                count++;
            }
            for (int i = 0; i < count; i++) {
                try {
                    //Result is void only interested to know the historicDataTask is completed
                    serv.take();
                } catch (InterruptedException ex) {
                    Logger.getLogger(BackgroundSaveManager.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            //All tasks are completed isShutdown thread pool and interrupt myself
            this.threadPool.shutdownNow();
            this.isShutdown = true;
        }
    }

    /**
     * This method uses a callable historicDataTask to backup all data for all running putups
     */
    private void runBackup() {
        ArrayList<RealTimeRunManager> runningPutups = DTConstants.getRunningRecords2();
        if (null != runningPutups && 0 < runningPutups.size()) {
            for (RealTimeRunManager manager : runningPutups) {
                BackupRealTimeRunManager task = new BackupRealTimeRunManager(manager, recoveryFolder);
                this.threadPool.submit(task);
            }
        }
    }

    /**
     * This function creates a directory to store recovery data. The created directory will be
     * named "Recovery_YYYYMMDD". So for example the data for 26th August 2013 will be saved
     * into a recovery folder named "Recovery_20130826.
     */
    private void makeRecoveryDir() {
        //Create folder for recovery files
        Calendar exchOpeningTime = DTUtil.getExchOpeningTime();
        int intDate = DTUtil.convertCalendarToIntDate(exchOpeningTime);
        String folderName = "Recovery_" + intDate;
        this.recoveryFolder = new File(folderName);
        if (!this.recoveryFolder.exists()) {
            this.recoveryFolder.mkdir();
        }
    }

    /**
     * Accessor to retrieve the File object that represents the recovery folder.
     *
     * @return the recoveryFolder File object
     */
    public File getRecoveryFolder() {
        return recoveryFolder;
    }
}
