package daytrader.cacheTypes;

import com.ib.client.Contract;
import daytrader.datamodel.BarPointGraph;
import daytradertasks.HistoricDataTask;

import java.time.LocalTime;

/**
 * A type to handle a Request
 */
public class Request {
    private final Contract contract;
    private final LocalTime timeRequested;
    private final HistoricDataTask task;
    private BarPointGraph points;

    /**
     * Gets contract.
     *
     * @return the contract
     */
    public Contract getContract() {
        return contract;
    }

    /**
     * Gets task.
     *
     * @return the task
     */
    public HistoricDataTask getTask() {
        return task;
    }

    /**
     * Gets time requested.
     *
     * @return the time requested
     */
    public LocalTime getTimeRequested() {
        return timeRequested;
    }

    /**
     * Gets points.
     *
     * @return the points
     */
    public BarPointGraph getPoints() {
        return points;
    }

    /**
     * Sets points.
     *
     * @param points the points
     */
    public void setPoints(BarPointGraph points) {
        this.points = points;
    }

    /**
     * Instantiates a new Request.
     *
     * @param contract the contract
     * @param task     the task
     * @param points   the points
     */
    public Request(Contract contract, HistoricDataTask task, BarPointGraph points) {
        this.contract = contract;
        this.points = points;
        this.task = task;
        this.timeRequested = LocalTime.now();
    }

    /**
     * Instantiates a new Request.
     *
     * @param contract the contract
     * @param task     the task
     */
    public Request(Contract contract, HistoricDataTask task) {
        this.contract = contract;
        this.task = task;
        this.points = new BarPointGraph();
        this.timeRequested = LocalTime.now();
    }

    /**
     * Instantiates a new Request.
     */
    public Request() {
        this.contract = null;
        this.task = null;
        this.points = null;
        this.timeRequested = LocalTime.now();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Request o2 = (Request) o;

        return contract.equals(o2.contract) && task.equals(o2.task);
    }
}
