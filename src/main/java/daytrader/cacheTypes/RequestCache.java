package daytrader.cacheTypes;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import daytrader.datamodel.ConfigHandler;
import daytrader.datamodel.LruQueue;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Wrapper to manage a cache of commonly-used items (ie current request count, lru queue of requests)
 */
public class RequestCache {
    private transient Logger logger = LogManager.getLogger();
    private Integer count;
    private List<Request> requests;
    private LruQueue lruQueue = new LruQueue(6);
    private transient String filePath = ConfigHandler.getFilePath("requestCache");
    private transient Kryo kryo = new Kryo();

    /**
     * Instantiates a new Request cache.
     */
    public RequestCache() {
        this.count = 0;
        this.requests = new ArrayList<>();
    }

    /**
     * Gets count.
     *
     * @return the count
     */
    public Integer getCount() {
        return count;
    }

    /**
     * Sets count.
     *
     * @param count the count
     */
    public void setCount(Integer count) {
        this.count = count;
    }

    /**
     * Gets requests.
     *
     * @return the requests
     */
    public List<Request> getRequests() {
        return requests;
    }

    /**
     * Sets requests.
     *
     * @param requests the requests
     */
    public void setRequests(List<Request> requests) {
        this.requests = requests;
    }

    /**
     * Gets lru queue.
     *
     * @return the lru queue
     */
    public LruQueue getLruQueue() {
        return lruQueue;
    }

    /**
     * Sets lru queue.
     *
     * @param lruQueue the lru queue
     */
    public void setLruQueue(LruQueue lruQueue) {
        this.lruQueue = lruQueue;
    }

    /**
     * Save boolean.
     *
     * @return the boolean
     */
    public boolean save() {
        boolean result = false;
        try (Output output = new Output(new FileOutputStream(filePath))) {
            kryo.writeObject(output, this);
            result = true;
        } catch (IOException e) {
            logger.error(e);
        }

        return result;
    }

    /**
     * Load request cache.
     *
     * @return the request cache
     */
    public RequestCache load() {
        RequestCache result = new RequestCache();
        if (new File(filePath).exists()) {
            try (Input input = new Input(new FileInputStream(filePath))) {
                result = kryo.readObject(input, RequestCache.class);
            } catch (IOException e) {
                logger.error(e);
            }
        }

        return result;
    }
}
