package daytrader.api.exceptions;

/**
 * A parent class to handle all TWS Exceptions
 */
public class TWSException extends Exception {
    /**
     * Instantiates a new Tws exception.
     */
    public TWSException() {
        super("Something went wrong.");
    }

    /**
     * Instantiates a new Tws exception.
     *
     * @param message the message
     */
    public TWSException(String message) {
        super(message);
    }
}
