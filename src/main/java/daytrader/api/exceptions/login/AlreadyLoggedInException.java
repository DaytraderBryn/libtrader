package daytrader.api.exceptions.login;

/**
 * This is thrown when a login attempt fails because an API client is already logged in.
 */
public class AlreadyLoggedInException extends TWSLoginException {

    /**
     * Instantiates a new Already logged in exception.
     * TODO: Investigate using process API to find which process is logged in
     * EDIT: Looked into this, is possible and quite simple but depends on Java 9
     */
    public AlreadyLoggedInException() {
        super("A client is already logged in to TWS.");
    }
}
