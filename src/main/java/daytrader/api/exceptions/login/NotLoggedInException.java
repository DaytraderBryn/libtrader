package daytrader.api.exceptions.login;

/**
 * This is thrown when a request is attempted before an API client is logged in
 */
public class NotLoggedInException extends TWSLoginException {
    /**
     * Instantiates a new Not logged in exception.
     */
    public NotLoggedInException() {
        super("Not logged in, connect to the API before proceeding.");
    }
}
