package daytrader.api.exceptions.login;

import daytrader.api.exceptions.TWSException;

/**
 * A parent class to handle all login exceptions
 */
public abstract class TWSLoginException extends TWSException {
    /**
     * Instantiates a new Tws login exception.
     *
     * @param message the message
     */
    public TWSLoginException(String message) {
        super(message);
    }
}
