package daytrader.api.exceptions.login;

/**
 * This is thrown when either TWS isn't running or the API access is disabled
 */
public class ApiDisabledException extends TWSLoginException {
    /**
     * Instantiates a new Api disabled exception.
     */
    public ApiDisabledException() {
        super("Verify that TWS is running and settings related to API access are correct.");
    }
}
