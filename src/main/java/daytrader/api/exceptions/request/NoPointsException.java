package daytrader.api.exceptions.request;

/**
 * This is thrown when no points are returned
 */
public class NoPointsException extends TWSRequestException {
    /**
     * Instantiates a new No points exception.
     */
    public NoPointsException() {
        super("Missing all data for this contract. Retrying.");
    }
}
