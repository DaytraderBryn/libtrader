package daytrader.api.exceptions.request;

/**
 * This is thrown when the points returned don't align with what should have been
 */
public class MissingPointsException extends TWSRequestException {
    /**
     * Instantiates a new Missing points exception.
     *
     * @param got      the got
     * @param expected the expected
     */
    public MissingPointsException(int got, int expected) {
        super(String.format("Retrieved %s bars, expected %s bars.", got, expected));
    }
}
