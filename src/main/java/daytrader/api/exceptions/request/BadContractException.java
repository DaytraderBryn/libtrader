package daytrader.api.exceptions.request;

/**
 * This is thrown when an malformed contract is queried
 */
public class BadContractException extends TWSRequestException {
    /**
     * Instantiates a new Bad contract exception.
     */
    public BadContractException() {
        super("Something wrong with this contract, skipping it.");
    }
}
