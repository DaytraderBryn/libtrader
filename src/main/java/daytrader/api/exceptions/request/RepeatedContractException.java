package daytrader.api.exceptions.request;

/**
 * This is thrown when the same contract is sent too quickly
 * TODO: Investigate why this isn't used anymore
 */
public class RepeatedContractException extends TWSRequestException {
    /**
     * Instantiates a new Repeated contract exception.
     */
    public RepeatedContractException() {
        super("Repeated contract limit reached. Wait 2 seconds.");
    }
}
