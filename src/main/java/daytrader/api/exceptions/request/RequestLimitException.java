package daytrader.api.exceptions.request;

/**
 * This is thrown when the request limit is reached
 * TODO: Investigate why this isn't used
 */
public class RequestLimitException extends TWSRequestException {
    /**
     * Instantiates a new Request limit exception.
     */
    public RequestLimitException() {
        super("Request limit reached. Wait a few minutes.");
    }
}
