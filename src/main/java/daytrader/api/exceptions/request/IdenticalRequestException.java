package daytrader.api.exceptions.request;

/**
 * This is thrown when the same request is queried too fast
 */
public class IdenticalRequestException extends TWSRequestException {
    /**
     * Instantiates a new Identical request exception.
     */
    public IdenticalRequestException() {
        super("Attempting to send the same request within a 15 second window.");
    }
}
