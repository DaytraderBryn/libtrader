package daytrader.api.exceptions.request;

import daytrader.api.exceptions.TWSException;

/**
 * A parent class to handle all request exceptions
 */
public abstract class TWSRequestException extends TWSException {
    /**
     * Instantiates a new Tws request exception.
     *
     * @param message the message
     */
    public TWSRequestException(String message) {
        super(message);
    }
}
