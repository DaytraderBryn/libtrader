package daytrader.datamodel;

import com.ib.controller.Bar;

import java.time.*;
import java.time.format.DateTimeFormatter;

/**
 * The type Bar point.
 */
public class BarPoint extends AbstractGraphPoint {

    private LocalDateTime dateTime;

    /**
     * Instantiates a new Bar point.
     *
     * @param bar the bar
     */
    public BarPoint(Bar bar) {
        this();
        this.date = format(bar.time());
        this.high = bar.high();
        this.low = bar.low();
        this.open = bar.open();
        this.close = bar.close();
        this.WAP = bar.wap();
        this.volume = bar.volume();
        this.count = bar.count();
    }

    /**
     * Instantiates a new Bar point.
     *
     * @param time   the time
     * @param high   the high
     * @param low    the low
     * @param open   the open
     * @param close  the close
     * @param wap    the wap
     * @param volume the volume
     * @param count  the count
     */
    public BarPoint(long time, double high, double low, double open, double close, double wap, long volume, int count) {
        this();
        this.date = format(time);
        this.high = high;
        this.low = low;
        this.open = open;
        this.close = close;
        this.WAP = wap;
        this.volume = volume;
        this.count = count;
    }

    /**
     * Instantiates a new Bar point.
     *
     * @param point the point
     */
    public BarPoint(AbstractGraphPoint point) {
        this();
        this.date = point.date;
        this.high = point.high;
        this.low = point.low;
        this.open = point.open;
        this.close = point.close;
        this.WAP = point.WAP;
        this.volume = point.volume;
        this.count = point.count;
    }


    /**
     * Instantiates a new Bar point.
     */
    public BarPoint() {
        orderingValue = GraphPointOrderEnum.BARPOINT;
    }

    @Override
    public LocalDate getDate() {
        return this.date.toLocalDate();
    }

    @Override
    public LocalTime getTime() {
        return this.date.toLocalTime();
    }

    @Override
    public LocalDateTime getDateTime() {
        return this.date;
    }

    @Override
    public int getYear() {
        return this.date.getYear();
    }

    @Override
    public int getMonth() {
        return this.date.getMonthValue();
    }

    @Override
    public int getDay() {
        return this.date.getDayOfMonth();
    }

    /**
     * Format local date time.
     *
     * @param ms the ms
     * @return the local date time
     */
    public static LocalDateTime format(long ms) {
        return LocalDateTime.ofInstant(Instant.ofEpochSecond(ms), ZoneId.of("America/New_York"));
    }

    public String toString() {
        return String.format("(%s) Open: %s High: %s Low: %s Close: %s", DateTimeFormatter.ofPattern("dd/MM/yy HH:mm:ss").format(this.date), this.open, this.high, this.low, this.close);
    }

    /**
     * Sets date time.
     *
     * @param dateTime the date time
     */
    public void setDateTime(LocalDateTime dateTime) {
        this.date = dateTime;
    }

    //TODO Add comparators for this datatype
}
