package daytrader.datamodel;

import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Another temporary abomination
 *
 * @param <T> the type parameter
 */
public class SingletonQueue<T> {
    /**
     * The Queue.
     */
    Queue<T> queue = new LinkedList<>();
    private static SingletonQueue INSTANCE = null;
    /**
     * The Lock.
     */
    ReentrantLock lock = new ReentrantLock();

    /**
     * Gets instance.
     *
     * @return the instance
     */
    public static SingletonQueue getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new SingletonQueue();
        }

        return INSTANCE;
    }

    /**
     * Gets queue.
     *
     * @return the queue
     */
    public Queue<T> getQueue() {
        return queue;
    }

    /**
     * Add.
     *
     * @param newItem the new item
     */
    public void add(T newItem) {
        lock.lock();
        try {
            queue.add(newItem);
        } finally {
            lock.unlock();
        }
    }

    /**
     * Remove.
     *
     * @param oldItem the old item
     */
    public void remove(T oldItem) {
        lock.lock();
        try {
            queue.remove(oldItem);
        } finally {
            lock.unlock();
        }
    }

    /**
     * Poll t.
     *
     * @return the t
     */
    public T poll() {
        lock.lock();
        T result = null;
        try {
            result = queue.poll();
        } finally {
            lock.unlock();
        }

        return result;
    }

    /**
     * Is empty boolean.
     *
     * @return the boolean
     */
    public boolean isEmpty() {
        return queue.isEmpty();
    }

    /**
     * Gets size.
     *
     * @return the size
     */
    public int getSize() {
        return queue.size();
    }
}
