/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package daytrader.datamodel;

import daytrader.historicRequestSystem.HistoricRequestProcessingSystem;
import daytrader.interfaces.ICallback;
import daytradertasks.LoadHistoricDataPointBatchResult;

import java.awt.*;
import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.TreeSet;

/**
 * This is the primary model class for the day trader application it stores a
 * list of all putups (active and inactive) and allows you to retrieve active
 * ones
 *
 * @author Roy
 */
public class PrimeDataModel implements Iterable<Putup> {

    private final boolean DEBUG = false;
    private ArrayList<Putup> allPutupsList;
    private boolean locked = false;
    /**
     * String containing the file name of the XML File that defines the Put ups
     * currently in use
     */
    public static final String PUTUP_FILE_NAME = "putups.xml";

    /**
     * Default constructor - loads putup data from its XML file and makes the
     * historic data request to the stock brokers API that produced details
     * of the days on which the New York stock markets have been open for trading
     */
    public PrimeDataModel() {
        this.allPutupsList = new ArrayList<Putup>();
        if (DEBUG) {
            this.allPutupsList.add(new Putup("BAX", PutupTypeEnum.LONGS, 50000, AtrClassEnum.UU, true));
        }

        //Need to check this runs on the event queue only
        if (EventQueue.isDispatchThread()) {
            loadTradingDays();
        }
    }

    /**
     * Accessor retrieves an ArrayList containing all the putups (active or otherwise)
     * currently in the system.
     *
     * @return An ArrayList of Putup objects.
     */
    public ArrayList<Putup> getAllPutupsList() {
        ArrayList<Putup> result = new ArrayList<Putup>(this.allPutupsList);
        return result;
    }

    /**
     * Accessor retrieves an ArrayList containing all the active putups entered
     * into the system.
     *
     * @return An ArrayList of Putup objects.
     */
    public ArrayList<Putup> getActivePutups() {
        ArrayList<Putup> result = new ArrayList<Putup>();
        if (null != this.allPutupsList && 0 < this.allPutupsList.size()) {
            for (Putup currPutup : this.allPutupsList) {
                if (currPutup.isActive()) {
                    result.add(currPutup);
                }
            }
        }
        return result;
    }

    /**
     * This method adds a new putup to the list of putups in the system
     *
     * @param newItem - The Putup object to add to the list.
     */
    public void addPutup(Putup newItem) {
        if (!this.locked) {
            this.allPutupsList.add(newItem);
        }
    }

    /**
     * This method removes a putup from the list of putups in the system
     *
     * @param oldPutup - The Putup object to remove from the list.
     */
    public void removePutup(Putup oldPutup) {
        if (null != oldPutup) {
            if (!this.isLocked()) {
                this.allPutupsList.remove(oldPutup);
            }
        }
    }

    /**
     * Completely clears the list of putups
     */
    public void clearAllPutups() {
        if (!this.locked) {
            this.allPutupsList.clear();
        }
    }

    /**
     * This method persists the putups list to an XML File defined by the
     * PUTUP_FILE_NAME class constant. Any existing list will be backed up to
     * "OLD_" + PUTUP_FILE_NAME rather than being overwritten
     *
     * @return boolean true if a successful save of the putup list was made,
     * False otherwise.
     */
    public boolean savePutups() {
        boolean result = false;
        if (0 < this.allPutupsList.size()) {
            File putupFile = new File(PUTUP_FILE_NAME);
            if (putupFile.exists()) {
                File oldFile = new File("OLD_" + PUTUP_FILE_NAME);
                putupFile.renameTo(oldFile);
            }
        }
        return result;
    }

    /**
     * @return the locked
     */
    public boolean isLocked() {
        return locked;
    }

    /**
     * @param locked the locked to set
     */
    public void setLocked(boolean locked) {
        if (!this.locked) {
            this.locked = locked;
        }
    }

    @Override
    public Iterator<Putup> iterator() {
        return this.allPutupsList.iterator();
    }

    //This method fires off a thread that loads the last week of trading days
    private void loadTradingDays() {
        LoadTradingDays task = new LoadTradingDays();
        Thread newThread = new Thread(task);
        newThread.start();
    }

    private class LoadTradingDays implements Runnable, ICallback {

        @Override
        public void run() {
            HistoricRequestProcessingSystem HRSys = HistoricRequestProcessingSystem.getInstance();
            //I need a putup so used Microsoft on NYSE
            Putup tempPutup = new Putup("JCI", PutupTypeEnum.LONGS, 0, AtrClassEnum.U, false);
            //FOR TESTING PURPOSES ONLY WE MAY NEED TO FUDGE THE CURRENT DAY - COMMENT OUT FOR LIVE RUNS - START
//            Calendar dateTime = Calendar.getInstance(DTConstants.EXCH_TIME_ZONE);
//            dateTime.set(Calendar.YEAR, 2013);
//            dateTime.set(Calendar.MONTH, Calendar.JULY);
//            dateTime.set(Calendar.DAY_OF_MONTH, 24);
//            dateTime.set(Calendar.HOUR_OF_DAY, 9);
//            dateTime.set(Calendar.MINUTE, 30);
//            dateTime.set(Calendar.SECOND, 0);
//            dateTime.set(Calendar.MILLISECOND, 0);
            //FOR TESTING PURPOSES ONLY WE MAY NEED TO FUDGE THE CURRENT DAY - COMMENT OUT FOR LIVE RUNS - END


            //FOR LIVE RUNS USE THIS CODE INSTEAD OF TESTING CODE - START
            Calendar dateTime = Calendar.getInstance();
            //FOR LIVE RUNS USE THIS CODE INSTEAD OF TESTING CODE - END

//            LoadTradingDaysTask theTask = new LoadTradingDaysTask(tempPutup, dateTime, this);
//            HRSCallableWrapper wrapper = new HRSCallableWrapper(theTask);
//            HRSys.submitRequest(wrapper);
        }

        @Override
        public void callback(CallbackType type, Object data) {
            switch (type) {
                case HISTORICDATATRADINGDAYS:
                    if (data instanceof LoadHistoricDataPointBatchResult) {
                        LoadHistoricDataPointBatchResult result = (LoadHistoricDataPointBatchResult) data;
                        //Each data point represents one trading day in the last week
                        TreeSet<Integer> newTradingDays = new TreeSet<Integer>();
                        for (AbstractGraphPoint currPoint : result.loadedPoints) {
                            int dateAsNumber = currPoint.getDay();
                            newTradingDays.add(dateAsNumber);
                        }
                        //Now store this as the trading days
                        DTConstants.TRADINGDAYSLASTWEEK = newTradingDays;
                    }
                    break;
                case HISTORICDATAERROR:
                    break;
            }
        }
    }
}
