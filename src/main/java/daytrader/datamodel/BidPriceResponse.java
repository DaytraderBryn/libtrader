/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package daytrader.datamodel;

import daytrader.utils.DTUtil;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;

/**
 * A Concrete implementation of the AbstractGraphPoint class that represents the
 * stocks 'Bid' price at a given moment in time.
 *
 * @author Roy
 */
public class BidPriceResponse extends AbstractGraphPoint {

    private double bidPrice;

    /**
     * Default constructor initialises object with a bid price of zero.
     */
    public BidPriceResponse() {
        //Needed to persist the object
        this.bidPrice = 0.0d;
        orderingValue = GraphPointOrderEnum.BPR;
    }

    private BidPriceResponse(long timestamp) {
        this();
//        createCalendarFromTimestamp(timestamp);
    }

    /**
     * Given a timestamp and a new bid price this constructor initialises the
     * object to reflect the provided price / time point.
     *
     * @param timestamp - long being a timestamp for this price / time point
     * @param newPrice  - double being the price for this price / time point
     */
    public BidPriceResponse(long timestamp, double newPrice) {
        this(timestamp);
        this.setBidPrice(newPrice * DTConstants.SCALE);
        this.open = this.bidPrice;
        this.high = this.bidPrice;
        this.low = this.bidPrice;
        this.close = this.bidPrice;
        this.WAP = this.bidPrice;
    }

    /**
     * Copy constructor, duplicates the provided price / time point but advances
     * the time element by the incTime parameter
     *
     * @param target  - The BidPriceResponse object to duplicate
     * @param incTime - long being the amount to add to the time component.
     */
    public BidPriceResponse(BidPriceResponse target, long incTime) {
//        this.calDate = Calendar.getInstance(TimeZone.getTimeZone("Europe/London"));
//        long time = target.getCalDate().getTimeInMillis();
//        time += incTime;
//        this.calDate.setTimeInMillis(time);
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd  HH:mm:ss");
//        this.date = LocalDateTime.parse(formatter.format(this.calDate.getTime()));
        double price = target.getBidPrice();
        this.setBidPrice(price);
        this.open = this.bidPrice;
        this.high = this.bidPrice;
        this.low = this.bidPrice;
        this.close = this.bidPrice;
        this.WAP = this.bidPrice;
    }

    /**
     * Accessor to retrieve the Bid price of this object
     *
     * @return double being the stored bid price
     */
    public double getBidPrice() {
        return bidPrice;
    }

    /**
     * Accessor to set the Bid price of this object
     *
     * @param bidPrice double being the new bid price to set
     */
    public final void setBidPrice(double bidPrice) {
        this.bidPrice = DTUtil.step1Rounding(bidPrice).doubleValue();
    }

    @Override
    public LocalDate getDate() {
        return this.date.toLocalDate();
    }

    @Override
    public LocalTime getTime() {
        return this.date.toLocalTime();
    }

    @Override
    public LocalDateTime getDateTime() {
        return this.date;
    }

    @Override
    public long getTimestamp() {
        return this.date.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
    }

    @Override
    public int getYear() {
        return this.date.getYear();
    }

    @Override
    public int getMonth() {
        return this.date.getMonthValue();
    }

    @Override
    public int getDay() {
        return this.date.getDayOfMonth();
    }
}
