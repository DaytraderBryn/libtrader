package daytrader.datamodel;

import com.google.gson.Gson;
import daytrader.utils.DoubleRange;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.time.LocalTime;
import java.util.HashMap;

/**
 * Created by beanemcbean on 17/06/2016.
 */
public class JsonRanges {

    private HashMap<String, HashMap<AtrClassEnum, Double>> ranges;
    private HashMap<Integer, ThreeMStage> threeMstages;
    private HashMap<Integer, ThreeMStage> singleDoubleDurations;
    private transient static Logger logger = LogManager.getLogger(JsonRanges.class);

    public static JsonRanges loadRanges() {
        JsonRanges ranges = new JsonRanges();
        // TODO: Use ConfigHandler and add support for checking if in JAR
        URL res = JsonRanges.class.getResource("/range.json");
        File file = null;
        if (res.toString().startsWith("jar:")) {
            try(InputStream input = JsonRanges.class.getResourceAsStream("/range.json")) {
                file = File.createTempFile("tempfile", ".tmp");
                OutputStream out = new FileOutputStream(file);
                int read;
                byte[] bytes = new byte[1024];

                while ((read = input.read(bytes)) != -1) {
                    out.write(bytes, 0, read);
                }
                ConfigHandler.copyFile(file.getPath(), "range.json");
                file.deleteOnExit();
            } catch (IOException ex) {
                logger.error(ex.getMessage());
            }
        } else {
            try {
                ConfigHandler.copyFile(res.toURI().getPath());
            } catch (URISyntaxException ex) {
                logger.error(ex);
            }
        }

        try {
            ConfigHandler.getFilePath("range.json");
        } catch (NullPointerException ex) {
            logger.error(ex);
        }

        try (BufferedReader reader = new BufferedReader(new FileReader(ConfigHandler.getFilePath("range.json")))) {
            String currentLine;
            String json = "";
            while ((currentLine = reader.readLine()) != null) {
                json += currentLine;
            }
            Gson gson = new Gson();
            ranges = gson.fromJson(json, JsonRanges.class);
        } catch (IOException e) {
            logger.error(e);
        }
        return ranges;
    }

    private JsonRanges() {
        ranges = new HashMap<>();
        threeMstages = new HashMap<>();
        singleDoubleDurations = new HashMap<>();
    }

    public HashMap<String, HashMap<AtrClassEnum, Double>> getRanges() {
        return ranges;
    }

    public HashMap<Integer, ThreeMStage> getThreeMstages() {
        return threeMstages;
    }

    public HashMap<Integer, ThreeMStage> getSingleDoubleDurations() {return singleDoubleDurations;}

    public void addRange(String name, HashMap<AtrClassEnum, Double> values) {
        ranges.put(name, values);
    }

    public boolean setThreeMstage(Integer number, LocalTime startTime, LocalTime endTime, HashMap<AtrClassEnum, DoubleRange> ranges) {
        if (number <= 4 && number >= 1) {
            ThreeMStage threeMStage = new ThreeMStage();
            threeMStage.startTime = startTime;
            threeMStage.endTime = endTime;
            threeMStage.ranges = ranges;
            threeMstages.put(number, threeMStage);
            return true;
        } else {
            return false;
        }
    }

    public boolean setSingleDoubleDuration(Integer number, LocalTime startTime, LocalTime endTime, HashMap<AtrClassEnum, DoubleRange> ranges) {
        if (number <= 3 && number >= 1) {
            ThreeMStage singleDoubleDuration = new ThreeMStage();
            singleDoubleDuration.startTime = startTime;
            singleDoubleDuration.endTime = endTime;
            singleDoubleDuration.ranges = ranges;
            singleDoubleDurations.put(number, singleDoubleDuration);
            return true;
        } else {
            return false;
        }
    }

    public void saveChanges() {
        Gson gson = new Gson();
        try {
            FileWriter writer = new FileWriter(ConfigHandler.getFilePath("range.json"));
            writer.write(gson.toJson(this));
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public class ThreeMStage {
        private LocalTime startTime;
        private LocalTime endTime;
        private HashMap<AtrClassEnum, DoubleRange> ranges;

        ThreeMStage() {
        }

        public LocalTime getStartTime() {
            return startTime;
        }

        public LocalTime getEndTime() {
            return endTime;
        }

        public HashMap<AtrClassEnum, DoubleRange> getRanges() {
            return ranges;
        }
    }
}
