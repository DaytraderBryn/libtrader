/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package daytrader.datamodel;

import daytrader.utils.DTUtil;

import java.time.*;
import java.util.ArrayList;
import java.util.Date;

/**
 * Given a year, month and day this class can be used to create a Java Calender
 * object for the start and end of the trading day and every 30 min block
 * between. It can also create ArrayLists of strings suitable for submission to
 * the stock brokers API to request the historic data batches covering these
 * times.
 *
 * @author Roy
 */
public final class StockExchangeHours {

    private LocalDateTime startCalendar;
    private LocalDateTime endCalendar;

    /**
     * Constructor that specifies the date to be used in generating LocalDateTime objects
     * as integer numbers
     *
     * @param year  - integer representing the year
     * @param month - integer representing the month
     * @param day   - integer representing the day
     */
    public StockExchangeHours(int year, int month, int day) {
        this.setDate(year, month, day);
    }

    /**
     * Constructor that specifies the date to be used in generating LocalDateTime objects
     * as a Java Date object
     *
     * @param aDate - A Date object encapsulating the data required.
     */
    public StockExchangeHours(Date aDate) {
        this.setDate(aDate.getYear(), aDate.getMonth(), aDate.getDate());
    }

    public StockExchangeHours(LocalDateTime date) {
        this.setDate(date.getYear(), date.getMonthValue(), date.getDayOfMonth());
    }

    /**
     * Accessor method to change the date value being worked with
     *
     * @param year  - integer representing the year
     * @param month - integer representing the month
     * @param day   - integer representing the day
     */
    public void setDate(int year, int month, int day) {
        this.startCalendar = LocalDateTime.of(year, month, day, DTConstants.EXCH_OPENING_HOUR, DTConstants.EXCH_OPENING_MIN, DTConstants.EXCH_OPENING_SEC);
        this.endCalendar = LocalDateTime.of(year, month, day, DTConstants.EXCH_CLOSING_HOUR, DTConstants.EXCH_CLOSING_MIN, DTConstants.EXCH_CLOSING_SEC);
    }

    /**
     * Retrieves aLocalDateTimeobject with the time trading starts on the market
     *
     * @return ALocalDateTimeencapsulating the date and time trading starts on the
     * New York stock markets
     */
    public LocalDateTime getStartCalendar() {
        return startCalendar;
    }

    /**
     * ALocalDateTimeencapsulating the date and time trading starts on the market
     * using the GMT time zone
     *
     * @return ALocalDateTimeencapsulating the date and time trading starts on the
     * New York stock markets
     */
    public LocalDateTime getStartCalendarInGMT() {
        return this.startCalendar;
    }

    /**
     * Retrieves aLocalDateTimeobject with the time trading ends on the market
     *
     * @return ALocalDateTimeencapsulating the date and time trading ends on the
     * New York stock markets
     */
    public LocalDateTime getEndCalendar() {
        return endCalendar;
    }

    /**
     * ALocalDateTimeencapsulating the date and time trading ends on the market
     * using the GMT time zone
     *
     * @return ALocalDateTimeencapsulating the date and time trading ends on the
     * New York stock markets
     */
    public LocalDateTime getEndCalendarInGMT() {
        return this.endCalendar;
    }

//    public ArrayList<String> get1HrsBatches(){
//        ArrayList<String> result = new ArrayList<String>();
//       LocalDateTimegmtEndTime = this.getEndCalendarInGMT();
//       LocalDateTimegmtCurrTime = this.getStartCalendarInGMT();
//        int intHr = 60 * 60 * 1000;
//        int intThirtyMin = 30 * 60 * 1000;                            //30 Min as milliseconds
//        //Advance 30 min (our first 1hr batch ends 30 min into the day)
//        gmtCurrTime.add(Calendar.MILLISECOND, intThirtyMin);
//        while(gmtCurrTime.compareTo(gmtEndTime) <= 0){
//            StringBuilder currString = new StringBuilder();
//            currString.append(gmtCurrTime.get(Calendar.YEAR));
//            Integer month = gmtCurrTime.get(Calendar.MONTH);
//            month++;
//            String strMonth;
//            switch (month) {
//                case 0:
//                case 1:
//                case 2:
//                case 3:
//                case 4:
//                case 5:
//                case 6:
//                case 7:
//                case 8:
//                case 9:
//                    strMonth = "0" + month.toString();
//                    break;
//                default:
//                    strMonth = month.toString();
//            }
//            currString.append(strMonth);
//            Integer day = gmtCurrTime.get(Calendar.DATE);
//            String strDay;
//            switch (day) {
//                case 0:
//                case 1:
//                case 2:
//                case 3:
//                case 4:
//                case 5:
//                case 6:
//                case 7:
//                case 8:
//                case 9:
//                    strDay = "0" + day.toString();
//                    break;
//                default:
//                    strDay = day.toString();
//            }
//            currString.append(strDay);
//            currString.append("  ");
//            Integer hours = gmtCurrTime.get(Calendar.HOUR_OF_DAY);
//            boolean inDaylightTime = DTConstants.EXCH_TIME_ZONE.inDaylightTime(gmtCurrTime.getTime());
//            if(inDaylightTime){
//                int dstSavings = DTConstants.EXCH_TIME_ZONE.getDSTSavings();
//                Double hrsAdded = dstSavings / DTConstants.MILLSECS_PER_HOUR;
//                hours += hrsAdded.intValue();
//            }
//            String strHrs;
//            switch (hours) {
//                case 0:
//                case 1:
//                case 2:
//                case 3:
//                case 4:
//                case 5:
//                case 6:
//                case 7:
//                case 8:
//                case 9:
//                    strHrs = "0" + hours.toString() + ":";
//                    break;
//                default:
//                    strHrs = hours.toString() + ":";
//            }
//            currString.append(strHrs);
//            Integer min = gmtCurrTime.get(Calendar.MINUTE);
//            String strMin;
//            switch (min) {
//                case 0:
//                case 1:
//                case 2:
//                case 3:
//                case 4:
//                case 5:
//                case 6:
//                case 7:
//                case 8:
//                case 9:
//                    strMin = "0" + min.toString() + ":";
//                    break;
//                default:
//                    strMin = min.toString() + ":";
//            }
//            currString.append(strMin);
//            Integer secs = gmtCurrTime.get(Calendar.SECOND);
//            String strSecs;
//            switch (secs) {
//                case 0:
//                case 1:
//                case 2:
//                case 3:
//                case 4:
//                case 5:
//                case 6:
//                case 7:
//                case 8:
//                case 9:
//                    strSecs = "0" + secs.toString();
//                    break;
//                default:
//                    strSecs = secs.toString();
//            }
//            currString.append(strSecs);
//            currString.append(" GMT");
//            //Write to results the stock broker time string
//            result.add(currString.toString());
//            //Advance 30 min
//            gmtCurrTime.add(Calendar.MILLISECOND, intHr);
//        }
//        return result;
//    }

    /**
     * The maximum length of time that we can get Historic Data from the
     * stock broker API with a resolution of 1 second is 30 minutes. This method
     * generates an ArrayList of strings that contain the data needed to
     * request ALL thirty minute batches in a trading day.
     *
     * @return An ArrayList of strings containing the date and time information
     * to be sent to the stock broker to load the whole trading days data at a
     * resolution of 1 second. This will necessitate multiple requests
     */
    public ArrayList<String> get30MinBatches() {
        ArrayList<String> result = new ArrayList<String>();
        LocalDateTime gmtEndTime = this.getEndCalendarInGMT();
        LocalDateTime gmtCurrTime = this.getStartCalendarInGMT();
        int intThirtyMin = 30 * 60;                            //30 Min as seconds
        //Advance 30 min (our first 30 min batch ends 30 min into the day)
        gmtCurrTime.plusSeconds(intThirtyMin);
        while (gmtCurrTime.compareTo(gmtEndTime) <= 0) {
            StringBuilder currString = new StringBuilder();
            currString.append(gmtCurrTime.getYear());
            Integer month = gmtCurrTime.getMonthValue();
            month++;
            String strMonth;
            switch (month) {
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                    strMonth = "0" + month.toString();
                    break;
                default:
                    strMonth = month.toString();
            }
            currString.append(strMonth);
            Integer day = gmtCurrTime.getDayOfMonth();
            String strDay;
            switch (day) {
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                    strDay = "0" + day.toString();
                    break;
                default:
                    strDay = day.toString();
            }
            currString.append(strDay);
            currString.append("  ");
            Integer hours = gmtCurrTime.getHour();
            boolean inDaylightTime = ZoneId.of("Europe/London").getRules().isDaylightSavings(Instant.from(gmtCurrTime));
            if (inDaylightTime) {
                int dstSavings = DTConstants.EXCH_TIME_ZONE.getDSTSavings();
                Double hrsAdded = dstSavings / DTConstants.MILLSECS_PER_HOUR;
                hours += hrsAdded.intValue();
            }
            String strHrs;
            switch (hours) {
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                    strHrs = "0" + hours.toString() + ":";
                    break;
                default:
                    strHrs = hours.toString() + ":";
            }
            currString.append(strHrs);
            Integer min = gmtCurrTime.getMinute();
            String strMin;
            switch (min) {
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                    strMin = "0" + min.toString() + ":";
                    break;
                default:
                    strMin = min.toString() + ":";
            }
            currString.append(strMin);
            Integer secs = gmtCurrTime.getSecond();
            String strSecs;
            switch (secs) {
                case 0:
                case 1:
                case 2:
                case 3:
                case 4:
                case 5:
                case 6:
                case 7:
                case 8:
                case 9:
                    strSecs = "0" + secs.toString();
                    break;
                default:
                    strSecs = secs.toString();
            }
            currString.append(strSecs);
            currString.append(" GMT");
            //Write to results the stock broker time string
            result.add(currString.toString());
            //Advance 30 min
            gmtCurrTime.plusSeconds(intThirtyMin);
        }
        return result;
    }

    /**
     * The maximum length of time that we can get Historic Data from the
     * stock broker API with a resolution of 1 second is 30 minutes. This method
     * generates an ArrayList of strings that contain the data needed to
     * request ALL thirty minute batches in a trading day.
     *
     * @param dateTime - ALocalDateTimeidentifying the trading day for which data is required
     * @return An ArrayList of strings containing the date and time information
     * to be sent to the stock broker to load the whole trading days data at a
     * resolution of 1 second. This will necessitate multiple requests
     */
    public ArrayList<String> getBatchesForDateTime(LocalDateTime dateTime) {
        ArrayList<String> result = new ArrayList<String>();
        if (null != dateTime) {
            LocalDateTime gmtEndCal = getEndCalendarInGMT();
            gmtEndCal = LocalDateTime.from(dateTime);
            //Work out start of trading day
            LocalDateTime gmtStartCal = getStartCalendarInGMT();
            gmtStartCal = LocalDateTime.of(dateTime.toLocalDate(), DTUtil.getExchOpeningLocalTime());

            long lngThirtyMin = 30 * 60;
            LocalTime current = gmtStartCal.toLocalTime();
            LocalTime end = gmtEndCal.toLocalTime();
            long diff = Duration.between(gmtStartCal, gmtEndCal).getSeconds();
            while (diff > lngThirtyMin) {
                //Work out a 30 min batch
                //Increment curr time
                current.plusSeconds(lngThirtyMin);
                //Create Calendar
                LocalDateTime myCal = LocalDateTime.from(current);
                //Convert to broker time
                String currBatch = DTUtil.convertCalToBrokerTime(LocalDateTime.from(myCal));
                //Add to batches
                result.add(currBatch);
                //recalc diff
                diff = Duration.between(gmtStartCal, gmtEndCal).getSeconds();
            }
            //Now add the last batch
            String lastBatch = DTUtil.convertCalToBrokerTime(LocalDateTime.from(gmtEndCal));
            result.add(lastBatch);
        }
        return result;
    }
}
