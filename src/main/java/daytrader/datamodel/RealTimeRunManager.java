/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package daytrader.datamodel;

import daytrader.historicRequestSystem.HistoricRequestProcessingSystem;
import daytrader.historicRequestSystem.TWSAccount;
import daytrader.historicRequestSystem.TWSAccountList;
import daytrader.interfaces.ICallback;
import daytrader.utils.DTUtil;
import daytradertasks.LoadHistoricDataPointBatchResult;
import daytradertasks.RealTimeFiveSecBarRequest;
import daytradertasks.RequestMarketDataTask;
import rules.FTGBreachPoint;
import rules.ThreeMLRanging;
import javax.swing.*;
import java.awt.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.TreeSet;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

//import daytradertasks.PreLoadYLinesTask;

/**
 * Objects of this class accept a put up, start the real time data run and
 * manage the application of the trading rules to the returned data. This is
 * a wrapper class around a Putup object that adds the needed attributes and
 * functions for that putup to operate with the real time processing system.
 *
 * @author Roy
 */
public class RealTimeRunManager implements Callable<Void>, ICallback {

    private static final long SLEEP_TIME = 750;
    private ReentrantLock lock;
    private Putup myPutup;
    private BaseGraph<AbstractGraphPoint> graph5SecBars;
    private BaseGraph<AbstractGraphPoint> graphReqMarketData;
    private BaseGraph<AbstractGraphPoint> graphHistoricData;
    private TWSAccount genAcc;
    private ExecutorCompletionService execService5SecBar;
    private ExecutorCompletionService execServiceReqMktData;
    private RealTimeFiveSecBarRequest initialReq;
    private RequestMarketDataTask reqMrkDataReq;
    private RulesStateManager rulesManager;
    //Also need 1 FTGBreach checker per a putup
    private FTGBreachPoint ftgChecker;
    //Also need a checker for the final 3m range test
    private ThreeMLRanging threeMChecker;
    private ShowJOptionWinError errorWin;

    private RealTimeRunManager() {
        this.lock = new ReentrantLock();
        this.ftgChecker = new FTGBreachPoint();
        this.ftgChecker.setRealTimeRunManager(this);
        this.threeMChecker = new ThreeMLRanging();
        this.threeMChecker.setRealTimeRunManager(this);

        //Initialise 5 Sec bar graph
        this.graph5SecBars = new BaseGraph<AbstractGraphPoint>();
        //Initialise Request Market Data graph
        this.graphReqMarketData = new BaseGraph<AbstractGraphPoint>();
        //Initialise Historic Data graph
        this.graphHistoricData = new BaseGraph<AbstractGraphPoint>();

        ExecutorService exec5SecBar = Executors.newFixedThreadPool(1);
        this.execService5SecBar = new ExecutorCompletionService(exec5SecBar);

        TWSAccountList accountList = HistoricRequestProcessingSystem.getInstance().getAccounts();
        //Use the first account on the list for non historic data requests
        if (0 < accountList.size()) {
            this.genAcc = accountList.getGeneralAcc();
        }
    }

    /**
     * Constructor that accepts the putup to wrap and a boolean flag indicating whether
     * it should immediately request Real Time 5 second bars from the stock brokers API
     * (Normally always true but you may want to skip this step in debugging)
     *
     * @param newPutup    - A Putup object to wrap
     * @param makeInitReq - A boolean flag True triggers the delivery of real time
     *                    5 sec bar data, False skips this step.
     */
    public RealTimeRunManager(Putup newPutup, boolean makeInitReq) {
        this();
        this.myPutup = newPutup;
        this.myPutup.setRunManager(this);

        //Initialise 5 Sec bar graph
        this.graph5SecBars.setPutup(this.myPutup);
        this.graph5SecBars.setTradingDays(DTConstants.TRADINGDAYSLASTWEEK);

        //Initialise Request Market Data graph
        this.graphReqMarketData.setPutup(this.myPutup);
        this.graphReqMarketData.setTradingDays(DTConstants.TRADINGDAYSLASTWEEK);

        //Initialise Historic Data graph
        this.graphHistoricData.setPutup(this.myPutup);
        this.graphHistoricData.setTradingDays(DTConstants.TRADINGDAYSLASTWEEK);

        this.myPutup.setTodaysDate(LocalDate.now(DTConstants.EXCH_TIME_ZONE.toZoneId()));
        this.rulesManager = new RulesStateManager(this.myPutup.getPutupType(), this);

        if (makeInitReq) {
            this.makeInitialRequest();
        }
    }

    private void makeInitialRequest() {
        lock.lock();
        try {
            if (null != this.genAcc) {
                this.initialReq = new RealTimeFiveSecBarRequest(this.graph5SecBars, this.genAcc);
                this.execService5SecBar.submit(initialReq);
            }
        } finally {
            lock.unlock();
        }
    }

    /**
     * Each running putup may be in a number of states that define the actions that
     * need to be taken for that putup. The actions depend on Bryn's rules, but this
     * class contains an instance of a RulesStateManager that defines this.
     * This Accessor Method retrieves the current rules state for this putup
     *
     * @return A RulesStateEnum reflecting the current state of this putups RulesStateManager
     */
    public RulesStateEnum getCurrentState() {
        RulesStateEnum result = null;
        lock.lock();
        try {
            result = this.rulesManager.getCurrState();
        } finally {
            lock.unlock();
        }
        return result;
    }

    @Override
    public Void call() throws Exception {
        //Store this running manager as the owner for the rules group
        this.rulesManager.setRealTimeRunManager(this);
        //From now until the end of the day test the rules every second against this putup
        //Determine end of trading day
        LocalDateTime endTime = LocalDate.now().plusDays(1).atTime(DTUtil.getExchClosingLocalTime());
        LocalDateTime nextCheckTime = LocalDateTime.now().plusSeconds(1);                 //One second after the start of running
        boolean blnEnd = false;
        while (!blnEnd) {
            if (LocalDateTime.now().isAfter(endTime)) {
                blnEnd = true;
            } else {
                if (LocalDateTime.now().isAfter(nextCheckTime)) {
                    nextCheckTime.plusSeconds(1);                                          //Advance another second
                    //Test rules group to see if we should proceed
                    try {
                        if (this.rulesManager.checkCurrentRules()) {
                            this.changeToNextState();
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        this.printToConsole("Exception in rules testing, " + ex.getMessage());
                    }
                }
            }
            //Yield any remaining CPU time in this time slice - Changed to sleep to avoid constantly holding a CPU
            //Thread.yield();
            Thread.sleep(SLEEP_TIME);
        }
        return null;
    }

    /**
     * Test whether the putup wrapped by this object is the target putup provided
     * as an argument
     *
     * @param target - The putup to compare with the putup wrapped by this object
     * @return boolean True if this is the RealTimeRunManager wrapping the target
     * putup, False otherwise.
     */
    public boolean isThisPutup(Putup target) {
        boolean result = false;
        lock.lock();
        try {
            if (null != target) {
                if (target.getPriceTimeTreeSet().equals(this.myPutup.getPriceTimeTreeSet())) {
                    if (target.getMarket() == this.myPutup.getMarket()) {
                        if (target.getPutupType() == this.myPutup.getPutupType()) {
                            result = true;
                        }
                    }
                }
            }
        } finally {
            lock.unlock();
        }
        return result;
    }

    /**
     * This method performs the data pre-load for the wrapped putup. This is defined as
     * loading the last 30 min of historical data for the previous trading day and
     * pre-loading any Y-Line data + calculating the provisional Y-Lines
     */
    public void preLoadData() {
        BaseGraph<AbstractGraphPoint> prevDayGraph = this.graphHistoricData.getPrevDayGraph();
        HistoricRequestProcessingSystem HRSys = HistoricRequestProcessingSystem.getInstance();
        if (null == prevDayGraph) {
            //Pre load previous days close
//            LoadPrevDayClose historicDataTask = new LoadPrevDayClose(myPutup, this.myPutup.getTodaysDate(), this);
//            HRSCallableWrapper wrapper = new HRSCallableWrapper(historicDataTask);
//            HRSys.submitRequest(wrapper);
        }
//        //Check to see if we should do a Y-Lines pre-load of Y Line data
//        if (null != this.myPutup.getMaxYLineDate() && !this.myPutup.hasLoadedYLines()) {
//            PreLoadYLinesTask yLineLoad = new PreLoadYLinesTask(myPutup);
//            HRSCallableWrapper yLineWrapper = new HRSCallableWrapper(yLineLoad, PriorityEnum.LOW);
//            HRSys.submitRequest(yLineWrapper);
//        }
    }

    /**
     * Loads the last 30 min of data for this putup from the previous trading day.
     * This is an asynchronous operation.
     */
    public void loadPrevDayClose() {
//        LoadPrevDayClose historicDataTask = new LoadPrevDayClose(myPutup, this.myPutup.getTodaysDate(), this);
//        HRSCallableWrapper wrapper = new HRSCallableWrapper(historicDataTask);
//        HistoricRequestProcessingSystem HRSys = HistoricRequestProcessingSystem.getInstance();
//        HRSys.submitRequest(wrapper);
//        Load
        try {
            Thread.sleep(50);
        } catch (InterruptedException ex) {
            Logger.getLogger(RealTimeRunManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private boolean isValid() {
        boolean result = false;
        lock.lock();
        try {
            if (null != this.genAcc && null != this.graph5SecBars && null != this.myPutup) {
                result = true;
            }
        } finally {
            lock.unlock();
        }
        return result;
    }

    @Override
    public void callback(CallbackType type, Object data) {
        lock.lock();
        try {
            switch (type) {
                case HISTORICDATACLOSEPREVDAY:
                    if (null != this.graph5SecBars && null != data && data instanceof LoadHistoricDataPointBatchResult) {
                        LoadHistoricDataPointBatchResult aResult = (LoadHistoricDataPointBatchResult) data;
                        if (null != aResult.loadedPoints && 0 < aResult.loadedPoints.size()) {
                            AbstractGraphPoint prevDayClose = aResult.loadedPoints.last();
                            this.graph5SecBars.setPrevDayClose(prevDayClose);
                            this.graphReqMarketData.setPrevDayClose(prevDayClose);
                            this.graphHistoricData.setPrevDayClose(prevDayClose);
                            BaseGraph<AbstractGraphPoint> replicateGraph = this.graph5SecBars.replicateGraph();
                            replicateGraph.clear();
                            replicateGraph.setPrevDayClose(null);
                            replicateGraph.storeHistoricData(aResult.loadedPoints);
                            this.graph5SecBars.addPreviousGraph(prevDayClose.getDay(), replicateGraph);
                            this.graphReqMarketData.addPreviousGraph(prevDayClose.getDay(), replicateGraph);
                            this.graphHistoricData.addPreviousGraph(prevDayClose.getDay(), replicateGraph);
                        }
                    }
                    break;
                case HISTORICDATAFINISHED:
                    if (data instanceof LoadHistoricDataPointBatchResult) {
                        LoadHistoricDataPointBatchResult aResult = (LoadHistoricDataPointBatchResult) data;
                        AbstractGraphPoint lastPoint = aResult.loadedPoints.last();
                        //Is the data for 'today' or a previous day
                        LocalDate today = this.myPutup.getTodaysDate();
                        if (today.isEqual(lastPoint.getDate())) {
                            //This is for today
                            this.graphHistoricData.addAll(aResult.loadedPoints);
                        } else {
                            //This is for previous day
                            int key = lastPoint.getDay();
                            //See if the graph already exists
                            if (this.graphHistoricData.getPreviousGraphs().containsKey(key)) {
                                //Get existing Graph and add the new points
                                BaseGraph prevDayGraph = this.graphHistoricData.getPreviousGraphs().get(key);
                                prevDayGraph.addAll(aResult.loadedPoints);
                            } else {
                                //No graph exists add this as a new graph
                                BaseGraph<AbstractGraphPoint> pointsAsGraph = aResult.getPointsAsGraph();
                                this.graphHistoricData.addPreviousGraph(key, pointsAsGraph);
                            }
                        }
                    }
                    break;
                case HISTORICDATAPREVIOUSDAYS:
                    if (data instanceof LoadHistoricDataPointBatchResult) {
                        LoadHistoricDataPointBatchResult aResult = (LoadHistoricDataPointBatchResult) data;
                        BaseGraph<AbstractGraphPoint> pointsAsGraph = aResult.getPointsAsGraph();
                        AbstractGraphPoint lastPoint = aResult.loadedPoints.last();
                        int key = lastPoint.getDay();
                        //See if the graph already exists
                        if (this.graphHistoricData.getPreviousGraphs().containsKey(key)) {
                            //Get existing Graph and add the new points
                            BaseGraph prevDayGraph = this.graphHistoricData.getPreviousGraphs().get(key);
                            prevDayGraph.addAll(aResult.loadedPoints);
                        } else {
                            //No graph exists add this as a new graph
                            this.graphHistoricData.addPreviousGraph(key, pointsAsGraph);
                        }
                    }
                    break;
                case HISTORICDATATODAY:
                    if (data instanceof LoadHistoricDataPointBatchResult) {
                        LoadHistoricDataPointBatchResult result = (LoadHistoricDataPointBatchResult) data;
                        //This data should be added to todays graph
                        this.graphHistoricData.addAll(result.loadedPoints);
                    }
                    break;
                case HISTORICDATAERROR:
                    if (data instanceof LoadHistoricDataPointBatchResult) {
                        LoadHistoricDataPointBatchResult aResult = (LoadHistoricDataPointBatchResult) data;
                        Exception execException = aResult.getExecException();
                        ShowJOptionWinError warning = new ShowJOptionWinError("Error on putup " + this.myPutup.getTickerCode() + ": " + execException.getMessage());
                        if (null == this.errorWin) {
                            this.errorWin = warning;
                            EventQueue.invokeLater(warning);
                        }
                    }
                    break;
                case HISTORICDATATRADINGDAYS:
                    if (data instanceof LoadHistoricDataPointBatchResult) {
                        LoadHistoricDataPointBatchResult result = (LoadHistoricDataPointBatchResult) data;
                        //Each data point represents one trading day in the last week
                        TreeSet<Integer> newTradingDays = new TreeSet<Integer>();
                        for (AbstractGraphPoint currPoint : result.loadedPoints) {
                            int dateAsNumber = currPoint.getDay();
                            newTradingDays.add(dateAsNumber);
                        }
                        //Now store this as the trading days
                        DTConstants.TRADINGDAYSLASTWEEK = newTradingDays;
                    }
                    break;
            }
        } finally {
            lock.unlock();
        }
    }

    /**
     * @return the myPutup
     */
    public Putup getMyPutup() {
        //This object is thread safe and can be returned safely
        return myPutup;
    }

    /**
     * @return the graph5SecBars
     */
    public BaseGraph<AbstractGraphPoint> getGraph5SecBars() {
        //This object is thread safe and can be returned safely
        return graph5SecBars;
    }

    private void printToConsole(String strMsg) {
        java.awt.EventQueue.invokeLater(new ConMsg(strMsg));
    }

    /**
     * @return the graphReqMarketData
     */
    public BaseGraph<AbstractGraphPoint> getGraphReqMarketData() {
        //This object is thread safe and can be returned safely
        return graphReqMarketData;
    }

    /**
     * @return the graphHistoricData
     */
    public BaseGraph<AbstractGraphPoint> getGraphHistoricData() {
        //This object is thread safe and can be returned safely
        return graphHistoricData;
    }

    private void changeToNextState() {
        RulesStateEnum currState = this.rulesManager.getCurrState();
        //Lock out rules testing while we change state
        switch (currState) {
            case FIVESECBARONLY:
                //This means that both the FTG and 3M Range are passed in the 'clue' version
                this.changeToState(RulesStateEnum.FIVESECBARPLUSMARKETDATA);
                break;
            case FIVESECBARPLUSMARKETDATA:
                //This means that a new Single / Double clue has been found and is being tested for validity
                this.changeToState(RulesStateEnum.TESTINGSINGLEDOUBLE);
                break;
        }
    }

    private void changeToState(RulesStateEnum newState) {
        lock.lock();
        try {
            switch (newState) {
                case FIVESECBARONLY:
                    this.rulesManager.changeRuleStateTo(RulesStateEnum.FIVESECBARONLY);
                    break;
                case FIVESECBARPLUSMARKETDATA:
                    this.rulesManager.changeRuleStateTo(RulesStateEnum.FIVESECBARPLUSMARKETDATA);
                    //Make the request for market data
                    if (null == this.reqMrkDataReq || (null != this.reqMrkDataReq && !this.reqMrkDataReq.isRunning())) {
                        this.reqMrkDataReq = null;
                        ExecutorService execReqMktData = Executors.newFixedThreadPool(1);
                        //this.execServiceReqMktData = new ExecutorCompletionService(DTConstants.THREAD_POOL);
                        RequestMarketDataTask newReq = new RequestMarketDataTask(this.graphReqMarketData, this.genAcc);
                        this.reqMrkDataReq = newReq;
                        //this.execServiceReqMktData.submit(newReq);
                        CallableToRunnable task = new CallableToRunnable(newReq);
                        Thread newThread = new Thread(task);
                        newThread.start();
                    }
                    break;
                case TESTINGSINGLEDOUBLE:
                    //NB the rules to run in this state are the same as the rules for FIVESECBARPLUSMARKETDATA but we also need to start another thread to test the
                    //newly found Single Double pattern. This is done by the rulesManager.
                    this.rulesManager.changeRuleStateTo(RulesStateEnum.TESTINGSINGLEDOUBLE);
                    break;
            }
        } finally {
            lock.unlock();
        }
    }

    /**
     * The real ie non-clue version of the FTG Rule check used for this putup
     *
     * @return boolean True if this rule has been passed at any point in the day
     * False otherwise
     */
    public boolean isFTGBreached() {
        boolean result = false;
        result = this.ftgChecker.runRule();
        return result;
    }

    /**
     * This function takes the currently loaded HISTORIC data graph for this putup
     * and tests if it is 'in 3M Range' using the full ie non-clue version of this rule
     *
     * @return boolean True if the current historic data graph for this putup passes
     * the rule, False otherwise.
     */
    public boolean isIn3MRange() {
        boolean result = false;
        result = this.threeMChecker.runRule();
        return result;
    }

    /**
     * Test to confirm the the putup is still receiving 5 sec bar data from the
     * stock brokers API.
     *
     * @return boolean True if the historicDataTask requesting 5 sec bar data is still running in
     * its thread, False otherwise.
     */
    public boolean isGetting5SecBars() {
        boolean result = false;
        if (null != this.initialReq) {
            result = this.initialReq.isRunning();
        }
        return result;
    }

    /**
     * A Runnable that shows an Error message box with the specified message on the GUI
     */
    private class ShowJOptionWinError implements Runnable {

        private String msg;

        public ShowJOptionWinError(String newMsg) {
            this.msg = newMsg;
        }

        @Override
        public void run() {
            JOptionPane.showMessageDialog(null, this.msg, "Error on putup", JOptionPane.ERROR_MESSAGE);
            errorWin = null;
        }
    }

    /**
     * A Java Runnable that outputs a message to the console
     */
    private class ConMsg implements Runnable {

        private String strMsg;

        public ConMsg(String strNewMsg) {
            this.strMsg = strNewMsg;
        }

        @Override
        public void run() {
            System.out.println(this.strMsg);
        }
    }

    /**
     * Wrapper class to convert a Java Callable to a Java Runnable
     */
    private class CallableToRunnable implements Runnable {

        private Callable callable;

        public CallableToRunnable(Callable target) {
            this.callable = target;
        }

        @Override
        public void run() {
            try {
                this.callable.call();
            } catch (Exception ex) {
                Logger.getLogger(RealTimeRunManager.class.getName()).log(Level.SEVERE, null, ex);
                System.err.println("Callable error was: " + ex.getMessage());
                ex.printStackTrace();
            }
        }
    }
}
