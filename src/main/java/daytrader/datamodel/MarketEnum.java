/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package daytrader.datamodel;

import daytrader.interfaces.INamedEnum;

/**
 * This represents the market on which the stock is traded
 *
 * @author Roy
 */
public enum MarketEnum implements INamedEnum {

    /**
     * Enumeration value representing the New York stock exchange
     */
    NYSE,
    /**
     * Enumeration value representing automatic stock exchange based
     * on stock.
     */
    SMART,
    /**
     * Enumeration value representing the National Association of
     * Securities Dealers Automated Quotations stock exchange
     */
    NASDAQ
}
