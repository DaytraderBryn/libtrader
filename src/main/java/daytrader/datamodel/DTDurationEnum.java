/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package daytrader.datamodel;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Calendar;

/**
 * Enumeration representing the duration settings that can be used on an
 * historic data request to the stock brokers API. See page 388 of the
 * stock brokers API documentation
 *
 * @author Roy
 */
public enum DTDurationEnum {
    S5("5 S"),
    S60("60 S"),
    S300("300 S"),
    S900("900 S"),
    S1800("1800 S"),
    S3600("3600 S"),
    S7200("7200 S"),
    S14400("14400 S"),
    S86400("86400 S"),
    D1("1 D"),
    D2("2 D"),
    W1("1 W"),
    M1("1 M"),
    M3("3 M"),
    M6("6 M"),
    Y1("1 Y");


    private String name;

    public String toString() {
        return name;
    }

    DTDurationEnum(String val) {
        name = val;
    }

    public static DTDurationEnum getDurationToCover(Calendar startCal, Calendar endCal) {
        DTDurationEnum result = DTDurationEnum.M3;
        Long lngdiff = startCal.getTimeInMillis() - endCal.getTimeInMillis();
        Double diff = lngdiff.doubleValue();
        diff /= 1000;
        diff /= 60;
        diff /= 60;
        if (diff < (24)) {
            result = DTDurationEnum.D1;
        } else if (diff < (24 * 2)) {
            result = DTDurationEnum.D2;
        } else if (diff < ((24 * 7))) {
            result = DTDurationEnum.W1;
        }
        return result;
    }

    public static DTDurationEnum getDurationToCover(LocalDateTime startCal, LocalDateTime endCal) {
        DTDurationEnum result = DTDurationEnum.M3;
        Long lngdiff = startCal.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli() - endCal.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
        Double diff = lngdiff.doubleValue();
        diff /= 1000;
        diff /= 60;
        diff /= 60;
        if (diff < (24)) {
            result = DTDurationEnum.D1;
        } else if (diff < (24 * 2)) {
            result = DTDurationEnum.D2;
        } else if (diff < ((24 * 7))) {
            result = DTDurationEnum.W1;
        }
        return result;
    }
}
