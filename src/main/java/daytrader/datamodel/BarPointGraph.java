package daytrader.datamodel;

import daytrader.interfaces.ICallback;

import java.util.ArrayList;

/**
 * An alias to convey better intent and includes some callback functionality
 * TODO: Either hook callbacks up or remove
 */
public class BarPointGraph extends BaseGraph<BarPoint> {
    /**
     * The Callback list.
     */
    ArrayList<ICallback> callbackList;

    /**
     * Gets callback list.
     *
     * @return the callback list
     */
    public ArrayList<ICallback> getCallbackList() {
        return callbackList;
    }

    /**
     * Sets callback list.
     *
     * @param callbackList the callback list
     */
    public void setCallbackList(ArrayList<ICallback> callbackList) {
        this.callbackList = callbackList;
    }

    /**
     * Add callback.
     *
     * @param callback the callback
     */
    public void addCallback(ICallback callback) {
        if (!callbackList.contains(callback)) {
            callbackList.add(callback);
        }
    }

    /**
     * Remove callback.
     *
     * @param callback the callback
     */
    public void removeCallback(ICallback callback) {
        if (callbackList.contains(callback)) {
            callbackList.remove(callback);
        }
    }

    @Override
    public String toString() {
        return String.format("Ticker: %s, Size: %s", this.getPutup(), this.size());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BarPointGraph o1 = (BarPointGraph) o;
        return size() == o1.size() && containsAll(o1);
    }
}
