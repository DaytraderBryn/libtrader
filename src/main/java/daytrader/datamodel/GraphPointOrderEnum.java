package daytrader.datamodel;

import daytrader.interfaces.IOrderedEnum;

/**
 * Created by beanemcbean on 17/06/2016.
 */
public enum GraphPointOrderEnum implements IOrderedEnum {

    RTVR,
    RTBGP,
    APR,
    BPR,
    DGP,
    HDGP,
    BARPOINT;

    public int order() {
        return ordinal() + 1;
    }

    public static GraphPointOrderEnum fromOrder(int order) {
        for(GraphPointOrderEnum item : GraphPointOrderEnum.values()) {
            if (item.order() == order) {
                return item;
            }
        }

        return null;
    }

    @Override
    public String toString() {
        return String.format("%s", name());
    }
}
