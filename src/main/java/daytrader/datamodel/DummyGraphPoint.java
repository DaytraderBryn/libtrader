/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package daytrader.datamodel;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;

/**
 * This class can be used to create dummy point for comparison purposes in
 * TreeSet operations. It is also used to fill a TWSAccounts processing queue
 * when a 'pacing violation' occurs. Forcing a 10 minute wait before further
 * historic requests can be made using that account.
 *
 * @author Roy
 */
public class DummyGraphPoint extends AbstractGraphPoint {

    private double dblLastPrice;

    /**
     * Default constructor initialises object to 'now' with a zero price
     *
     * @param timeInMillis
     */
    public DummyGraphPoint(long timeInMillis) {
        this.dblLastPrice = 0.0d;
        orderingValue = GraphPointOrderEnum.DGP;
//        this.calDate = Calendar.getInstance();
    }

    /**
     * Constructor initialises the object to a zero price value at the given time
     *
     * @param dateTime - The timestamp used to initialise the date / time values
     */
    public DummyGraphPoint(LocalDateTime dateTime) {
//        this.calDate = Calendar.getInstance();
//        this.calDate.clear();
        LocalDateTime objDateTime = dateTime;
        this.date = (objDateTime);
        this.dblLastPrice = 0.0d;
    }

    /**
     * Constructor initialises object to a give price / time value
     *
     * @param dateTime - The timestamp used to initialise the date / time values
     * @param newPrice - double being the price value at the given time
     */
    public DummyGraphPoint(LocalDateTime dateTime, double newPrice) {
        this(dateTime);
        this.dblLastPrice = newPrice;
        this.open = this.dblLastPrice;
        this.high = this.dblLastPrice;
        this.low = this.dblLastPrice;
        this.close = this.dblLastPrice;
        this.WAP = this.dblLastPrice;
    }

    @Override
    public LocalDate getDate() {
        return this.date.toLocalDate();
    }

    @Override
    public LocalTime getTime() {
        return this.date.toLocalTime();
    }

    @Override
    public LocalDateTime getDateTime() {
        return this.date;
    }

    @Override
    public long getTimestamp() {
        return this.date.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
    }

    @Override
    public int getYear() {
        return this.date.getYear();
    }

    @Override
    public int getMonth() {
        return this.date.getMonthValue();
    }

    @Override
    public int getDay() {
        return this.date.getDayOfMonth();
    }

    @Override
    public double getLastPrice() {
        return this.dblLastPrice;
    }
}