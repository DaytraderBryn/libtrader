package daytrader.datamodel;

import java.time.LocalTime;
import java.util.Comparator;
import java.util.TreeMap;

/**
 * A complex data structure to handle the prices, times and F status for a putup
 */
public class PriceTimeTree implements Comparable<PriceTimeTree> {
    private TreeMap<Double, LocalTime> priceTimeMap;
    private Double price;
    private boolean isF;

    /**
     * Instantiates a new Price time tree.
     *
     * @param priceTimeMap the price time map
     * @param price        the price
     * @param isF          the is f
     */
    public PriceTimeTree(TreeMap<Double, LocalTime> priceTimeMap, Double price, boolean isF) {
        this.priceTimeMap = priceTimeMap;
        this.price = price;
        this.isF = isF;
    }

    /**
     * Instantiates a new Price time tree.
     *
     * @param price the price
     * @param isF   the is f
     */
    public PriceTimeTree(double price, boolean isF) {
        this.priceTimeMap = new TreeMap<>(new Comparator<Double>() {
            @Override
            public int compare(Double o1, Double o2) {
                return o1.compareTo(o2);
            }
        });

        this.price = price;
        this.isF = isF;
    }

    /**
     * Gets price time map.
     *
     * @return the price time map
     */
    public TreeMap<Double, LocalTime> getPriceTimeMap() {
        return priceTimeMap;
    }

    /**
     * Sets price time map.
     *
     * @param priceTimeMap the price time map
     */
    public void setPriceTimeMap(TreeMap<Double, LocalTime> priceTimeMap) {
        this.priceTimeMap = priceTimeMap;
    }

    /**
     * Gets price.
     *
     * @return the price
     */
    public Double getPrice() {
        return price;
    }

    /**
     * Sets price.
     *
     * @param price the price
     */
    public void setPrice(Double price) {
        this.price = price;
    }

    /**
     * Is f boolean.
     *
     * @return the boolean
     */
    public boolean isF() {
        return isF;
    }

    /**
     * Sets f.
     *
     * @param f the f
     */
    public void setF(boolean f) {
        isF = f;
    }

    @Override
    public int compareTo(PriceTimeTree o) {
        return this.price.compareTo(o.price);
    }

    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PriceTimeTree priceTimeTree = (PriceTimeTree) o;

        return this.price.equals(priceTimeTree.price);
    }

    /**
     * Equals boolean.
     *
     * @param o the o
     * @return the boolean
     */
    public boolean equals(PriceTimeTree o) {
        return this.price.equals(o.price);
    }
}
