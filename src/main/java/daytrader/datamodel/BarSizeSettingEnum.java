/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package daytrader.datamodel;

/**
 * This enumeration provides the valid BarPoint Size Settings for Historical Data Requests
 * See page 388 of stock broker's API documentation. The toString() method is
 * overridden to provide the strings used by the API.
 *
 * @author Roy
 */
public enum BarSizeSettingEnum {

    SEC1("1 secs"),
    SEC5("5 secs"),
    SEC15("15 secs"),
    SEC30("30 secs"),
    MIN1("1 min"),
    MIN2("2 mins"),
    MIN3("3 mins"),
    MIN5("5 mins"),
    MIN15("15 mins"),
    MIN30("30 mins"),
    HR1("1 hour"),
    DAY1("1 day");

    private String name;

    BarSizeSettingEnum(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
