/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package daytrader.datamodel;

import daytrader.interfaces.IGraphLine;

import java.time.Duration;
import java.time.LocalTime;
import java.time.temporal.ChronoField;
import java.util.TreeSet;

/**
 * This class represents a trend line on a graph it is a base concrete implementation
 * of the IGraphLine interface.
 *
 * @author Roy
 */
public class GraphLine<T extends AbstractGraphPoint> implements IGraphLine<T> {

    private T startPoint;
    private T endPoint;
    private BaseGraph<T> graph;
    private T standInC;
    private T standInE;
    private Double dblGradientCache;
    //If a line reaches across more than one trading day we must use only the time 'in a trading day' when calculating the gradient
    //This attribute maintains a list of trading days. It does not have to be set but gradient calcs across multiple days will not work
    //if it is not
    private TreeSet<Integer> tradingDays;
    private PutupTypeEnum typeEnum;

    /**
     * Constructor defines a new Graph Line with the given start and end points and associates it with the provided graph
     *
     * @param newStartPoint - The price / time point that represents the start of the graph
     * @param newEndPoint   - The price / time point that represents the end of the graph
     * @param newGraph      - The BaseGraph object on which this Graph Line is 'drawn'
     */
    public GraphLine(T newStartPoint, T newEndPoint, BaseGraph<T> newGraph, PutupTypeEnum putupTypeEnum) {
        this.startPoint = newStartPoint;
        this.endPoint = newEndPoint;
        this.graph = newGraph;
        this.typeEnum = putupTypeEnum;
    }

    private GraphLine(T newStartPoint, T newEndPoint, BaseGraph<T> newGraph, T standInC, T standInE, PutupTypeEnum putupTypeEnum) {
        this.startPoint = newStartPoint;
        this.endPoint = newEndPoint;
        this.graph = newGraph;
        this.standInC = standInC;
        this.standInE = standInE;
        this.typeEnum = putupTypeEnum;
    }

    @Override
    public double getGradient() {
        double result = 0.0d;
        if (this.isValid()) {
            if (null == this.dblGradientCache) {
                //Get the start point (original or stand in)
                T myCurrStart = this.getCurrentC();
                T myCurrEnd = this.getCurrentE();
                double deltaX = myCurrEnd.getTime().get(ChronoField.SECOND_OF_DAY) - myCurrStart.getTime().get(ChronoField.SECOND_OF_DAY);
                double deltaY;
                if (typeEnum == PutupTypeEnum.LONGS) {
                    deltaY = myCurrEnd.getHigh() * 100 - myCurrStart.getHigh() * 100;
                } else {
                    deltaY = myCurrEnd.getLow() * 100 - myCurrStart.getLow() * 100;
                }
                result = deltaY / deltaX;
                this.dblGradientCache = result;
            } else {
                //No need to work out the gradient we already have it, return the cache
                result = this.dblGradientCache.doubleValue();
            }
        }
        return result;
    }

    @Override
    public double yInterceptPrice() {
        double result = 0;
        if (this.isValid()) {
            //Get the start point (original or stand in)
            T myCurrStart = this.getCurrentC();
            //Find the y value at zero time on the graph
            result = myCurrStart.getHigh();
        }
        return result;
    }

    @Override
    public double xInterceptTime() {
        double result = 0;
        if (this.isValid() && this.getLinedirection() != LineDirectionEnum.HORIZONTAL) {
            //Find the x value on the graph where the price is 0
            //Determine b
            double b = this.yInterceptPrice();
            double gradient = this.getGradient();
            double timesteps = b / gradient;
            switch (this.getLinedirection()) {
                case FALLING:
                    result = this.getCurrentC().getTimestamp() + timesteps;
                    break;
                case RISING:
                    result = this.getCurrentC().getTimestamp() - timesteps;
                    break;
            }
        }
        return result;
    }

    @Override
    public double getPriceAtTime(LocalTime time) {
        double result = 0;
        if (this.isValid()) {
            //Get the start point (original or stand in)
            T myCurrStart = this.getCurrentC();
            Duration deltaTime = Duration.between(myCurrStart.getTime(), time);
            result = myCurrStart.getLastPrice();
            if (typeEnum == PutupTypeEnum.LONGS) {
                if (this.getLinedirection() == LineDirectionEnum.FALLING) {
                    double b = myCurrStart.getHigh();
                    result = (this.getGradient() * deltaTime.getSeconds()) / 100 + b;
                }
            } else if (typeEnum == PutupTypeEnum.SHORTS) {
                if (this.getLinedirection() == LineDirectionEnum.RISING) {
                    double b = myCurrStart.getLow();
                    result = (this.getGradient() * deltaTime.getSeconds() / 100 + b);
                }
            }
        }
        return result;
    }

    @Override
    public LineDirectionEnum getLinedirection() {
        double gradient = this.getGradient();
        LineDirectionEnum result;
        if (0 == gradient) {
            result = LineDirectionEnum.HORIZONTAL;
        } else if (gradient > 0) {
            result = LineDirectionEnum.RISING;
        } else {
            result = LineDirectionEnum.FALLING;
        }
        return result;
    }

    @Override
    public boolean isValid() {
        boolean result = false;
        if (null != this.startPoint && null != this.endPoint && null != graph) {
            if (this.startPoint.getTime().isBefore(this.endPoint.getTime())) {
                result = true;
            }
        }
        return result;
    }

    @Override
    public T getStartPoint() {
        return startPoint;
    }

    @Override
    public void setStartPoint(T startPoint) {
        this.startPoint = (T) startPoint;
        this.dblGradientCache = null;
    }

    @Override
    public T getEndPoint() {
        return endPoint;
    }

    @Override
    public void setEndPoint(T endPoint) {
        this.endPoint = (T) endPoint;
        this.dblGradientCache = null;
    }

    @Override
    public BaseGraph<T> getGraph() {
        return graph;
    }

    @Override
    public void setGraph(BaseGraph graph) {
        this.graph = graph;
        this.dblGradientCache = null;
    }

    @Override
    public T getStandInC() {
        return standInC;
    }

    @Override
    public void setStandInC(T standInC) {
        if (null != standInC) {
            if (standInC.getTimestamp() > this.startPoint.getTimestamp()) {
                if (standInC.getTimestamp() < this.endPoint.getTimestamp()) {
                    this.standInC = (T) standInC;
                    this.dblGradientCache = null;
                }
            }
        }
    }

    @Override
    public T getCurrentC() {
        T result = this.startPoint;
        if (null != this.standInC) {
            result = this.standInC;
        }
        return (T) result;
    }

    @Override
    public T getStandInE() {
        return standInE;
    }

    @Override
    public void setStandInE(T standInE) {
        if (null != standInE) {
            if (standInE.getTimestamp() > this.endPoint.getTimestamp()) {
                if (standInE.getTimestamp() > this.startPoint.getTimestamp()) {
                    this.standInE = (T) standInE;
                    this.dblGradientCache = null;
                }
            }
        }
    }

    @Override
    public T getCurrentE() {
        T result = this.endPoint;
        if (null != this.standInE) {
            result = this.standInE;
        }
        return (T) result;
    }

    @Override
    public String toString() {
        StringBuilder msg = new StringBuilder("C Point = ");
        msg.append(this.getCurrentC().toString());
        msg.append(", E Point = ");
        msg.append(this.getCurrentE().toString());
        return msg.toString();
    }

    /**
     * @param tradingDays the tradingDays to set
     */
    @Override
    public void setTradingDays(TreeSet<Integer> tradingDays) {
        if (null != tradingDays) {
            this.tradingDays = new TreeSet<>(tradingDays);
        } else {
            this.tradingDays = null;
        }
        this.dblGradientCache = null;
    }

    @Override
    public IGraphLine deepCopyLine() {
        IGraphLine result;
        result = new GraphLine(startPoint, endPoint, graph, standInC, standInE, typeEnum);
        if (null != this.tradingDays) {
            result.setTradingDays(tradingDays);
        }
        return result;
    }

    /**
     * @return the tradingDays
     */
    @Override
    public TreeSet<Integer> getTradingDays() {
        return new TreeSet<Integer>(this.tradingDays);
    }

    @Override
    public boolean isACPoint(T potentialCPoint) {
        boolean result = false;
        if (null != potentialCPoint) {
            if ((null != this.startPoint && this.startPoint.equals(potentialCPoint)) || (null != this.standInC && this.standInC.equals(potentialCPoint))) {
                result = true;
            }
        }
        return result;
    }

    @Override
    public T getIntersect(IGraphLine targetLine) {
        T result = null;
        if (null != targetLine) {
            //Equation of a line is y = mx+b
            //Where y is the WAP
            //m is the gradient
            //x is the timestamp and
            //b is a constant defined as y - mx
            //Let b1 refer to this lines b value and b2 refer to the target (and so on for other variables)
            //Gradients of the two lines (m1 and m2)
            double m1 = this.getGradient();
            double m2 = targetLine.getGradient();
            //m3 is defined as m1-m2
            double m3 = m1 - m2;
            //Use the start point of each line to calculate its b value
            double b1 = this.getCurrentC().getHigh() - (m1 * this.getCurrentC().getTimestamp());
            double b2 = targetLine.getCurrentC().getHigh() - (m2 * targetLine.getCurrentC().getTimestamp());
            //x is defined as (b2-b1) / m3 (x is the timestamp of the intersect
            Double x = (b2 - b1) / m3;
            //To find the intersecting WAP (y) simply plug x into the equation for either line (I use 'this' line)
            double y = m1 * x + b1;
            //To create a point x must be a 'long' convert to long value and generate the intersect point
            //todo fix this, equation is right, just need to store it better
            //result = (T) new DummyGraphPoint(x.longValue(), y);
        }
        return null;
    }

    @Override
    public int compareTo(IGraphLine o) {
        //For the purpose of comparing 2 lines they will be considered equal if the current C and current E are equal
        int result = 0;
        IGraphLine graphLine = (IGraphLine) o;
        if (this.getCurrentC().equals(graphLine.getCurrentC()) && this.getCurrentE().equals(graphLine.getCurrentE())) {
            result = 0;
        } else {
            //Ordering will be decided by when the lines 'start' per their start point those that start earlier are 'less than'
            //those that start later
            long thisTimestamp = this.getCurrentC().getTimestamp();
            long oTimestamp = graphLine.getCurrentC().getTimestamp();
            long diffTime = thisTimestamp - oTimestamp;
            if (diffTime > 0) {
                result = 1;
            } else {
                result = -1;
            }
        }
        return result;
    }
}