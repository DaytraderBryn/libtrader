package daytrader.datamodel;

import java.util.HashSet;
import java.util.concurrent.locks.ReentrantLock;

/**
 * A temporary abomination
 */
public class GlobalPoints {
    private static GlobalPoints INSTANCE = null;
    private HashSet<BarPointGraph> pointsList;
    private ReentrantLock lock;

    private GlobalPoints() {
        pointsList = new HashSet<>();
        lock = new ReentrantLock();
    }

    /**
     * Gets instance.
     *
     * @return the instance
     */
    public static GlobalPoints getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new GlobalPoints();
        }

        return INSTANCE;
    }

    /**
     * Gets points list.
     *
     * @return the points list
     */
    public HashSet<BarPointGraph> getPointsList() {
        return this.pointsList;
    }

    /**
     * Add graph.
     *
     * @param newGraph the new graph
     */
    public void addGraph(BarPointGraph newGraph) {
        this.lock.lock();
        try {
            if (newGraph != null && !this.pointsList.contains(newGraph)) {
                this.pointsList.add(newGraph);
                System.out.println("Adding " + newGraph.getPutup().getTickerCode());
            }
        } finally {
            this.lock.unlock();
        }
    }

    /**
     * Remove item.
     *
     * @param oldGraph the old graph
     */
    public void removeItem(BarPointGraph oldGraph) {
        this.lock.lock();
        try {
            if (oldGraph != null && this.pointsList.contains(oldGraph)) {
                this.pointsList.remove(oldGraph);
            }
        } finally {
            this.lock.unlock();
        }
    }

    /**
     * Gets size.
     *
     * @return the size
     */
    public int getSize() {
        return this.pointsList.size();
    }

    /**
     * Gets graph by ticker.
     *
     * @param tickerCode the ticker code
     * @return the graph by ticker
     */
    public BarPointGraph getGraphByTicker(String tickerCode) {
        BarPointGraph result = null;
        for (BarPointGraph points : pointsList) {
            if (points.getPutup().getTickerCode().equals(tickerCode)) {
                result = points;
                break;
            }
        }

        return result;
    }
}
