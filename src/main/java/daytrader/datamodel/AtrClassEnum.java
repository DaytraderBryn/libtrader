/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package daytrader.datamodel;

import daytrader.interfaces.IOrderedEnum;

/**
 * @author Roy
 */
public enum AtrClassEnum implements IOrderedEnum {

    /**
     * A Putup class used to apply different formula and limits throughout the application
     */
    UU,
    U,
    PA,
    PAP,
    PP,
    PH;

    public int order() {
        return ordinal() + 1;
    }
}