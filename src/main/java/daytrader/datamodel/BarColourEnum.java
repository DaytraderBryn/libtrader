package daytrader.datamodel;

import daytrader.interfaces.INamedEnum;

/**
 * Created by elken on 27/06/2016.
 */
public enum BarColourEnum implements INamedEnum {
    RED, GREEN;
}
