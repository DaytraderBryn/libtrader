/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package daytrader.datamodel;

import daytrader.interfaces.IGraphPoint;
import daytrader.interfaces.IRoundFunction;
import daytrader.utils.DTUtil;
import rules.BrynRoundingStep2;
import rules.RoundToTenthCent;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Date;
import java.util.TimeZone;

/**
 * This class encapsulates a piece of historic data returned by the Stock
 * Brokers API. Note: this class has a natural ordering that is inconsistent
 * with equals. Natural ordering is based on timestamp but equals compares the
 * closing price
 *
 * @author Roy
 */
public class HistoricDataGraphPoint extends AbstractGraphPoint implements IGraphPoint {

    /**
     * Default Constructor - Creates empty object
     */
    public HistoricDataGraphPoint() {
        orderingValue = GraphPointOrderEnum.HDGP;
    }

    /**
     * Constructor builds an historic data graph point using a timestamp and price
     * value. Hence this object represents a point on a Price / Time graph
     *
     * @param timestamp - The timestap to be used to initialise the objects date
     *                  and time values
     * @param value     - The price of a security at the point in time represented by the
     *                  timestamp
     */
    public HistoricDataGraphPoint(long timestamp, double value) {
        this();
        RoundToTenthCent rounder = new RoundToTenthCent();
        this.reqId = 0;
        Date theDate = new Date(timestamp);
        DTUtil.dateToCalendar(theDate, TimeZone.getTimeZone("America/New_York"));
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd  HH:mm:ss");
        this.date = LocalDateTime.parse(formatter.format(theDate));
        this.open = (rounder.performRounding(value)).doubleValue();//value;
        this.high = (rounder.performRounding(value)).doubleValue();
        this.low = (rounder.performRounding(value)).doubleValue();
        this.close = (rounder.performRounding(value)).doubleValue();
        this.volume = 0;
        this.count = 0;
        this.WAP = (rounder.performRounding(value)).doubleValue();
        this.hasGaps = true;

//        this.createCalendarFromTimestamp(timestamp);

    }

    /**
     * Constructor which accepts the data returned from the stock brokers API for
     * an historic data request and builds a price / time point encapsulating this data.
     *
     * @param reqId   - The integer number identifying the historic data request
     * @param date    - A String containing the data and time information for this point
     * @param open    - The securities 'open' price at this data and time
     * @param high    - The securities 'highest' price at this data and time
     * @param low     - The securities 'lowest' price at this data and time
     * @param close   - The securities 'closing' price at this data and time
     * @param volume  - The volume of shares traded at this data and time
     * @param count   - The number of trades made at this data and time
     * @param WAP     - The Average Weighted Price over the time period (usually 1 sec for historic data)
     * @param hasGaps - boolean flag, True if their are gaps in the data, False otherwise
     */
    public HistoricDataGraphPoint(int reqId, LocalDateTime date, double open, double high, double low, double close, int volume, int count, double WAP, boolean hasGaps) {
        this();
        //RoundToTenthCent rounder = new RoundToTenthCent();
        this.reqId = reqId;
        this.date = date;
        //this.open = (rounder.performRounding(open * DTConstants.SCALE)).doubleValue();
        //this.high = (rounder.performRounding(high * DTConstants.SCALE)).doubleValue();
        //this.low = (rounder.performRounding(low * DTConstants.SCALE)).doubleValue();
        //this.close = (rounder.performRounding(close * DTConstants.SCALE)).doubleValue();
        this.volume = volume;
        this.count = count;
        //this.WAP = (rounder.performRounding(WAP * DTConstants.SCALE)).doubleValue();
        this.hasGaps = hasGaps;
        this.setValues(open * DTConstants.SCALE, high * DTConstants.SCALE, low * DTConstants.SCALE, close * DTConstants.SCALE, WAP * DTConstants.SCALE);

//        createCalendar();
    }

    @Override
    public int getReqId() {
        return this.reqId;
    }

    /**
     * Accessor method to retrieve the string representing this objects date and time
     * component in a format acceptable to the stockbrokers API
     *
     * @return A String representing this points date and time data.
     */
    public LocalDate getDate() {
        return date.toLocalDate();
    }

    @Override
    public LocalTime getTime() {
        return date.toLocalTime();
    }

    @Override
    public LocalDateTime getDateTime() {
        return date;
    }

    @Override
    public long getTimestamp() {
        return date.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
    }

    @Override
    public int getYear() {
        return date.getYear();
    }

    @Override
    public int getMonth() {
        return date.getMonthValue();
    }

    @Override
    public int getDay() {
        return date.getDayOfMonth();
    }

    @Override
    public long getVolume() {
        return this.volume;
    }

    @Override
    public int getCount() {
        return this.count;
    }

    @Override
    public boolean isHasGaps() {
        return this.hasGaps;
    }

    @Override
    protected void setValues(double open, double high, double low, double close, double WAP) {
        super.setValues(open, high, low, close, WAP);
        //Now apply stage two rounding
        BrynRoundingStep2 rounder = new BrynRoundingStep2();
        this.open = rounder.performRounding(this.open).doubleValue();
        this.high = rounder.performRounding(this.high).doubleValue();
        this.low = rounder.performRounding(this.low).doubleValue();
        this.close = rounder.performRounding(this.close).doubleValue();
        this.WAP = rounder.performRounding(this.WAP).doubleValue();
    }

//    @Override
//    public boolean equals(Object obj) {
//        boolean result = false;
//        if (obj instanceof HistoricDataGraphPoint) {
//            HistoricDataGraphPoint target = (HistoricDataGraphPoint) obj;
//            if (this.close == target.getClose()) {
//                result = true;
//            }
//        }
//        return result;
//    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 29 * hash + (int) (Double.doubleToLongBits(this.close) ^ (Double.doubleToLongBits(this.close) >>> 32));
        return hash;
    }

    @Override
    public int compareTo(IGraphPoint o) {
        return o.getDateTime().compareTo(this.getDateTime());
    }

    @Override
    public double getLastPrice() {
        double wap = this.getWAP();
        return wap;
    }

    /**
     * The 'Last' price for an historic data item is defined as the weighted average price
     * This allows you to retrieve that price and specify a rounding function that
     * should be applied to the value.
     *
     * @param rounder - The rounding function to use
     * @return double being the Weighted Average Price after rounding using the
     * provided function
     */
    public double getLastPrice(IRoundFunction<Double> rounder) {
        double wap = this.getWAP();
        return rounder.performRounding(wap).doubleValue();
    }

    @Override
    public String toString() {
        DateTimeFormatter usFormatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT).withZone(ZoneId.of("America/New_York"));
        DateTimeFormatter ukFormatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT).withZone(ZoneId.of("Europe/London"));

        return String.format("%s GMT, %s Exch, Last: %s, Low: %s", ukFormatter.format(date.atZone(ZoneId.systemDefault()).toInstant()), usFormatter.format(date.atZone(ZoneId.systemDefault()).toInstant()), this.getLastPrice(), this.getLow());
    }
}
