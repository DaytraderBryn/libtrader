package daytrader.datamodel;

import daytrader.cacheTypes.Request;

import java.util.ArrayList;

/**
 * Simple least recently used queue
 */
public class LruQueue extends ArrayList<Request> {
    private int limit;

    /**
     * Instantiates a new Lru queue.
     *
     * @param limit the limit
     */
    public LruQueue(int limit) {
        this.limit = limit;
    }

    /**
     * Instantiates a new Lru queue.
     */
    public LruQueue() {
        this.limit = 0;
    }

    /**
     * Push boolean.
     *
     * @param item the item
     * @return the boolean
     */
    public boolean push(Request item) {
        boolean result = super.add(item);
        if (size() > limit) {
            removeRange(0, size() - limit - 1);
        }

        return result;
    }

    /**
     * Contains values boolean.
     *
     * @param item the item
     * @return the boolean
     */
    public boolean containsValues(Request item) {
        boolean result = false;
        for(Request t : this) {
            if (t.getContract().exchange().equals(item.getContract().exchange()) && t.getContract().equals(item.getContract())) {
                result = true;
                break;
            }
        }

        return result;
    }
}
