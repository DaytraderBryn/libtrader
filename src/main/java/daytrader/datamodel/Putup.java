/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package daytrader.datamodel;

//import daytrader.gui.YLineLoadingDisplay;

import daytrader.interfaces.Lockable;
import daytrader.interfaces.observerpattern.IObserver;
import daytrader.interfaces.observerpattern.ISubject;
import daytrader.interfaces.observerpattern.ISubjectDelegate;
import daytrader.utils.DoubleRange;

import java.math.RoundingMode;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.*;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

import static daytrader.utils.DTUtil.roundTwoDP;

/**
 * A class providing the details of a 'Putup' i.e. a stock being monitored.
 * This is a thread safe class
 *
 * @author Roy
 */
public class Putup implements Lockable, ISubject {

    private boolean active;
    private PutupTypeEnum putupType;
    private String tickerCode;
    private MarketEnum market;
    private AtrClassEnum atrClass;
    private ReentrantLock lock;
    private LocalDate todaysDate;
    private ISubjectDelegate subjectDelegate;
    private RealTimeRunManager runManager;
    private TreeSet<PriceTimeTree> priceTimeTreeSet;

    public boolean isF() {
        boolean result = false;
        lock.lock();
        try {
            result = this.isF;
        } finally {
            lock.unlock();
        }
        return result;
    }

    public void setF(boolean f) {
        lock.lock();
        try {
            this.isF = f;
            this.notifyObservers();
        } finally {
            lock.unlock();
        }
    }

    private boolean isF;

    /**
     * Default constructor that creates an inactive Putup for an UNKNOWN stock
     * ticker on the New York Stock Exchange with a 3ml price of 0 and an
     * AtrClass of UU
     */
    public Putup() {
        priceTimeTreeSet = new TreeSet<>(new Comparator<PriceTimeTree>() {
            @Override
            public int compare(PriceTimeTree o1, PriceTimeTree o2) {
                return o1.compareTo(o2);
            }
        });
        this.subjectDelegate = new ISubjectDelegate();

        active = false;
        tickerCode = "UNKNOWN";
        market = MarketEnum.SMART;
        this.putupType = PutupTypeEnum.LONGS;
        atrClass = AtrClassEnum.UU;
        todaysDate = LocalDate.now();
        lock = new ReentrantLock();
    }

    /**
     * Constructor that creates an inactive Putup for the provided stock ticker,
     * stock market, 3ml Price and AtrClass.
     *
     * @param newTicker       - String being the ticker code of the stock to be
     *                        monitored
     * @param newType         - PutupTypeEnum the type of putup (Long or Short)
     * @param newThreeMlPrice - The price component of the 3ML code
     * @param newClass        - The AtrClass used for this Putup
     */
    public Putup(String newTicker, PutupTypeEnum newType, double newThreeMlPrice, AtrClassEnum newClass, boolean isF) {
        priceTimeTreeSet = new TreeSet<>(new Comparator<PriceTimeTree>() {
            @Override
            public int compare(PriceTimeTree o1, PriceTimeTree o2) {
                return o1.compareTo(o2);
            }
        });
        this.subjectDelegate = new ISubjectDelegate();
        this.active = true;
        this.tickerCode = newTicker;
        this.market = MarketEnum.SMART;
        this.putupType = newType;
        this.atrClass = newClass;
        todaysDate = LocalDate.now();
        lock = new ReentrantLock();
        populatePriceTupleList(new TreeMap<Double, Boolean>() {{
            put(newThreeMlPrice, isF);
        }});
        this.isF = isF;
    }

    public Putup(String newTicker, PutupTypeEnum newType, TreeMap<Double, Boolean> priceFMap, AtrClassEnum newClass) {
        this.subjectDelegate = new ISubjectDelegate();

        priceTimeTreeSet = new TreeSet<>(new Comparator<PriceTimeTree>() {
            @Override
            public int compare(PriceTimeTree o1, PriceTimeTree o2) {
                return o1.compareTo(o2);
            }
        });
        this.active = true;
        this.tickerCode = newTicker;
        this.market = MarketEnum.SMART;
        this.putupType = newType;
        this.atrClass = newClass;
        todaysDate = LocalDate.now();
        lock = new ReentrantLock();
        populatePriceTupleList(priceFMap);
    }

    /**
     * Generates a TreeMap of threeMlPrice and start/end times for each putup
     *
     * @param threeMlPrice Base threeMlPrice to create map from
     * @return Map containing all prices and start/end times for entry
     */
    public TreeMap<Double, LocalTime> generatePriceTimeMap(Double threeMlPrice, Boolean isF) {
        JsonRanges ranges = JsonRanges.loadRanges();
        TreeMap<Double, LocalTime> result = new TreeMap<>(new Comparator<Double>() {
            @Override
            public int compare(Double o1, Double o2) {
                return o1.compareTo(o2);
            }
        });

        HashMap<Integer, JsonRanges.ThreeMStage> threeMRoot = ranges.getThreeMstages();
        HashMap<AtrClassEnum, Double> threeMRootMax = ranges.getRanges().get("3m_f_max");
        Double max = threeMRootMax.get(atrClass);
        for (int i = 1; i >= -1; i--) {
            if (i != 0) {
                for (int stage = 1; stage < 4; stage++) {
                    JsonRanges.ThreeMStage threeMStage = threeMRoot.get(stage);
                    LocalTime startTime = threeMStage.getStartTime();
                    LocalTime endTime = threeMStage.getEndTime();
                    DoubleRange doubleRange = threeMStage.getRanges().get(atrClass);
                    RoundingMode roundingMode;
                    if (i == -1) {
                        roundingMode = RoundingMode.DOWN;
                    } else {
                        roundingMode = RoundingMode.UP;
                    }
                    double lower = roundTwoDP((threeMlPrice * doubleRange.getMin() * i) / 100 + threeMlPrice, roundingMode);
                    double upper = roundTwoDP((threeMlPrice * doubleRange.getMax() * i) / 100 + threeMlPrice, roundingMode);
                    //Difference in time in seconds/difference in threeMlPrice
                    Duration duration = Duration.between(startTime, endTime);
                    if (i != -1) {
                        for (double k = lower; k < upper; k = roundTwoDP(k + 0.011, RoundingMode.DOWN)) {
                            if (isF && k > roundTwoDP(threeMlPrice - (threeMlPrice * max) / 100, RoundingMode.DOWN) && putupType == PutupTypeEnum.SHORTS)
                                break;
                            result.put(k, startTime.plusSeconds((long) Math.ceil(((((k - threeMlPrice) / threeMlPrice) * 100) - doubleRange.getMin()) * 10 * duration.getSeconds())));
                        }
                    } else {
                        for (double k = lower; k > upper; k = roundTwoDP(k - 0.011, RoundingMode.UP)) {
                            if (isF && k < roundTwoDP(threeMlPrice - (threeMlPrice * max) / 100, RoundingMode.UP) && putupType == PutupTypeEnum.LONGS)
                                break;
                            result.put(k, startTime.plusSeconds((long) Math.ceil(((((threeMlPrice - k) / threeMlPrice) * 100) - doubleRange.getMin()) * 10 * duration.getSeconds())));
                        }
                    }
                }
            } else {
                JsonRanges.ThreeMStage threeMStage = threeMRoot.get(1);
                DoubleRange doubleRange = threeMStage.getRanges().get(atrClass);
                LocalTime startTime = threeMStage.getStartTime();
                double mid = threeMlPrice * doubleRange.getMin();
                double upper = roundTwoDP(threeMlPrice + (mid / 100));
                double lower = roundTwoDP(threeMlPrice - (mid / 100) + 0.01);
                for (double j = lower; j <= upper; j = roundTwoDP(j + 0.011)) {
                    result.put(j, startTime);
                }
            }
        }

        return result;
    }

    /**
     * Populates the tuple list containing TreeMap of prices and open/close times, price and f status using generatePriceTimeMap
     *
     * @param priceFMap Map of prices and f status
     * @see Putup#generatePriceTimeMap
     */
    private void populatePriceTupleList(TreeMap<Double, Boolean> priceFMap) {
        priceTimeTreeSet.addAll(priceFMap.entrySet().stream().map(pair -> new PriceTimeTree(generatePriceTimeMap(pair.getKey(), pair.getValue()), pair.getKey(), pair.getValue())).collect(Collectors.toList()));
    }

    /**
     * Return a LocalTime of the entry time for that low of the day
     *
     * @param lowOfTheDay price to get the entry time for
     * @return Time range for the given price
     * TODO: Make this work. Iterate through each possible treemap to find which "section" to use, then use the below mostly
     */
    public LocalTime getEntryTime(Double lowOfTheDay, Integer index) {
//        TreeMap<Double, LocalTime> timeMap = priceTimeTreeSet.contains(index).getFirst();
//        if (timeMap.containsKey(lowOfTheDay)) {
//            return timeMap.get(lowOfTheDay);
//        } else {
        return null;
//        }
    }

    /**
     * Accessor to test if this putup is currently going to be used when
     * monitoring starts
     *
     * @return - boolean true if this stock will be monitored, false otherwise.
     */
    public boolean isActive() {
        boolean result = false;
        lock.lock();
        try {
            result = this.active;
        } finally {
            lock.unlock();
        }
        return result;
    }

    /**
     * Accessor to set whether this stock should be monitored when monitoring
     * begins
     *
     * @param active The True / False value to set for the active flag
     */
    public void setActive(boolean active) {
        lock.lock();
        try {
            this.active = active;
            this.notifyObservers();
        } finally {
            lock.unlock();
        }
    }

    /**
     * Accessor to retrieve the stocks ticker code
     *
     * @return String being the ticker code used to identify this stock on the
     * market
     */
    public String getTickerCode() {
        //Java String is immutable object no lock needed
        return tickerCode;
    }

    /**
     * Accessor to set the stocks ticker code used to identify this stock on the
     * market
     *
     * @param tickerCode String being the new ticker code to use
     */
    public void setTickerCode(String tickerCode) {
        //Java String is immutable object no lock needed
        this.tickerCode = tickerCode;
        this.notifyObservers();
    }

    /**
     * Accessor to retrieve the AtrClass of this putup.
     *
     * @return AtrClassEnum being the AtrClass of this putup
     */
    public AtrClassEnum getAtrClass() {
        AtrClassEnum result = null;
        lock.lock();
        try {
            result = this.atrClass;
        } finally {
            lock.unlock();
        }
        return result;
    }

    /**
     * Accessor to set the AtrClass of this putup.
     *
     * @param atrClass - The new AtrClassEnum value for this putup to use
     */
    public void setAtrClass(AtrClassEnum atrClass) {
        lock.lock();
        try {
            this.atrClass = atrClass;
            this.notifyObservers();
        } finally {
            lock.unlock();
        }
    }

    /**
     * Accessor to retrieve the market on which this stock is traded
     *
     * @return the market enumeration for the market on which the stock is
     * traded
     */
    public MarketEnum getMarket() {
        MarketEnum result = null;
        lock.lock();
        try {
            result = this.market;
        } finally {
            lock.unlock();
        }
        return result;
    }

    /**
     * Accessor to set the market on which this stock is traded
     *
     * @param market the market enumeration for the market on which this stock
     *               trades
     */
    public void setMarket(MarketEnum market) {
        lock.lock();
        try {
            this.market = market;
            this.notifyObservers();
        } finally {
            lock.unlock();
        }
    }

    public TreeSet<PriceTimeTree> getPriceTimeTreeSet() {
        return this.priceTimeTreeSet;
    }

    @Override
    public void acquireObjectLock() {
        lock.lock();
    }

    @Override
    public void releaseObjectLock() {
        lock.unlock();
    }

    /**
     * Retrieves todays date (the day on which the Putup started to run)
     *
     * @return A Java calendar object encapsulating a date and time
     */
    public LocalDate getTodaysDate() {
        return todaysDate;
    }

    /**
     * Sets todays date (the day on which the Putup started to run)
     *
     * @param todaysDate A Java calendar object encapsulating a date and time
     */
    public void setTodaysDate(LocalDate todaysDate) {
        this.todaysDate = todaysDate;
        this.notifyObservers();
    }

    /**
     * Retrieves an enumeration defining the type of the putup.
     *
     * @return A PutupTypeEnum value for this putup or NULL if none is set.
     */
    public PutupTypeEnum getPutupType() {
        PutupTypeEnum result = null;
        lock.lock();
        try {
            switch (this.putupType) {
                case LONGS:
                    result = PutupTypeEnum.LONGS;
                    break;
                case SHORTS:
                    result = PutupTypeEnum.SHORTS;
                    break;
            }
        } finally {
            lock.unlock();
        }
        return result;
    }

    /**
     * Sets the enumeration defining the type of the putup.
     *
     * @param putupType A PutupTypeEnum to use for this putup
     */
    public void setPutupType(PutupTypeEnum putupType) {
        if (null != putupType) {
            lock.lock();
            try {
                this.putupType = putupType;
                this.notifyObservers();
            } finally {
                lock.unlock();
            }
        }
    }

    @Override
    public boolean registerObserver(IObserver newObserver) {
        boolean result = false;
        lock.lock();
        try {
            result = this.subjectDelegate.registerObserver(newObserver);
        } finally {
            lock.unlock();
        }
        return result;
    }

    @Override
    public boolean removeObserver(IObserver oldObserver) {
        boolean result = false;
        lock.lock();
        try {
            result = this.removeObserver(oldObserver);
        } finally {
            lock.unlock();
        }
        return result;
    }

    @Override
    public void notifyObservers() {
        lock.lock();
        try {
            this.subjectDelegate.notifyObservers();
        } finally {
            lock.unlock();
        }
    }

    /**
     * This is a blocking function that identifies the first gap in a graphs
     * data (1 sec resolution) and loads a 30 min batch of data. It is used to
     * populate a graph and have the system wait until the data is loaded
     */
    private void loadMoreGraphData(BaseGraph<AbstractGraphPoint> graph) {
        if (null != graph && 0 < graph.size()) {
            //Find first gap in 1 sec data
            AbstractGraphPoint prevPoint = graph.last();
            Iterator<AbstractGraphPoint> descIterator = graph.descendingIterator();
            AbstractGraphPoint currPoint;
            long timeDiff = 0;
            while (descIterator.hasNext()) {
                currPoint = descIterator.next();
                timeDiff = prevPoint.getTimestamp() - currPoint.getTimestamp();
                //Should always be 1000 millisecs between historic data
                if (timeDiff != 1000) {
                    //If the previous point was NOT the start of a day we have found a gap break and do the load
                    if (prevPoint.isStartOfDay()) {
                        break;
                    }
                }
                prevPoint = currPoint;
            }
            //At this point the previous point marks where we need to load data to. Now submit a request to load 30 min of data
//            LoadHistoricDataBatchTask loadTask = new LoadHistoricDataBatchTask(this, prevPoint.getCalDate());
//            HRSCallableWrapper wrapper = new HRSCallableWrapper(loadTask);
//            HistoricRequestProcessingSystem HRSys = HistoricRequestProcessingSystem.getInstance();
//            HRSys.submitRequest(wrapper);
            //Now block until complete
//            while (!loadTask.isLoadComplete()) {
//                try {
//                    Thread.sleep(2000);
//                } catch (InterruptedException ex) {
//                    Logger.getLogger(Putup.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
//            Object resultObject = wrapper.getResultObject();
//            if (resultObject instanceof LoadHistoricDataPointBatchResult) {
//                LoadHistoricDataPointBatchResult result = (LoadHistoricDataPointBatchResult) resultObject;
//                switch (result.getCallbackType()) {
//                    case HISTORICDATAFINISHED:
//                        graph.addAll(result.loadedPoints);
//                        break;
//                }
//            }
        }

    }

    /**
     * Accessor method to retrieve the RealTimeRunManager that wraps this putup while
     * a live data run is in progress.
     *
     * @return The RealTimeRunManager that wraps this Putup or NULL if one has not
     * yet been created and assigned
     */
    @Deprecated
    public RealTimeRunManager getRunManager() {
        return runManager;
    }

    /**
     * Accessor method to set the RealTimeRunManager that wraps this putup while
     * a live data run is in progress.
     *
     * @param runManager A RealTimeRunManager that wraps this object.
     */
    @Deprecated
    public void setRunManager(RealTimeRunManager runManager) {
        this.runManager = runManager;
    }

    public List<Double> getPriceList() {
        return priceTimeTreeSet.stream().map(PriceTimeTree::getPrice).collect(Collectors.toList());
    }

    public List<Boolean> getStatuses() {
        return priceTimeTreeSet.stream().map(PriceTimeTree::isF).collect(Collectors.toList());
    }
}
