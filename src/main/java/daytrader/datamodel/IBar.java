package daytrader.datamodel;

import com.ib.controller.Bar;

/**
 * I came up with this in a dream, and forgot its use in another dream
 */
public class IBar extends com.ib.controller.Bar implements com.ib.controller.ApiController.IHistoricalDataHandler {
    public IBar(long time, double high, double low, double open, double close, double wap, long volume, int count) {
        super(time, high, low, open, close, wap, volume, count);
    }

    @Override
    public void historicalData(Bar bar, boolean b) {

    }

    @Override
    public void historicalDataEnd() {

    }
}
