/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package daytrader.datamodel;

/**
 * This class encapsulates the details of an entry into the stock market.
 * Not at all complete (or used yet, feel free to replace or extend it).
 *
 * @author Roy
 */
public class EntryDetails {

    private RealTimeRunRecord runRecord;
    private Integer I;
    private boolean TEntryBool;
    private AbstractGraphPoint singleBar;
    private AbstractGraphPoint doubleBar;

    /**
     * Constructor accepts the RealTimeRunManager for a Put up
     *
     * @param newRec - The RealTimeRunManager of the put up for which the entry is to be made
     * @param newI   - Integer being the 'I' value determined for this entry
     */
    public EntryDetails(RealTimeRunRecord newRec, Integer newI) {
        this.runRecord = newRec;
        this.I = newI;
        this.TEntryBool= false;
    }

    /**
     * Accessor method to retrieve the real time run manager involved in this entry
     *
     * @return A RealTimeRunRecord object for the security that the entry relates to.
     */
    public RealTimeRunRecord getRunRecord() {
        return runRecord;
    }

    /**
     * Accessor method to retrieve the 'I' value determined by the entry rules
     *
     * @return An Integer being this entries 'I' rule.
     */
    public Integer getI() {
        return I;
    }

    /**
     *  Set the details for a TEntry
     *
     * @param singleBar - The value for the bar that the single point lies
     * @param doubleBar - The value for the bar that the double point lies
     */
    public void setTEntry(AbstractGraphPoint singleBar, AbstractGraphPoint doubleBar) {
        this.TEntryBool = true;
        this.singleBar = singleBar;
        this.doubleBar = doubleBar;
    }

    /**
     * Checks if TEntry was used to enter
     *
     * @return Returns True if TEntry was used
     */
    public boolean checkTEntry() {return this.TEntryBool;}

    /**
     * Returns the single bar information determined by the single double rule
     *
     * @return Returns an AbstractGraphPoint containing the data for the single
     */
    public AbstractGraphPoint getSingle() {return singleBar;}

    /**
     * Returns the double bar information determined by the single double rule
     *
     * @return Returns a AbstractGraphPoint containing the data for the double
     */
    public AbstractGraphPoint getDouble() {return doubleBar;}
}
