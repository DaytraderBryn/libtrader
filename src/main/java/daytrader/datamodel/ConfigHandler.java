package daytrader.datamodel;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Manages config files for the client. Includes file management extension methods.
 */
public class ConfigHandler {
    private static final Path configDir = Paths.get(System.getProperty("user.home"), "/Daytrader");
    private static final ConfigHandler instance = new ConfigHandler();
    private static ReentrantLock lock = new ReentrantLock();

    private static boolean checkConfigDirExists() {
        return Files.exists(configDir);
    }

    /**
     * Create config dir.
     */
    static void createConfigDir() {
        if (!checkConfigDirExists()) {
            try {
                Files.createDirectory(configDir);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Check file exists.
     *
     * @param fileName the file name
     * @return the boolean
     */
// TODO: Make this safer
    public static boolean checkFileExists(String fileName) {
        if (!checkConfigDirExists()) {
            createConfigDir();
            return false;
        } else {
            return Files.exists(Paths.get(String.valueOf(configDir), fileName));
        }
    }

    /**
     * Gets instance.
     *
     * @return the instance
     */
    public static ConfigHandler getInstance() {
        return instance;
    }

    private ConfigHandler() {
        if (!checkConfigDirExists()) {
            createConfigDir();
        }
    }

    /**
     * Copy file.
     *
     * @param fileName the file name
     */
    static void copyFile(String fileName) {
        copyFile(fileName, fileName);
    }

    /**
     * Copy file.
     *
     * @param fileName    the file name
     * @param newFileName the new file name
     */
    static void copyFile(String fileName, String newFileName) {
        String name = getFileName(fileName);
        String outFile = getFileName(newFileName);

        if (!checkFileExists(name)) {
            try (BufferedReader reader = new BufferedReader(new FileReader(fileName));
                 BufferedWriter writer = Files.newBufferedWriter(Paths.get(String.valueOf(configDir), outFile))) {
                lock.lock();
                reader.lines().forEach(line -> {
                    try {
                        writer.append(line);
                        writer.newLine();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
        }
    }

    /**
     * Delete file.
     *
     * @param fileName the file name
     */
    static void deleteFile(String fileName) {
        String name = getFileName(fileName);

        if (checkFileExists(name)) {
            try {
                lock.lock();
                Files.delete(Paths.get(String.valueOf(configDir), name));
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
        }
    }

    /**
     * Create file.
     *
     * @param fileName the file name
     */
    public static void createFile(String fileName) {
        String name = getFileName(fileName);

        if (!checkFileExists(name)) {
            try {
                lock.lock();
                Files.createFile(Paths.get(String.valueOf(configDir), name));
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
        }
    }

    private static String getFileName(String fileName) {
        return String.valueOf(Paths.get(System.getProperty( "os.name" ).contains( "indow" ) && (fileName.substring(0,1).equals("/") || fileName.substring(0,1).equals("/")) ? fileName.substring(1) : fileName).getFileName());
    }

    /**
     * Gets file path.
     *
     * @param fileName the file name
     * @return the file path
     */
    public static String getFilePath(String fileName) {
        return Paths.get(String.valueOf(configDir), fileName).toString();
    }
}