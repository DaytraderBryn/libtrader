/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package daytrader.datamodel;

import com.ib.controller.Bar;
import daytrader.interfaces.IGraphPoint;
import daytrader.interfaces.IRoundFunction;
import daytrader.utils.DTUtil;
import rules.NoRoundingOp;
import rules.RoundToTenthCent;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Comparator;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Base class to implement GraphPoints from.
 *
 * @author Roy
 */
public abstract class AbstractGraphPoint implements IGraphPoint {

    /**
     * Attribute to store the ID of the data request that generated this data point
     */
    protected int reqId;
    /**
     * The string representation of the time and date used by the stockbroker's server
     */
    protected LocalDateTime date;
    /**
     * Attribute to store the Open price for this data point
     */
    protected double open;
    /**
     * Attribute to store the High price for this data point
     */
    protected double high;
    /**
     * Attribute to store the low price for this data point
     */
    protected double low;
    /**
     * Attribute to store the close price for this data point
     */
    protected double close;
    /**
     * Attribute to store the volume traded at this data point
     */
    protected long volume;
    /**
     * Attribute to store the count of orders placed at this data point
     */
    protected int count;
    /**
     * Attribute to store the Weighted Average Price (WAP) for this data point
     */
    protected double WAP;
    /**
     * Attribute to store the has gaps flag for this data point
     */
    protected boolean hasGaps;

    public BarColourEnum getBarColor() {
        return barColor;
    }

    public void setBarColor(BarColourEnum barColor) {
        this.barColor = barColor;
    }

    protected BarColourEnum barColor;


    /**
     * Added a recursion cache for use in tracking the Y-Line's the point might be involved in
     */
    protected RecursionCache recursionCache;
    /**
     * As the recursion cache will be storing data (i.e. writing into a point I will use a lock to manage multi threaded interactions).
     */
    protected transient ReentrantLock lock;

    /**
     * Default Constructor, All Graph Points are required to provide a zero argument constructor
     */
    public AbstractGraphPoint() {
        orderingValue = GraphPointOrderEnum.APR;
        lock = new ReentrantLock();
    }

    protected GraphPointOrderEnum orderingValue;

    @Override
    public double getLastPrice() {
        double wap = this.getWAP();
        return wap;
    }

    @Override
    public double getOpen() {
        return this.getOpen(AbstractGraphPoint.getNoOpRounder());
    }

    @Override
    public double getOpen(IRoundFunction rounder) {
        double result = this.open;
        if (null != rounder) {
            Number value = this.open;
            result = rounder.performRounding(value).doubleValue();
        }
        return result;
    }

    @Override
    public double getHigh() {
        return this.getHigh(AbstractGraphPoint.getNoOpRounder());
    }

    @Override
    public double getHigh(IRoundFunction rounder) {
        double result = this.high;
        if (null != rounder) {
            Number value = this.high;
            result = rounder.performRounding(value).doubleValue();
        }
        return result;
    }

    @Override
    public double getLow() {
        return this.getLow(AbstractGraphPoint.getNoOpRounder());
    }

    @Override
    public double getLow(IRoundFunction rounder) {
        double result = this.low;
        if (null != rounder) {
            Number value = this.low;
            result = rounder.performRounding(value).doubleValue();
        }
        return result;
    }

    @Override
    public double getClose() {
        return this.getClose(AbstractGraphPoint.getNoOpRounder());
    }

    @Override
    public double getClose(IRoundFunction rounder) {
        double result = this.close;
        if (null != rounder) {
            Number value = this.close;
            result = rounder.performRounding(value).doubleValue();
        }
        return result;
    }

    @Override
    public double getRep(PutupTypeEnum typeEnum) {
        if (typeEnum == PutupTypeEnum.LONGS) {
            return getHigh();
        } else {
            return getLow();
        }
    }

    @Override
    public double getWAP() {
        return this.getWAP(AbstractGraphPoint.getNoOpRounder());
    }

    @Override
    public double getWAP(IRoundFunction rounder) {
        double result = this.WAP;
        if (null != rounder) {
            Number value = this.WAP;
            result = rounder.performRounding(value).doubleValue();
        }
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        //For two abstract graph points to be equal they must have the same timestamp and the same last price.
        return obj instanceof AbstractGraphPoint && this.date.compareTo(((AbstractGraphPoint) obj).date) == 0 && this.getLastPrice() == ((AbstractGraphPoint) obj).getLastPrice();
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 53 * hash + (int) (Double.doubleToLongBits(this.WAP) ^ (Double.doubleToLongBits(this.WAP) >>> 32));
        return hash;
    }

    @Override
    public Integer getOrderingValue() {
        return orderingValue.order();
    }

    @Override
    public long getMSElapsedSinceStartOfTrading() {
        long result = 0;
        Calendar exchOT = DTUtil.getExchOpeningTimeFromPoint(this);
        result = this.date.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli() - exchOT.getTimeInMillis();
        return result;
    }

    private int DefaultTimeComparison(IGraphPoint o) {
        int result = 0;
        Comparator<AbstractGraphPoint> comp = AbstractGraphPoint.TimeComparator;
        result = comp.compare(this, (AbstractGraphPoint) o);
        return result;
    }

    private static IRoundFunction getNoOpRounder() {
        return new NoRoundingOp();
    }

    private static IRoundFunction getTenthCentRounder() {
        return new RoundToTenthCent();
    }

    @Override
    public int compareTo(IGraphPoint o) {
        return this.DefaultTimeComparison(o);
    }

//    /**
//     * A Java Comparator suitable to compare graph data points based on price
//     */
//    public static Comparator<AbstractGraphPoint> PriceComparator =
//            (o1, o2) -> {
//                int result = 0;
//                if (o1.getLastPrice() < o2.getLastPrice()) {
//                    result = -1;
//                } else if (o1.getLastPrice() > o2.getLastPrice()) {
//                    result = 1;
//                }
//                //If the price is the same then make the decision based on time
//                if (0 == result) {
//                    result = o1.getDateTime().compareTo(o2.getDateTime());
//                }
//                return result;
//            };

    public static Comparator<AbstractGraphPoint> HighPriceComparator =
            (o1, o2) -> {
                int result = 0;
                if (o1.getHigh() < o2.getHigh()) {
                    result = 1;
                } else if (o1.getHigh() > o2.getHigh()) {
                    result = -1;
                }
                //If the price is the same then make the decision based on time
                if (0 == result) {
                    result = o1.getDateTime().compareTo(o2.getDateTime());
                }
                return result;
            };

    public static Comparator<AbstractGraphPoint> LowPriceComparator =
            (o1, o2) -> {
                int result = 0;
                if (o1.getLow() < o2.getLow()) {
                    result = -1;
                } else if (o1.getLow() > o2.getLow()) {
                    result = 1;
                }
                //If the price is the same then make the decision based on time
                if (0 == result) {
                    result = o1.getDateTime().compareTo(o2.getDateTime());
                }
                return result;
            };

    /**
     * A Java Comparator suitable to compare graph data points based on time. This
     * is the default comparator used to order graph data points in a time sequential
     * manner in a TreeSet.
     */
    public static Comparator<AbstractGraphPoint> TimeComparator =
            (o1, o2) -> {
                int result = o1.getDateTime().compareTo(o2.getDateTime());
                //If Time is the same order by type of point
                if (0 == result) {
                    result = o2.getOrderingValue() - o1.getOrderingValue();
                }
                return result;
            };

    /**
     * @return the reqId
     */
    public int getReqId() {
        return reqId;
    }

    /**
     * @return the volume
     */
    public long getVolume() {
        return volume;
    }

    /**
     * @return the count
     */
    public int getCount() {
        return count;
    }

    /**
     * @return the hasGaps
     */
    public boolean isHasGaps() {
        return hasGaps;
    }

    @Override
    public void setOpen(double open) {
        this.open = DTUtil.step1Rounding(open).doubleValue();
    }

    @Override
    public void setHigh(double high) {
        this.high = DTUtil.step1Rounding(high).doubleValue();
    }

    @Override
    public void setLow(double low) {
        this.low = DTUtil.step1Rounding(low).doubleValue();
    }

    @Override
    public void setClose(double close) {
        this.close = DTUtil.step1Rounding(close).doubleValue();
    }

    @Override
    public void setWAP(double WAP) {
        this.WAP = DTUtil.step1Rounding(WAP).doubleValue();
    }

    /**
     * Accessor method to quickly set the 5 standard price values for a graph data
     * point.
     *
     * @param open  - double being the new open price for this Graph Data Point
     * @param high  - double being the new high price for this Graph Data Point
     * @param low   - double being the new low price for this Graph Data Point
     * @param close - double being the new close price for this Graph Data Point
     * @param WAP   - double being the new WAP price for this Graph Data Point
     */
    protected void setValues(double open, double high, double low, double close, double WAP) {
        this.setOpen(open);
        this.setHigh(high);
        this.setLow(low);
        this.setClose(close);
        this.setWAP(WAP);
    }

    /**
     * Accessor method to quickly set the 5 standard price values for a graph data point.
     *
     * @param bar BarPoint to set values from
     */
    public void setValues(Bar bar) {
        this.setOpen(bar.open());
        this.setHigh(bar.high());
        this.setLow(bar.low());
        this.setClose(bar.close());
        this.setWAP(bar.wap());
        this.setCalDate(LocalDateTime.parse(bar.formattedTime(), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
    }

    @Override
    public void setReqId(int newId) {
        this.reqId = newId;
    }

    @Override
    public void setVolume(long newVol) {
        this.volume = newVol;
    }

    @Override
    public void setCount(int newCount) {
        this.count = newCount;
    }

    @Override
    public void setHasGaps(boolean newHasGaps) {
        this.hasGaps = newHasGaps;
    }

    public void setCalDate(LocalDateTime newTimestamp) {
        this.date = newTimestamp;
    }

    @Override
    public RecursionCache getRecursionCache() {
        RecursionCache result = null;
        lock.lock();
        try {
            if (null != this.recursionCache) {
                result = new RecursionCache(this.recursionCache);
            }
        } finally {
            lock.unlock();
        }
        return result;
    }

    @Override
    public boolean setRecursionCache(RecursionCache newCache) {
        boolean result = false;
        lock.lock();
        try {
            if (null != newCache) {
                this.recursionCache = newCache;
            }
        } finally {
            lock.unlock();
        }
        return result;
    }

    /**
     * Tests if this price time graph point represents the start of a trading day on
     * the date to which the time point relates.
     *
     * @return boolean True if the time for this graph point represents the time at which
     * the stock market opened on that trading day, False otherwise.
     */
    public boolean isStartOfDay() {
        boolean result = false;
//        Calendar exchOpeningCalendar = DTUtil.getExchOpeningCalendar(this.date);
        LocalDateTime exchangeTime = this.date;
        if (this.date.atZone(ZoneId.systemDefault()).toEpochSecond() == exchangeTime.atZone(ZoneId.systemDefault()).toEpochSecond()) {
            result = true;
        }
        return result;
    }

    @Override
    public long getTimestamp() {
        return this.date.atZone(ZoneId.systemDefault()).toInstant().toEpochMilli();
    }
}
