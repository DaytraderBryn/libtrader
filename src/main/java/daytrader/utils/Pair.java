package daytrader.utils;

import com.google.gson.TypeAdapter;
import com.google.gson.annotations.SerializedName;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;

/**
 * Created by beanemcbean on 10/06/2016.
 */
public class Pair<A, B> {
    private A first;
    private B second;

    public Pair(A first, B second) {
        super();
        this.first = first;
        this.second = second;
    }

    public A first() {
        return first;
    }

    public void first(A first) {
        this.first = first;
    }

    public B second() {
        return second;
    }

    public void second(B second) {
        this.second = second;
    }
}
