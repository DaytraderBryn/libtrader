package daytrader.utils;

import java.time.LocalTime;

public class TimeRange {
    private LocalTime start;
    private LocalTime end;

    public TimeRange(LocalTime start, LocalTime end) {
        this.start = start;
        this.end = end;
    }

    public LocalTime getEnd() {
        return end;
    }

    public LocalTime getStart() {
        return start;
    }

    public boolean contains(LocalTime time) {
        return (time.isAfter(start)) && (time.isBefore(end));
    }
}
