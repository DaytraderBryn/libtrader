package daytrader.utils;


import java.io.Serializable;
import java.util.Objects;
import java.util.function.IntFunction;

/**
 * A simple tuple class
 * TODO: Consider deletion
 *
 * @param <A> the type parameter
 * @param <B> the type parameter
 * @param <C> the type parameter
 */
public class Tuple<A, B, C> implements IntFunction, Serializable, Comparable<Tuple<A, B, C>> {
    private A first;
    private B middle;
    private C last;

    /**
     * Instantiates a new Tuple.
     *
     * @param first  the first
     * @param middle the middle
     * @param last   the last
     */
    public Tuple(A first, B middle, C last) {
        this.first = first;
        this.middle = middle;
        this.last = last;
    }

    /**
     * Gets first.
     *
     * @return the first
     */
    public A getFirst() {
        return first;
    }

    /**
     * Sets first.
     *
     * @param first the first
     */
    public void setFirst(A first) {
        this.first = first;
    }

    /**
     * Gets middle.
     *
     * @return the middle
     */
    public B getMiddle() {
        return middle;
    }

    /**
     * Sets middle.
     *
     * @param middle the middle
     */
    public void setMiddle(B middle) {
        this.middle = middle;
    }

    /**
     * Gets last.
     *
     * @return the last
     */
    public C getLast() {
        return last;
    }

    /**
     * Sets last.
     *
     * @param last the last
     */
    public void setLast(C last) {
        this.last = last;
    }

    /**
     * Equals boolean.
     *
     * @param o the o
     * @return the boolean
     */
    public boolean equals(Tuple<A, B, C> o) {
        return (this.getFirst() == o.getFirst() && this.getMiddle() == o.getMiddle() && this.getLast() == o.getLast());
    }

    @Override
    public int hashCode() {
        int result = first != null ? first.hashCode() : 0;
        result = 31 * result + (middle != null ? middle.hashCode() : 0);
        result = 31 * result + (last != null ? last.hashCode() : 0);
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Tuple tuple = (Tuple) o;

        return first != null ? first.equals(tuple.first) : tuple.first == null && (middle != null ? middle.equals(tuple.middle) : tuple.middle == null && (last != null ? last.equals(tuple.last) : tuple.last == null));
    }

    @Override
    public int compareTo(Tuple<A, B, C> o) {
        Objects.requireNonNull(o);
        if (!getClass().equals(o.getClass())) {
            throw new ClassCastException(o.getClass() + " must equal " + getClass());
        }

        for (int i = 0; i < 3; i++) {
            @SuppressWarnings("unchecked")
            Comparable<Object> l = (Comparable<Object>) apply(i);
            Object r = o.apply(i);
            int c = l.compareTo(r);
            if (c != 0) {
                return c;
            }
        }

        return 0;
    }

    @Override
    public Object apply(int value) {
        switch (value) {
            case 0:
                return first;
            case 1:
                return middle;
            case 2:
                return last;
            default:
                throw new IndexOutOfBoundsException();
        }
    }

//    public int compareTo(Tuple<A, B, C> o) {
//        int a, b;
//        if (o.first instanceof Comparable) {
//            a = ((Comparable<A>) first).compareTo(o.first);
//        } else {
//            a = ((Comparator<A>) first).compare(this.first, o.first);
//        }
//        if (a != 0) return a;
//
//        if (o.middle instanceof Comparable) {
//            b = ((Comparable<B>) middle).compareTo(o.middle);
//        } else {
//            b = ((Comparator<B>) middle).compare(this.middle, o.middle);
//        }
//        if (b != 0) return b;
//        if (o.last instanceof Comparable) {
//            return ((Comparable<C>) last).compareTo(o.last);
//        } else {
//            return ((Comparator<C>) last).compare(this.last, o.last);
//        }
//    }
}
