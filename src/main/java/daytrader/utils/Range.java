package daytrader.utils;

/**
 * Generic range type
 *
 * @param <T> the type parameter
 */
public class Range<T extends Comparable<? super T>> {
    private T high;
    private T low;

    /**
     * Instantiates a new Range.
     *
     * @param high the high
     * @param low  the low
     */
    public Range(T high, T low) {
        this.high = high;
        this.low = low;
    }

    /**
     * Check if the range contains the value passed
     *
     * @param val the val
     * @return the boolean
     */
    public boolean contains(T val) {
        return (this.high.compareTo(val) == -1) && (val.compareTo(this.low) == -1);
    }
}
