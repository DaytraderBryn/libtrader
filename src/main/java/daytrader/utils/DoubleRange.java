package daytrader.utils;

/**
 * Created by beanemcbean on 10/06/2016.
 */
public class DoubleRange {
    private Double min;
    private Double max;
    private Double range = null;

    public DoubleRange(Double min, Double max) {
        super();
        this.min = min;
        this.max = max;
    }

    public Double getRange() {
        if (range == null) {
            range = Math.abs(max-min);
        }
        return range;
    }

    public Double getMin() {
        return min;
    }

    public void setMin(Double min) {
        range = null;
        this.min = min;
    }

    public Double getMax() {
        return max;
    }

    public void setMax(Double max) {
        range = null;
        this.max = max;
    }
}
