/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package daytrader.interfaces;

import daytrader.datamodel.BarPointGraph;
import daytrader.historicRequestSystem.TWSAccount;

import java.util.List;
import java.util.concurrent.Callable;

/**
 * This interface is implemented by AbstractHDTCallable which is the base class
 * from which all Historical Data HistoricDataTask extend (i.e. the classes that load historic
 * data from the stock broker API).
 *
 * @author Roy
 */
public interface IHDTCallable extends Callable<BarPointGraph> {

    /**
     * Retrieves the account that is executing this callable
     *
     * @return A TWS Trading Account object
     */
    TWSAccount getExecutingAcc();

    /**
     * Sets the TWS Trading account that will execute this historicDataTask. Assigned by the Historic
     * Request System when it has an account that can execute this historicDataTask
     *
     * @param newAcc The TWS Account assigned to execute this historicDataTask.
     * @return Boolean True if the account was accepted and stored, False otherwise
     */
    boolean setExecutingAcc(TWSAccount newAcc);

    /**
     * Retrieves the tasks 'main' callback. This is the one defined by the person who
     * is using the historicDataTask to load historic data
     *
     * @return An ICallback interface to the users callback
     */
    ICallback getCallback();

    /**
     * Sets the tasks 'main' callback. This is the one defined by the person who
     * is using the historicDataTask to load historic data
     *
     * @param newCallback The users callback object
     * @return Boolean True if the callback was accepted and stored, False otherwise
     */
    boolean setCallback(ICallback newCallback);

    /**
     * Retrieves a list of callback's to be made by this historicDataTask when it completes
     * The first item on the list will be the users main callback, others may be added
     * by the historic data request system to control rule execution while the historicDataTask
     * is running and on completion
     *
     * @return A list of all callback's to be made when this historicDataTask completes
     */
    List<ICallback> getCallBackList();

    /**
     * Adds a new callback to the list of callback's that will be made when this historicDataTask
     * completes execution.
     *
     * @param newCallBack A callback that is to be made when the historicDataTask completes
     * @return Boolean True if the callback was accepted and stored, False otherwise
     */
    boolean addCallBack(ICallback newCallBack);

    @Override
    BarPointGraph call() throws Exception;

    /**
     * Retrieves the integer representing the unique ID number of this historicDataTask
     *
     * @return int being the ID number for this historicDataTask
     */
    int getReqId();

}
