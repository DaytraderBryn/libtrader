package daytrader.interfaces;

/**
 * Cheap trick for a named enum
 */
public interface INamedEnum {
    /**
     * Name string.
     *
     * @return the string
     */
    String name();
}
