package daytrader.interfaces;

@FunctionalInterface
public interface ITaskCallback {
    public abstract void run();
}
