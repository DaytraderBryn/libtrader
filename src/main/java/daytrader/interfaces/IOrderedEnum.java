package daytrader.interfaces;

/**
 * Cheap trick for an order enum
 */
public interface IOrderedEnum extends INamedEnum {
    int order();
}
