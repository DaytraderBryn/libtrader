package daytrader.hibernate;

import daytrader.datamodel.BarPoint;
import daytrader.datamodel.GraphPointOrderEnum;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Middleware to handle the hibernate entities
 * TODO: Consider merging all entities to their corresponding classes
 */
@Entity
@Table(name = "bars")
public class BarEntity extends BarPoint {

    private long id;
    private RequestEntity requestEntity;
    private long requestCount;

    /**
     * Instantiates a new Bar entity.
     *
     * @param date          the date
     * @param open          the open
     * @param high          the high
     * @param low           the low
     * @param close         the close
     * @param wap           the wap
     * @param requestCount  the request count
     * @param requestEntity the request entity
     */
    public BarEntity(LocalDateTime date, double open, double high, double low, double close, double wap, long requestCount, RequestEntity requestEntity) {
        this();
        this.date = date;
        this.open = open;
        this.high = high;
        this.low = low;
        this.close = close;
        this.WAP = wap;
        this.requestCount = requestCount;
        this.requestEntity = requestEntity;
    }

    /**
     * Instantiates a new Bar entity.
     *
     * @param id            the id
     * @param date          the date
     * @param open          the open
     * @param high          the high
     * @param low           the low
     * @param close         the close
     * @param wap           the wap
     * @param requestCount  the request count
     * @param requestEntity the request entity
     */
    public BarEntity(long id, LocalDateTime date, double open, double high, double low, double close, double wap, long requestCount, RequestEntity requestEntity) {
        this();
        this.id = id;
        this.date = date;
        this.open = open;
        this.high = high;
        this.low = low;
        this.close = close;
        this.WAP = wap;
        this.requestCount = requestCount;
        this.requestEntity = requestEntity;
    }

    /**
     * Instantiates a new Bar entity.
     */
    public BarEntity() {
        this.orderingValue = GraphPointOrderEnum.BARPOINT;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    @Id
    @GenericGenerator(name="seq" , strategy="increment")
    @GeneratedValue(generator="seq")
    public long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Gets request entity.
     *
     * @return the request entity
     */
    @ManyToOne
    @JoinColumn(name = "request_id")
    public RequestEntity getRequestEntity() {
        return requestEntity;
    }

    /**
     * Sets request entity.
     *
     * @param requestEntity the request entity
     */
    public void setRequestEntity(RequestEntity requestEntity) {
        this.requestEntity = requestEntity;
    }

    /**
     * Gets request count.
     *
     * @return the request count
     */
    @Column(name = "request_count")
    public long getRequestCount() {
        return requestCount;
    }

    /**
     * Sets request count.
     *
     * @param requestCount the request count
     */
    public void setRequestCount(long requestCount) {
        this.requestCount = requestCount;
    }

    /**
     * Gets date and time.
     *
     * @return the date and time
     */
    @Column(name = "date")
    public String getDateAndTime() {
        return this.date.toString();
    }

    /**
     * Sets date and time.
     *
     * @param date the date
     */
    public void setDateAndTime(String date) {
        this.date = LocalDateTime.parse(date);
    }

    /**
     * Gets the open.
     *
     * @return the open
     */
    @Column(name = "open")
    public double getOpen() {
        return this.open;
    }

    /**
     * Sets the open.
     *
     * @param open the open
     */
    public void setOpen(double open) {
        this.open = open;
    }

    /**
     * Gets the close.
     *
     * @return the close
     */
    @Column(name = "close")
    public double getClose() {
        return this.close;
    }

    /**
     * Sets the close.
     *
     * @param close the close
     */
    public void setClose(double close) {
        this.close = close;
    }

    /**
     * Gets the high.
     *
     * @return the high
     */
    @Column(name = "high")
    public double getHigh() {
        return this.high;
    }

    /**
     * Sets the high.
     *
     * @param high the high
     */
    public void setHigh(double high) {
        this.high = high;
    }

    /**
     * Gets the low.
     *
     * @return the low
     */
    @Column(name = "low")
    public double getLow() {
        return this.low;
    }

    /**
     * Sets the low.
     *
     * @param low the low
     */
    public void setLow(double low) {
        this.low = low;
    }

    /**
     * Gets wap.
     *
     * @return the wap
     */
    @Column(name = "wap")
    public double getWap() {
        return this.WAP;
    }

    /**
     * Sets wap.
     *
     * @param wap the wap
     */
    public void setWap(double wap) {
        this.WAP = wap;
    }

    /**
     * Gets ordering.
     *
     * @return the ordering
     */
    @Column(name = "ordering_value")
    public String getOrdering() {
        return this.orderingValue.toString();
    }

    /**
     * Sets ordering.
     *
     * @param ordering the ordering
     */
    public void setOrdering(String ordering) {
        this.orderingValue = GraphPointOrderEnum.valueOf(ordering);
    }
}
