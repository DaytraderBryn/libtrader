package daytrader.hibernate;

import com.ib.client.Contract;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * Middleware to handle the hibernate entities
 * TODO: Consider merging all entities to their corresponding classes
 */
@Entity
@Table(name = "contracts")
public class ContractEntity extends Contract {

    private long id;

    /**
     * Instantiates a new Contract entity.
     *
     * @param symbol          the symbol
     * @param sec_type        the sec type
     * @param strike          the strike
     * @param exchange        the exchange
     * @param include_expired the include expired
     */
    public ContractEntity(String symbol, String sec_type, double strike, String exchange, boolean include_expired) {
        super();
        exchange(exchange);
        symbol(symbol);
        secType(sec_type);
        strike(strike);
        includeExpired(include_expired);
    }

    /**
     * Instantiates a new Contract entity.
     */
    public ContractEntity() {
    }

    /**
     * Instantiates a new Contract entity.
     *
     * @param contract the contract
     */
    public ContractEntity(Contract contract) {
        super();
        exchange(contract.exchange());
        symbol(contract.symbol());
        secType(contract.secType());
        strike(contract.strike());
        includeExpired(contract.includeExpired());
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    @Id
    @GenericGenerator(name="seq" , strategy="increment")
    @GeneratedValue(generator="seq")
    public long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Gets exchange.
     *
     * @return the exchange
     */
    @Column(name = "exchange")
    public String getExchange() {
        return exchange();
    }

    /**
     * Sets exchange.
     *
     * @param exchange the exchange
     */
    public void setExchange(String exchange) {
        exchange(exchange);
    }

    /**
     * Gets symbol.
     *
     * @return the symbol
     */
    @Column(name = "symbol")
    public String getSymbol() {
        return symbol();
    }

    /**
     * Sets symbol.
     *
     * @param symbol the symbol
     */
    public void setSymbol(String symbol) {
        symbol(symbol);
    }

    @Column(name = "sec_type")
    public String getSecType() {
        return secType().toString();
    }

    /**
     * Sets sec type.
     *
     * @param secType the sec type
     */
    public void setSecType(String secType) {
        secType(secType);
    }

    /**
     * Gets strike.
     *
     * @return the strike
     */
    @Column(name = "strike")
    public double getStrike() {
        return strike();
    }

    /**
     * Sets strike.
     *
     * @param strike the strike
     */
    public void setStrike(double strike) {
        strike(strike);
    }

    /**
     * Gets include expired.
     *
     * @return the include expired
     */
    @Column(name = "include_expired")
    public boolean getIncludeExpired() {
        return includeExpired();
    }

    /**
     * Sets include expired.
     *
     * @param includeExpired the include expired
     */
    public void setIncludeExpired(boolean includeExpired) {
        includeExpired(includeExpired);
    }

    /**
     * To contract contract.
     *
     * @return the contract
     */
    public Contract toContract() {
        Contract contract = new Contract();
        contract.includeExpired(includeExpired());
        contract.strike(strike());
        contract.secType(secType());
        contract.exchange(exchange());
        contract.symbol(symbol());
        contract.localSymbol(symbol());
        contract.primaryExch(exchange());
        return contract;
    }
}
