package daytrader.hibernate;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Middleware to handle the hibernate entities
 * TODO: Consider merging all entities to their corresponding classes
 */
@Entity
@Table(name = "requests")
public class RequestEntity {
    private long id;
    private ContractEntity contractEntity;
    private LocalDateTime startDate;
    private LocalDateTime endDate;

    private List<BarEntity> points;

    /**
     * Instantiates a new Request entity.
     *
     * @param contractEntity the contract entity
     * @param startDate      the start date
     * @param endDate        the end date
     * @param points         the points
     */
    public RequestEntity(ContractEntity contractEntity, LocalDateTime startDate, LocalDateTime endDate, List<BarEntity> points) {
        this.contractEntity = contractEntity;
        this.startDate = startDate;
        this.endDate = endDate;
        this.points = points;
    }

    /**
     * Instantiates a new Request entity.
     */
    public RequestEntity() {
    }

    /**
     * Instantiates a new Request entity.
     *
     * @param contractEntity the contract entity
     * @param startDate      the start date
     * @param endDate        the end date
     */
    public RequestEntity(ContractEntity contractEntity, LocalDateTime startDate, LocalDateTime endDate) {
        this.contractEntity = contractEntity;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    @Id
    @GenericGenerator(name="seq" , strategy="increment")
    @GeneratedValue(generator="seq")
    public long getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Gets contract entity.
     *
     * @return the contract entity
     */
    @ManyToOne
    @JoinColumn(name = "contract_id")
    public ContractEntity getContractEntity() {
        return contractEntity;
    }

    /**
     * Sets contract entity.
     *
     * @param contractEntity the contract entity
     */
    public void setContractEntity(ContractEntity contractEntity) {
        this.contractEntity = contractEntity;
    }

    /**
     * Gets start date.
     *
     * @return the start date
     */
    @Column(name = "start_date")
    public String getStartDate() {
        return startDate.toString();
    }

    /**
     * Sets start date.
     *
     * @param startDate the start date
     */
    public void setStartDate(String startDate) {
        this.startDate = LocalDateTime.parse(startDate);
    }

    /**
     * Gets end date.
     *
     * @return the end date
     */
    @Column(name = "end_date")
    public String getEndDate() {
        return endDate.toString();
    }

    /**
     * Sets end date.
     *
     * @param endDate the end date
     */
    public void setEndDate(String endDate) {
        this.endDate = LocalDateTime.parse(endDate);
    }

    /**
     * Gets points.
     *
     * @return the points
     */
    @OneToMany
    @JoinColumn(name = "request_id")
    public List<BarEntity> getPoints() {
        return points;
    }

    /**
     * Sets points.
     *
     * @param points the points
     */
    public void setPoints(List<BarEntity> points) {
        this.points = points;
    }
}
