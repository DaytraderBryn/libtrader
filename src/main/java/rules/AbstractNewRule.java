package rules;

import daytrader.datamodel.AbstractGraphPoint;
import daytrader.datamodel.BaseGraph;
import daytrader.datamodel.CallbackType;
import daytrader.interfaces.ICallback;
import daytrader.interfaces.IRule;
import javafx.scene.text.TextFlow;

public abstract class AbstractNewRule<T extends AbstractGraphPoint> extends RuleRunner implements IRule, ICallback {

    public abstract RuleResult runPrimaryRule(BaseGraph<T> points, TextFlow flow);

    public BaseGraph<T> getPoints() {
        return points;
    }

    public void setPoints(BaseGraph<T> points) {
        this.points = points;
    }

    protected BaseGraph<T> points;

    @Override
    public void callback(CallbackType type, Object data) {
    }
}
