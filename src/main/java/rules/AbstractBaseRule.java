/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rules;

import daytrader.datamodel.CallbackType;
import daytrader.datamodel.RealTimeRunManager;
import daytrader.interfaces.ICallback;
import daytrader.interfaces.IRule;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * This provides a basic implementation of the IRule interface and the code to
 * manage sub rule collections. All rules should extend from this class
 *
 * @author Roy
 */
public abstract class AbstractBaseRule extends RuleRunner implements IRule, ICallback {

    /**
     * The RealTimeRunManager that is using this rule to test its data.
     */
    protected RealTimeRunManager owner;

    /**
     * Default constructor
     */
    protected AbstractBaseRule() {
        this.subRules = new ArrayList<IRule>();
        this.loadingMoreData = new AtomicBoolean(false);
    }

    @Override
    public boolean setRealTimeRunManager(RealTimeRunManager target) {
        boolean result = false;
        if (null != target) {
            this.owner = target;
            if (null != this.subRules && 0 < this.subRules.size()) {
                for (IRule currRule : this.subRules) {
                    currRule.setRealTimeRunManager(target);
                }
            }
        }
        return result;
    }


    @Override
    public void callback(CallbackType type, Object data) {
        this.loadingMoreData.set(false);
    }

    /**
     * A thread safe method to print a message to the console - use for debugging
     *
     * @param strMsg - String being the message to print
     */
    protected void printToConsole(String strMsg) {
        java.awt.EventQueue.invokeLater(new ConMsg(strMsg));
    }

    /**
     * Java Runnable that may be used to print a message to the console
     */
    protected class ConMsg implements Runnable {

        private String strMsg;

        /**
         * Constructor that accepts the message to print to the console
         *
         * @param strNewMsg - String containing a message to print.
         */
        public ConMsg(String strNewMsg) {
            this.strMsg = strNewMsg;
        }

        @Override
        public void run() {
            System.out.println(this.strMsg);
        }
    }
}
