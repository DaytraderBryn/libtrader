package rules;

import daytrader.datamodel.RealTimeRunManager;
import daytrader.historicRequestSystem.HRSCallableWrapper;
import daytrader.historicRequestSystem.HistoricRequestProcessingSystem;
import daytrader.historicRequestSystem.PriorityEnum;
import daytrader.interfaces.IHDTCallable;
import daytrader.interfaces.IRule;

import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * A middleware class to hide some of the implementation
 */
public class RuleRunner implements IRule {

    /**
     * An Array of rules that must be passed for this test to pass
     */
    protected ArrayList<IRule> subRules;

    /**
     * Thread safe boolean flag indicating that this rule is waiting on more data
     * to be loaded from the stock brokers data server.
     */
    protected AtomicBoolean loadingMoreData;

    @Override
    public boolean runRule() {
        boolean result = false;
        if (!this.loadingMoreData.get()) {
            try {
                boolean blnPrime = this.runPrimaryRule().isPassed();
                if (blnPrime) {
                    //Prime rule passed run all sub rules
                    for (IRule currRule : this.subRules) {
                        if (!currRule.runRule()) {
                            blnPrime = false;
                            break;
                        }
                    }
                    result = blnPrime;
                }
            } catch (LoadingAdditionalDataException ex) {
                this.loadingMoreData.set(true);
                result = false;
            }
        }
        return result;
    }

    @Override
    public boolean addSubRule(IRule newRule) {
        boolean result = false;
        if (null != newRule) {
            if (null == this.subRules) {
                this.subRules = new ArrayList<IRule>();
            }
            result = this.subRules.add(newRule);
//            if (result) {
//                newRule.setRealTimeRunManager(this.owner);
//            }
        }
        return result;
    }

    @Override
    public boolean removeSubRule(IRule oldRule) {
        boolean result = false;
        if (null != oldRule) {
            if (null != this.subRules && 0 < this.subRules.size()) {
                result = this.subRules.remove(oldRule);
//                if (result) {
//                    oldRule.setRealTimeRunManager(null);
//                }
            }
        }
        return result;
    }

    @Override
    public boolean setRealTimeRunManager(RealTimeRunManager target) {
        return false;
    }


    @Override
    public boolean isLoadingMoreData() {
        return this.loadingMoreData.get();
    }

    @Override
    public void requestMoreData(IHDTCallable historicRequestTask) throws LoadingAdditionalDataException {
        if (null != historicRequestTask) {
            //Add rule as an additional callback
//            historicRequestTask.addCallBack(this);
            //Submit the historicDataTask to the Historic Request System
            HistoricRequestProcessingSystem HRSys = HistoricRequestProcessingSystem.getInstance();
            HRSCallableWrapper wrapper = new HRSCallableWrapper(historicRequestTask, PriorityEnum.IMMEDIATE);
            HRSys.submitRequest(wrapper);
            //Set flag to indicate waiting for historicDataTask to complete
            this.loadingMoreData.set(true);
            //Throw the Loading Additional Data Exception to abort the current test of the rule.
            throw new LoadingAdditionalDataException("Loading more data for rule...");
        }
    }


    /**
     * This method should be implemented by the extending class. The code to run
     * the primary rule should be implement into this 'hook' method
     *
     * @return boolean True if the primary rules was passed and the sub rules should now be tested. False otherwise
     */
    public RuleResult runPrimaryRule() {
        return new RuleResult<>(false, null);
    }
}