package rules;

import daytrader.datamodel.*;
import daytrader.interfaces.IGraphLine;
import daytrader.utils.DTUtil;
import daytrader.utils.TimeRange;

import java.time.Duration;
import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * beanemcbean providing you with the fresh MeiMei's.
 */
public class GenerateCELine<T extends AbstractGraphPoint> extends AbstractBaseRule {

    private ArrayList<GraphLinePlusCEFormulaeData> ceFormulaDataCache;
//    private ArrayList<PointsCEFormulaData> addedPoints = new ArrayList<>();
    private BaseGraph<T> graph;


//    public ArrayList<IGraphLine> generateCELine(BaseGraph<T> graph, List<PointsCEFormulaData> addedPoints) throws LoadingAdditionalDataException {
//        this.addedPoints.addAll(addedPoints);
//        return generateCELine(graph);
//    }

    public ArrayList<IGraphLine> generateCELine(BaseGraph<T> graph, LocalTime startPoint) throws LoadingAdditionalDataException {
        ArrayList<IGraphLine> result = new ArrayList<>();
        this.graph = graph;
        IdentifyPB3Points<T> rule = new IdentifyPB3Points<>();
        List<PointsCEFormulaData> pointList = rule.performBPOperationOnData(graph);
        if (startPoint != null) {
            for (Iterator<PointsCEFormulaData> iter = pointList.listIterator(); iter.hasNext(); ) {
                PointsCEFormulaData point = iter.next();
                if (point.getPoint().getTime().isBefore(startPoint)) {
                    iter.remove();
                }
            }
        }
        //The point list now contains EVERY PB3 POINT UP TO THE LOW OF THE DAY + THE 'FIRST' point of the day however
        //For CE calc eliminate all points before the LATEST high of the day
        List<PointsCEFormulaData> revisedPointList = new ArrayList<>();
        TimeRange timeRange = new TimeRange(graph.getHighestPointSoFar().getTime(), graph.getLowestPointSoFar().getTime());
        revisedPointList.addAll(pointList.stream().filter(currPoint -> timeRange.contains(currPoint.getPoint().getTime())).collect(Collectors.toList()));
        //We now have a list of points between the latest high and latest low all but the first are valid points the first must
        //be verified possibly against previous days data

        if (0 < pointList.size()) {
            T pointOne = (T) pointList.get(pointList.size() - 1).getPoint();
            if (!validateFirstPoint(pointList, graph)) {
                System.out.println("Point one is not an SD");
            }
        }

        GraphLine ce;
        ArrayList<IGraphLine> tempStore;
        switch (pointList.size()) {
            case 0:
                //There is NO CE LINE.
                break;
            case 1:
                //We have only 1 point. Line is previous days close to the single point
                T prevDayClose = graph.getPrevDayClose();
                T ePoint = (T) pointList.get(0).getPoint();
                ce = new GraphLine(prevDayClose, ePoint, graph, graph.getPutup().getPutupType());
                TreeSet<Integer> tradeDay = new TreeSet<>();
                tradeDay.add(prevDayClose.getDay());
                ce.setTradingDays(tradeDay);
                tempStore = new ArrayList<>();
                tempStore.add(ce);
                break;
            case 2:
                //We have only 2 points these define the only possible C-E Line
                T firstPoint = (T) pointList.get(0).getPoint();
                T secPoint = (T) pointList.get(1).getPoint();
                ce = new GraphLine(firstPoint, secPoint, graph, graph.getPutup().getPutupType());
                tempStore = new ArrayList<>();
                tempStore.add(ce);
                break;
            default:
                //We have at least 3 points
                tempStore = generateLines(pointList, graph.getPutup().getPutupType());
                result = tempStore;
        }

        //Final sanity check ensure their are no 'Horizonal' C-E Lines
        if (0 < result.size()) {
            List<IGraphLine> tempList = new LinkedList<>(result);
            for (IGraphLine currLine : tempList) {
                if (0 == currLine.getGradient()) {
                    result.remove(currLine);
                }
            }
        }
        //Store the final C-E Lines in the result
        return result;
    }

    private ArrayList<IGraphLine> generateLines(List<PointsCEFormulaData> points, PutupTypeEnum typeEnum) throws LoadingAdditionalDataException {
        ArrayList<IGraphLine> result = new ArrayList<>();
        ArrayList<GraphLinePlusCEFormulaeData> tempStore = new ArrayList<>();
        if (2 <= points.size()) {                      //Must have 2 points minimum for a line
            //if we have more than one result use formula to play off the CE Lines
            //Now find the record with the highest y score, THEN find the record with the highest x score
            TreeSet<PointsCEFormulaData> PBValOrder = new TreeSet<>(points);                              //This is the points data sorted by PBValue (Y per Bryn).
            PointsCEFormulaData highestY = PBValOrder.last();
            TreeSet<PointsCEFormulaData> durationOrder = new TreeSet<>(PointsCEFormulaData.DurationComparator);  //This is the points data sorted by DURATION since earliest low (X per Bryn)
            durationOrder.addAll(points);
            PointsCEFormulaData highestX = durationOrder.last();
            PointsCEFormulaData provSD = null;
            //Highest PBValue is the same object as highest duration? If so this is the provisional SD
            if (highestY.equals(highestX)) {
                //if a single point has both the greatest y (pb) and the greatest x (duration) then it is the provisional sd
                provSD = highestY;
            }
            boolean noSD = false;


            //Is their a provisional SD
            if (null != provSD) {
                //I have a provisional SD
                //does the provisional SD include the validated point
                AbstractGraphPoint highPoint;
                if (typeEnum == PutupTypeEnum.LONGS) {
                    highPoint = points.get(0).getPoint();
                } else {
                    highPoint = points.get(points.size()-1).getPoint();
                }
                AbstractGraphPoint pointSD = provSD.getPoint();
                //Check in test that equals
                if (highPoint.equals(pointSD)) {
                    //SD and "High point" are the same YES
                    provSD.setFirstPoint(true);
                    //The SD is also the "High of the day", so we know it is the C point
                    //So we only need to create lines for the right side
                    tempStore.add(applyStandIn(points.subList(points.indexOf(provSD), points.size()), provSD, typeEnum));
                } else {
                    //SD is NOT the "High point"  NO
                    provSD.setFirstPoint(true);
                    //The SD is not the high of the day, so it could be either C or E
                    //So we have to create lines for both the left side and right side
                    GraphLinePlusCEFormulaeData standInLine = applyStandIn(points.subList(points.indexOf(provSD), points.size()), provSD, typeEnum);
                    tempStore.add(standInLine);
                    standInLine = applyReverseStandIn(points.subList(0, points.indexOf(provSD) + 1), provSD, standInLine, typeEnum);
                    if (standInLine != null) {
                        tempStore.add(standInLine);
                    }
                }
            } else {
                //I do not have a provisional SD
                if (typeEnum == PutupTypeEnum.LONGS) {
                    points.sort(PointsCEFormulaData.HighComparator.reversed());
                } else {
                    points.sort(PointsCEFormulaData.HighComparator);
                }
                //We have to assume that every point could be the C or E
                //So go though each point, calculate left hand side and right hand side
                for (PointsCEFormulaData graphPoint : points) {
                    GraphLinePlusCEFormulaeData standInLine;
                    final List subPoints = new ArrayList(points.subList(points.indexOf(graphPoint), points.size() - 1));
                    if (subPoints.size() > 1) {
                        standInLine = applyStandIn(subPoints, graphPoint, typeEnum);
                        tempStore.add(standInLine);
                    } else {
                        standInLine = null;
                    }
                    final List reverseSubPoints = new ArrayList(points.subList(0, points.indexOf(graphPoint)+1));
                    if (reverseSubPoints.size() > 1) {
                        GraphLinePlusCEFormulaeData reverseStandIn = applyReverseStandIn(reverseSubPoints, graphPoint, standInLine, typeEnum);
                        if (reverseStandIn != null) {
                            tempStore.add(reverseStandIn);
                        }
                    }
                }
                noSD = true;
            }

            //The PBValOrder list and durationOrder may be out of order create a final list
            TreeSet<PointsCEFormulaData> finalPointList = new TreeSet<>();
            finalPointList.addAll(PBValOrder);


            //We may need to know all the C-E Lines AND their C-E Formula data at a later stage
            //In the name of sanity merge the two and cache the result
            ArrayList<GraphLinePlusCEFormulaeData> dataToCache = new ArrayList<>();
            for (GraphLinePlusCEFormulaeData currLine : tempStore) {
                GraphLinePlusCEFormulaeData aLine = new GraphLinePlusCEFormulaeData(currLine.getCPointData(), currLine.getEPointData(), graph, graph.getPutup().getPutupType());
                dataToCache.add(aLine);
            }
            this.ceFormulaDataCache = dataToCache;

            ArrayList<GraphLinePlusCEFormulaeData> potentialCELines = new ArrayList<>(dataToCache);
            //Apply "the formula" to the lines, this removes lines that do not "win" on y (pb value) or x (duration)
            cullTheWeak(potentialCELines);
            //We are looking for the line with the highest price at the low of the day
            //with SD values they share a point, so you can just use the gradient
            //but with no sd you have to calculate the price at the low of the day for each line
            if (noSD) {
                potentialCELines.sort(GraphLinePlusCEFormulaeData.LotdComparitor);
            } else {
                potentialCELines.sort(GraphLinePlusCEFormulaeData.GradientComparitor);
            }
            tempStore.clear();
            tempStore.add(potentialCELines.get(0));
        }
        if (tempStore.size() == 1) {
            result.addAll(tempStore);
        }
        return result;
    }

    private boolean validateFirstPoint(List<PointsCEFormulaData> pointList, BaseGraph<T> graphData) throws LoadingAdditionalDataException {
        boolean blnWasValidated = false;
        PutupTypeEnum typeEnum = graphData.getPutup().getPutupType();
        pointList.sort(PointsCEFormulaData.TimeComparator);
        PointsCEFormulaData firstPoint = pointList.get(0);
        TreeSet<PointsCEFormulaData> PBValOrder = new TreeSet<>(pointList);                              //This is the points data sorted by PBValue (Y per Bryn).
        PointsCEFormulaData highestY = PBValOrder.last();
        TreeSet<PointsCEFormulaData> durationOrder = new TreeSet<>(PointsCEFormulaData.DurationComparator);  //This is the points data sorted by TIME since earliest low (X per Bryn)
        durationOrder.addAll(pointList);
        PointsCEFormulaData highestX = durationOrder.last();
        Duration displacement = Duration.ofHours(24).minus(Duration.between(DTUtil.getExchOpeningLocalTime(), DTUtil.getExchClosingLocalTime()));
        Duration difference = Duration.between(graphData.getPrevDayClose().getDateTime(), firstPoint.getPoint().getDateTime());
        while (Duration.ofHours(24).compareTo(difference.minus(displacement)) < 0) {
            displacement = displacement.plusHours(24);
        }
        boolean needX = false;
        boolean needY = false;
        if (highestX != firstPoint && highestY != firstPoint) {
            needX = true;
            needY = true;
        } else if (highestX != firstPoint) {
            needY = true;
        } else if (highestY != firstPoint) {
            needX = true;
        }
        if (null != pointList && null != graphData) {
            if (0 < pointList.size() && 0 < graphData.size()) {
                //First check use close of previous day to try and validate first point as a PB3
                T prevDayClose = graphData.getPrevDayClose();
                if (null != prevDayClose) {
                    if (typeEnum == PutupTypeEnum.LONGS) {
                        if (prevDayClose.getHigh() > firstPoint.getPoint().getHigh()) {
                            //CANNOT be a PB3 remove from list and mark as validated
                            blnWasValidated = true;
                        }
                    } else {
                        if (prevDayClose.getLow() < firstPoint.getPoint().getLow()) {
                            //CANNOT be a PB3 remove from list and mark as validated
                            blnWasValidated = true;
                        }
                    }
                }
                if (!blnWasValidated) {
                    //The close of previous day failed to validate the point now try to use the first min
                    //Of cached data from loading up the close of the previous day
                    BaseGraph<T> graph60Secs = graphData.getGraphClosePrevDayData();
                    if (null != graph60Secs && 0 < graph60Secs.size()) {
                        T currPoint;
                        Iterator<T> descIter = graph60Secs.descendingIterator();
                        while (descIter.hasNext()) {
                            currPoint = descIter.next();
                            if (currPoint != prevDayClose) {
                                if (typeEnum == PutupTypeEnum.LONGS && currPoint.getHigh() > firstPoint.getPoint().getHigh() ||
                                    typeEnum == PutupTypeEnum.SHORTS && currPoint.getLow() < firstPoint.getPoint().getLow()) {
                                    blnWasValidated = true;
                                    break;
                                } else {
                                    Double priceDiff;
                                    if (typeEnum == PutupTypeEnum.LONGS) {
                                        priceDiff = firstPoint.getPoint().getHigh() * 1000 - currPoint.getHigh() * 1000;
                                    } else {
                                        priceDiff = firstPoint.getPoint().getLow() * 1000 - currPoint.getLow() * 1000;
                                        priceDiff *= -1;
                                    }
                                    if (needX) {
                                        if (priceDiff >= firstPoint.getPb3Value()) {
                                            firstPoint.setPb3Value(priceDiff);
                                            if (priceDiff >= highestX.getPb3Value()) {
                                                needX = false;
                                            }
                                        }
                                    }
                                    if (needY) {
                                        Duration currDuration = Duration.between(currPoint.getDateTime(), firstPoint.getPoint().getDateTime()).minus(displacement);
                                        if (currDuration.compareTo(firstPoint.getDuration()) > 0) {
                                            firstPoint.setDuration(currDuration);
                                            if (currDuration.compareTo(highestX.getDuration()) > 0) {
                                                needY = false;
                                            }
                                        }
                                    }
                                    if (!needX && !needY) {
                                        pointList.set(0, firstPoint);
                                        blnWasValidated = true;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
                if (!blnWasValidated) {
                    //If we get this far the first min of previous days data did not validate point (or is not availiable)
                    //Now we must use previous days data look and see if it is avaliable
                    BaseGraph<T> prevDayGraph = this.graph.getPrevDayGraph();
                    if (null != prevDayGraph) {
                        //We may proceed to do the check as the data is avaliable
                        //Do Validation
                        Iterator<T> descIter = prevDayGraph.descendingIterator();
                        T currPoint;
                        while (descIter.hasNext()) {
                            currPoint = descIter.next();
                            if (currPoint != prevDayClose) {
                                if (typeEnum == PutupTypeEnum.LONGS && currPoint.getHigh() > firstPoint.getPoint().getHigh() ||
                                        typeEnum == PutupTypeEnum.SHORTS && currPoint.getLow() < firstPoint.getPoint().getLow()) {
                                    blnWasValidated = true;
                                    break;
                                } else {
                                    Double priceDiff;
                                    if (typeEnum == PutupTypeEnum.LONGS) {
                                        priceDiff = firstPoint.getPoint().getHigh() * 1000 - currPoint.getHigh() * 1000;
                                    } else {
                                        priceDiff = firstPoint.getPoint().getLow() * 1000 - currPoint.getLow() * 1000;
                                        priceDiff *= -1;
                                    }
                                    if (needX) {
                                        if (priceDiff >= firstPoint.getPb3Value()) {
                                            firstPoint.setPb3Value(priceDiff);
                                            if (priceDiff >= highestX.getPb3Value()) {
                                                needX = false;
                                            }
                                        }
                                    }
                                    if (needY) {
                                        Duration currDuration = Duration.between(currPoint.getDateTime(), firstPoint.getPoint().getDateTime()).minus(displacement);
                                        if (currDuration.compareTo(firstPoint.getDuration()) > 0) {
                                            firstPoint.setDuration(currDuration);
                                            if (currDuration.compareTo(highestX.getDuration()) > 0) {
                                                needY = false;
                                            }
                                        }
                                    }
                                    if (!needX && !needY) {
                                        pointList.set(0, firstPoint);
                                        blnWasValidated = true;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return blnWasValidated;
    }

    private GraphLinePlusCEFormulaeData applyStandIn(List<PointsCEFormulaData> formulaData, PointsCEFormulaData targetC, PutupTypeEnum typeEnum) {
        boolean valid = false;
        int cIndex;
        GraphLinePlusCEFormulaeData standInLine = null;
        formulaData.sort(PointsCEFormulaData.TimeComparator);
        cIndex = formulaData.indexOf(targetC);
        PointsCEFormulaData cPoint = new PointsCEFormulaData(targetC.getPoint(), targetC.getPb3Value(), targetC.getDuration());
        while (!valid) {
            formulaData.sort(PointsCEFormulaData.TimeComparator);
            //Get the List from C to the LOTD
            List<PointsCEFormulaData> filteredFormulaData = new ArrayList<>();
            filteredFormulaData.addAll(formulaData.subList(cIndex + 1, formulaData.size()));
            //Calculate the possible lines from C (SD)
            ArrayList<GraphLinePlusCEFormulaeData> CELineClump = filteredFormulaData.stream().map(
                    pointE -> new GraphLinePlusCEFormulaeData(cPoint, pointE, graph, typeEnum)).collect(Collectors.toCollection(ArrayList::new));
            if (CELineClump.size() > 0) {
                //Sorts list by gradient
                if (typeEnum == PutupTypeEnum.LONGS) {
                    CELineClump.sort(GraphLinePlusCEFormulaeData.GradientComparitor);
                } else {
                    CELineClump.sort(GraphLinePlusCEFormulaeData.GradientComparitor.reversed());
                }
                //Gets the line with the lowest gradient (the most shallow line)
                standInLine = CELineClump.get(0);
                int eIndex = formulaData.indexOf(standInLine.getEPointData());
                //For the lowest gradient line, find the stand in value for that point
                List<PointsCEFormulaData> eList = formulaData.subList(cIndex + 1, eIndex + 1);
                eList.sort(PointsCEFormulaData.PBValueComparator.reversed());
                double highestY = eList.get(0).getYScore();
                eList.sort(PointsCEFormulaData.DurationComparator.reversed());
                Duration highestX = eList.get(0).getXScore();
                //Set the standIn for this line
                PointsCEFormulaData ePoint = new PointsCEFormulaData(standInLine.getCurrentE(), highestY, highestX);
                standInLine.standInEPointData(ePoint);
                List<PointsCEFormulaData> eTwoList = formulaData.subList(eIndex + 1, formulaData.size());
                if (eTwoList.size() > 2) {
                    eTwoList.sort(PointsCEFormulaData.PBValueComparator.reversed());
                    highestY = eTwoList.get(0).getYScore();
                    eTwoList.sort(PointsCEFormulaData.DurationComparator.reversed());
                    highestX = eTwoList.get(0).getXScore();
                    //If there is a better candidate further past in the list then start again with that point, otherwise end.
                    if (ePoint.getXScore().compareTo(highestX) >= 0 || ePoint.getYScore() >= highestY) {
                        valid = true;
                    } else {
                        cIndex = eIndex;
                        cPoint.setPoint(ePoint.getPoint());
                    }
                } else {
                    break;
                }
            } else {
                break;
            }
        }
        return standInLine;
    }

    private GraphLinePlusCEFormulaeData applyReverseStandIn(List<PointsCEFormulaData> formulaData, PointsCEFormulaData targetE, GraphLinePlusCEFormulaeData rightLine, PutupTypeEnum typeEnum) {
        boolean valid = false;
        PointsCEFormulaData ePoint = targetE.carbonCopy();
        GraphLinePlusCEFormulaeData standInLine = null;
        //Sort the points to time, to make sure they are in the correct order.
        formulaData.sort(PointsCEFormulaData.TimeComparator);
        //When there are only 2 points passed in use them to make the line
        if (formulaData.size() == 2) {
            standInLine = new GraphLinePlusCEFormulaeData(formulaData.get(0), formulaData.get(1), graph, typeEnum);
            valid = true;
        }
        while (!valid) {
            List<PointsCEFormulaData> filterdFormulaData = new ArrayList<>();
            //Create a sublist from the beginning of the data, to the current point E
            filterdFormulaData.addAll(formulaData.subList(0, formulaData.indexOf(ePoint)));
            PointsCEFormulaData finalEPoint = ePoint;
            //Create a descending iterator so we go though the list of points back through time
            ListIterator<PointsCEFormulaData> descIter = filterdFormulaData.listIterator(filterdFormulaData.size());
            Stack<GraphLinePlusCEFormulaeData> CELines = null;
            while (descIter.hasPrevious()) {
                PointsCEFormulaData cPoint = descIter.previous();
                //If there are no CELines crated yet, create one with the first set we find
                if (CELines == null) {
                    GraphLinePlusCEFormulaeData ceLine = new GraphLinePlusCEFormulaeData(cPoint, finalEPoint, graph, typeEnum);
                    CELines = new Stack<>();
                    CELines.push(ceLine);
                } else if (typeEnum == PutupTypeEnum.LONGS && CELines.peek().getPriceAtTime(cPoint.getPoint().getTime()) > cPoint.getPoint().getHigh()||
                           typeEnum == PutupTypeEnum.SHORTS && CELines.peek().getPriceAtTime(cPoint.getPoint().getTime()) < cPoint.getPoint().getLow()) {      //If the point we are looking at is "under" the line we are looking
                    GraphLinePlusCEFormulaeData ceLine = CELines.pop();                                                     //then we take all the points in between and use the strongest x and y for our e point
                    List<PointsCEFormulaData> cList = formulaData.subList(formulaData.indexOf(cPoint), formulaData.indexOf(finalEPoint));   //// TODO: 11/08/2016 this part can be optimized, just needs some tweaking, but i dont particuallary want to break it at this point
                    if (cList.size() != 0) {
                        cList.sort(PointsCEFormulaData.PBValueComparator);
                        double highestY = cList.get(0).getYScore();
                        cList.sort(PointsCEFormulaData.DurationComparator);
                        Duration highestX = cList.get(0).getXScore();
                        PointsCEFormulaData standInCPoint = new PointsCEFormulaData(ceLine.getCurrentC(), highestY, highestX);
                        ceLine.standInCPointData(standInCPoint);
                    }
                    CELines.push(ceLine);
                }else {            //Else if the line "hits" another point, then that point becomes a C for a new line;
                    GraphLinePlusCEFormulaeData ceLine = new GraphLinePlusCEFormulaeData(cPoint, finalEPoint, graph, typeEnum);
                    CELines.push(ceLine);
                }
            }
            if (CELines == null) {
                return null;
            }
            //We then use the Line calculated from the right side equation, to make sure the gradient is greater than this line
            //if it is not greater, then we know the line will "collide" with the graph, making it invalid
            if (rightLine != null) {
                if (typeEnum == PutupTypeEnum.LONGS) {
                    CELines.removeIf(l -> l.getGradient() < rightLine.getGradient());
                } else {
                    CELines.removeIf(l -> l.getGradient() > rightLine.getGradient());
                }
            }
            if (CELines.size() > 0) {
                //We also remove lines that do not "win" on y (pb value) or x (duration)
                cullTheWeak(CELines);
                if (typeEnum == PutupTypeEnum.LONGS) {
                    CELines.sort(GraphLinePlusCEFormulaeData.GradientComparitor);
                } else {
                    CELines.sort(GraphLinePlusCEFormulaeData.GradientComparitor);
                }
                standInLine = CELines.get(0);
                List<PointsCEFormulaData> eTwoList = formulaData.subList(0, formulaData.indexOf(standInLine.getEPointData()));
                eTwoList.sort(PointsCEFormulaData.PBValueComparator);
                double highestY = eTwoList.get(0).getYScore();
                eTwoList.sort(PointsCEFormulaData.DurationComparator);
                Duration highestX = eTwoList.get(0).getXScore();
                //if there is a better candidate further past in the list then start again with that point, otherwise end.
                if (ePoint.getXScore().compareTo(highestX) >= 0 || ePoint.getYScore() >= highestY) {
                    valid = true;
                } else {
                    ePoint = standInLine.getCPointData();
                }
            } else {
                return null;
            }
        }
        return standInLine;
    }

    private void cullTheWeak(List<GraphLinePlusCEFormulaeData> lines) {
        //A line needs to have an aggregate x or y value that is greater than or equal to all other lines to "survive"
        ArrayList<GraphLinePlusCEFormulaeData> deadCELines = new ArrayList<>();
        for (GraphLinePlusCEFormulaeData currCELine : lines) {
            for (GraphLinePlusCEFormulaeData targetCELine : lines) {
                //Ensure I do not test the line against itself
                boolean survived;
                if (currCELine != targetCELine) {
                    survived = currCELine.survivesAgainst(targetCELine);
                    if (!survived) {
                        deadCELines.add(currCELine);
                        break;
                    }
                }
            }
        }
        //Now remove all 'dead' lines from the potential CE Lines
        lines.removeAll(deadCELines);
    }
}
