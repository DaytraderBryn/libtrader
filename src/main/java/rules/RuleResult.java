package rules;

import daytrader.datamodel.AbstractGraphPoint;
import daytrader.datamodel.BaseGraph;

/**
 * A generic type to handle the output of a rule
 *
 * @param <T> the type parameter
 */
public class RuleResult<T extends AbstractGraphPoint> {
    private final boolean isPassed;
    private final BaseGraph<T> points;

    /**
     * Instantiates a new Rule result.
     *
     * @param isPassed the is passed
     * @param points   the points
     */
    public RuleResult(boolean isPassed, BaseGraph<T> points) {
        this.isPassed = isPassed;
        this.points = points;
    }

    /**
     * Instantiates a new Rule result.
     *
     * @param isPassed the is passed
     */
    public RuleResult(boolean isPassed) {
        this.isPassed = isPassed;
        this.points = null;
    }

    /**
     * Is passed boolean.
     *
     * @return the boolean
     */
    public boolean isPassed() {
        return isPassed;
    }

    /**
     * Gets points.
     *
     * @return the points
     */
    public BaseGraph<T> getPoints() {
        return points;
    }
}
