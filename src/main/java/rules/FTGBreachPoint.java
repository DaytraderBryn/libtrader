/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rules;

import daytrader.datamodel.*;
import javafx.scene.text.TextFlow;

import java.time.LocalTime;
import java.util.HashMap;
import java.util.NavigableSet;

/**
 * Given a base Graph with an close point for the previous day set this
 * determines the first point at which a FTG breach occurs
 *
 * @param <T> Any Price / Time data point
 * @author Roy
 */
public class FTGBreachPoint<T extends AbstractGraphPoint> extends AbstractNewRule<T> {

    /**
     * DEBUG FLAG set to false in final production code
     */
    protected static final boolean DEBUG = false;
    /**
     * Boolean used to test if this rule is ever passed. If passed then it is passed for the rest of the day
     */
    protected boolean blnEverPassed = false;

    /**
     * This attribute will be used by the CLUE versions to subset the graph being examined so we only
     * check for a FTGBreach AFTER the given startPoint
     */
    protected AbstractGraphPoint startTime;

    /**
     * Test the provided graph to see if it can identify a point which 'breaches' the FTG rule
     * if found it returns the point.
     *
     * @param data A graph of AbstractBasePoints
     * @return The first point that breaches the FTG rule or NULL if no such point can be found
     */
    public RuleResult<T> getFTGBreachPoint(BaseGraph<T> data) {
        RuleResult<T> result = new RuleResult<>(false, null);
        if (data.getPutup().isF()) {
            return new RuleResult<>(true, null);
        }
        BaseGraph<T> points = new BaseGraph<>();
        JsonRanges ranges = JsonRanges.loadRanges();
        //Lists of percentage values for ftg and ftg override
        HashMap<AtrClassEnum, Double> ftg = ranges.getRanges().get("ftg");
        HashMap<AtrClassEnum, Double> ftgOverride = ranges.getRanges().get("ftg_override");
        //Test for FTG Breach without override
        if (0 < data.size()) {
            if (null != data.getPutup()) {
                Putup putup = data.getPutup();
                AbstractGraphPoint prevDayClose = data.getPrevDayClose();
                AtrClassEnum atrClass = putup.getAtrClass();
                if (null != prevDayClose && null != atrClass) {
                    Double closePD = prevDayClose.getClose();
                    Double ftgFraction = ftg.get(atrClass);
                    Double limit = ((closePD - data.getLowestPointSoFar().getLow()) * 100 / closePD);
                    if (ftgFraction < limit && data.last().getTime().isAfter(LocalTime.of(9, 32, 0))) {
                        points.add(data.last());
                        return new RuleResult<>(true, points);
                    }
                }
            }
            //Test for breach on FTG Override if no breach so far
            if (null != data.getPutup() && !result.isPassed()) {
                //Get lowest & highest points
                T objHigh = data.getHighestPointSoFar();
                NavigableSet<T> subSet = data.subSet(objHigh, true, data.last(), true);
                BaseGraph<T> subGraph = new BaseGraph<T>(subSet);
                T objLow = subGraph.getLowestPointSoFar();
                Putup putup = data.getPutup();
                AtrClassEnum atrClass = putup.getAtrClass();
                Double overrideFTGFraction = ftgOverride.get(atrClass);
                Double limit = (objHigh.getHigh() - objLow.getLow()) * 100 / objLow.getLow();
                if (overrideFTGFraction < limit) {
                    points.add(data.last());
                    return new RuleResult<>(true, points);
                }
            }
        }
        return result;
    }

    @Override
    public RuleResult<T> runPrimaryRule(BaseGraph<T> graph, TextFlow flow) {
        RuleResult<T> result;
        if (graph.getPutup().getPriceTimeTreeSet().first().isF()) {
            return new RuleResult<>(true, null);
        }
        result = this.getFTGBreachPoint(graph);
        if (DEBUG) {
            if (result.isPassed()) {
                System.out.println("Inside FTG Range for put up " + graph.getPutup().getTickerCode());
            } else {
                System.out.println("Outside FTG Range for put up " + graph.getPutup().getTickerCode());
            }
        }
        return result;
    }
}