/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package rules;

import daytrader.datamodel.AbstractGraphPoint;
import daytrader.datamodel.BaseGraph;
import daytrader.datamodel.PriceTimeTree;
import daytrader.datamodel.PutupTypeEnum;
import daytrader.utils.Range;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;

import java.time.LocalTime;
import java.util.List;
import java.util.TreeMap;
import java.util.stream.Collectors;

/**
 * This class tests if the low point of the day for a given graph is inside the
 * 3ML range
 *
 * @author Roy
 */
public class ThreeMLRanging<T extends AbstractGraphPoint> extends AbstractNewRule<T> {
    protected boolean outOfRange = false;
    protected RuleResult result;
    protected TextFlow debugOut = null;

    /**
     * Tests the provided data graph to determine if the most recent Price / Time
     * point falls into the 3M Range
     *
     * @param dataGraph - The data graph to test
     * @return boolean True if the graph is inside 3M Range, False otherwise.
     */
    public RuleResult isIn3MLRange(BaseGraph<T> dataGraph, TreeMap<Double, LocalTime> threeMRange) {
        BaseGraph<T> resultGraph = new BaseGraph<T>();
        result = new RuleResult(false);
        //Get 3MLPrice
        if (0 < dataGraph.size()) {
            T lastPoint = dataGraph.last();
            LocalTime entryTime = threeMRange.get(dataGraph.getLowestPointSoFar().getLow());
            debugOut.getChildren().add(new Text("Entry time for 3ml is " + entryTime.toString()));
            if (entryTime != null) {
                if (entryTime.isBefore(lastPoint.getTime())) {
                    result = new RuleResult<>(true, resultGraph);
                }
            }
        }
        return result;
    }

    @Override
    public RuleResult runPrimaryRule(BaseGraph<T> graph, TextFlow flow) {
        if (flow != debugOut) {
            debugOut = flow;
        }
        result = new RuleResult(false);
        if (null != graph) {
            for (PriceTimeTree tuple : graph.getPutup().getPriceTimeTreeSet()) {
                Double lead = null;
                if (graph.getPutup().getPutupType().equals(PutupTypeEnum.LONGS)) {
                    lead = graph.getLowestPointSoFar().getLow();
                } else if (graph.getPutup().getPutupType().equals(PutupTypeEnum.SHORTS)) {
                    lead = graph.getHighestPointSoFar().getHigh();
                }
                List<Double> keySet = tuple.getPriceTimeMap().keySet().stream().collect(Collectors.toList());
                if (new Range<>(keySet.get(0), keySet.get(keySet.size() - 1)).contains((lead)) && !outOfRange) {
                    result = this.isIn3MLRange(graph, tuple.getPriceTimeMap());
                } else {
                    if (tuple == graph.getPutup().getPriceTimeTreeSet().last()) {
                        if (lead != null) {
                            if (lead < tuple.getPriceTimeMap().firstKey()) {
                                if (graph.getPutup().getPutupType().equals(PutupTypeEnum.LONGS)) {
                                    outOfRange = true;
                                } else {
                                    break;
                                }
                            } else if (lead > tuple.getPriceTimeMap().lastKey()) {
                                if (graph.getPutup().getPutupType().equals(PutupTypeEnum.LONGS)) {
                                    break;
                                } else {
                                    outOfRange = true;
                                }
                            }
                        }
                    }
                }
            }
            if (debugOut != null) {
                if (result.isPassed()) {
                    debugOut.getChildren().add(new Text("Inside 3m Range for put up " + graph.getPutup().getTickerCode()));
                } else {
                    debugOut.getChildren().add(new Text("Outside 3m Range for put up " + graph.getPutup().getTickerCode()));
                }
            }
        }
        return result;
    }
}
