package rules;

import daytrader.datamodel.*;
import daytrader.interfaces.IGraphLine;
import daytrader.utils.DTUtil;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;

import java.time.Duration;
import java.time.LocalTime;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.NavigableSet;

import static java.lang.Math.abs;

/**
 * beanemcbean providing you with the fresh MeiMei's.
 */
public class TEntry<T extends AbstractGraphPoint> extends AbstractNewRule<T> {
    private T lowOfTheDay = null;
    private T provSingleTip = null;
    private T provDoubleTip = null;
    private priceDiff doubleMagnitude = new priceDiff();
    private T singleTip = null;
    private T earliestSingleTip = null;
    private LocalTime maxDoubleTime = DTUtil.getExchOpeningLocalTime();
    private Boolean stagesPassed = false;
    private Duration singleDuration;
    private Duration doubleDuration;
    private HashMap<LocalTime, Duration> durationTable = null;


    private int stage = 0;
    private boolean confirm = false;
    private boolean methodTwo = false;
    private int counter = 0;
    private TextFlow debugOut = null;

    private priceDiff singleMagnitude;

    public RuleResult runPrimaryRule(BaseGraph dataGraph, TextFlow flow) {
        if (debugOut != flow) {
            debugOut = flow;
        }
        generateDurationTable(dataGraph.getPutup());
        boolean returned = false;
        if (maxDoubleTime.compareTo(((BarPointGraph) dataGraph).last().getTime()) < 0) {
            stagesPassed = stages(dataGraph, null);
            if (stagesPassed) {
                IdentifyPB3Points rule = new IdentifyPB3Points();
//              todo check this all works properly
                List<PointsCEFormulaData> pointList = rule.performBPOperationOnData(dataGraph);
                PointsCEFormulaData singlePoint = pointList.get(0);
                pointList.remove(0);
                boolean magCompPassed = magComp(pointList, singlePoint, ((BarPointGraph) dataGraph).getPrevDayClose().getDateTime());
                returned = realTime(dataGraph);
            }
        }

        if (debugOut != null && returned) {
            debugOut.getChildren().add(new Text("Confirm: " + confirm + "\r\nMethod 2: " + methodTwo + "\r\nCounter: " + counter));
        }
        return new RuleResult(returned, dataGraph);
    }

    private boolean realTime(BaseGraph dataGraph) {
        return false;
    }

    private boolean stages(BaseGraph<T> dataGraph, LocalTime startTime ) {
        //Is the putup we are working with long
        boolean isLong = dataGraph.getPutup().getPutupType().equals(PutupTypeEnum.LONGS);

        //If the low of the data is not the lowest point so far, then set it too be and start over again from stage 1
        if (lowOfTheDay != dataGraph.getLowestPointSoFar() && isLong) {
            lowOfTheDay = dataGraph.getLowestPointSoFar();
            stage = 1;
        } else if (lowOfTheDay != dataGraph.getHighestPointSoFar() && !isLong) {
            lowOfTheDay = dataGraph.getHighestPointSoFar();
            stage = 1;
        }

        //If the low of the day is less than the close of the prev day then it fails
        if (isLong) {
            if (lowOfTheDay.getLow() > dataGraph.getPrevDayClose().getClose()) {
                return false;
            }
        } else {
            if (lowOfTheDay.getHigh() < dataGraph.getPrevDayClose().getClose()) {
                return false;
            }
        }

        //todo refix this
        //If we get a new high then start again from stage 1
//        if (subSet.size() > 1 && provDoubleTip != highSet.getHighestPointSoFar()) {
//            stage = 1;
//        }
        switch (stage) {
            case 1: {
                //==========Set initial height===============
                //Take a subset of data from the high to the current time
                BaseGraph negSet = new BaseGraph(dataGraph.subSet(lowOfTheDay, false, dataGraph.last(), true));

                //Calculate the fall and a provisional high
                if (isLong) {
                    provSingleTip = (T) negSet.getHighestPointSoFar();
                } else {
                    provSingleTip = (T) negSet.getLowestPointSoFar();
                }

                //Take a subset of data from the lowest point to the current time
                NavigableSet subSet = dataGraph.subSet(provSingleTip, false, dataGraph.last(), true);
                //Convert NavigableSet to a BaseGraph
                BaseGraph<T> highSet = new BaseGraph<>();
                highSet.addAll(subSet);

                if (subSet.size() > 1) {
                    if (isLong) {
                        provDoubleTip = highSet.getLowestPointSoFar();
                        doubleMagnitude.setHeight(provSingleTip.getHigh(), provDoubleTip.getLow());
                        singleMagnitude = new priceDiff(lowOfTheDay.getHigh(), provSingleTip.getLow());
                    } else {
                        provDoubleTip = highSet.getHighestPointSoFar();
                        doubleMagnitude.setHeight(provSingleTip.getLow(), provDoubleTip.getHigh());
                        singleMagnitude = new priceDiff(lowOfTheDay.getLow(), provSingleTip.getHigh());
                    }
                    stage = 2;
                    System.out.println("Stage 1");
                } else {
                    break;
                }
            }
            case 2: {
                //===========Retrace Check==============
                BaseGraph negSet = new BaseGraph(dataGraph.subSet(lowOfTheDay, false, dataGraph.last(), true));

                //Check the provisional high is greater than the close of the prev day
                if (provDoubleTip.getLow() < dataGraph.getPrevDayClose().getClose() && isLong || provDoubleTip.getHigh() > dataGraph.getPrevDayClose().getClose() && !isLong) {
                    //Check the retrace is greater than 50%
                    if (singleMagnitude.getHeight() > doubleMagnitude.getHeight() / 2) {
                        //Calculate the duration between the low and the high
                        singleDuration = Duration.between(dataGraph.getEarliestLow().getTime(), provSingleTip.getTime());
                        if (singleDuration.compareTo(durationTable.get(provSingleTip.getTime())) < 0) {
                            //set the high to the provisional high
                            singleTip = provSingleTip;
                            earliestSingleTip = (T) negSet.getEarliestHigh();
                            maxDoubleTime = earliestSingleTip.getTime().plus(durationTable.get(earliestSingleTip.getTime()));
                            if (maxDoubleTime.compareTo(((BarPointGraph) dataGraph).last().getTime()) > 0) {
                                BaseGraph doubleSet = new BaseGraph(dataGraph.subSet(singleTip, true, dataGraph.last(), true));
                                provDoubleTip = (T) doubleSet.getLowestPointSoFar();
                                if (Duration.between(earliestSingleTip.getTime(), provDoubleTip.getTime()).compareTo(durationTable.get(provDoubleTip.getTime())) < 0) {
                                    stage = 3;
                                    System.out.println("Stage 2");
                                } else {
                                    stage = 1;
                                    System.out.println("Failed double duration check");
                                }
                            }
                        } else {
                            break;
                        }
                    } else {
                        break;
                    }
                } else {
                    break;
                }
            }
            case 3: {
                //Create CE Lines
                GenerateCELine CELineRule = new GenerateCELine();
                ArrayList<IGraphLine> lines = null;

                BaseGraph lowSub = new BaseGraph(dataGraph.tailSet(lowOfTheDay));
                doubleDuration = Duration.between(lowSub.getEarliestHigh().getTime(), singleTip.getTime());
                if (doubleDuration.compareTo(durationTable.get(singleTip.getTime())) < 0) {
                    try {
                        BaseGraph subset = dataGraph.replicateGraph();
                        subset.removeAll(dataGraph);
                        subset.addAll(dataGraph.headSet(lowOfTheDay, false));
                        lines = CELineRule.generateCELine(subset, startTime);
                    } catch (LoadingAdditionalDataException e) {
                        e.printStackTrace();
                    }
                    if (lines != null && lines.size() > 0) {
                        //Get the best line and set it as the main line
                        GraphLinePlusCEFormulaeData line = (GraphLinePlusCEFormulaeData) lines.get(0);
                        Double priceAtHigh = line.getPriceAtTime(provDoubleTip.getTime());
                        //If the graph breaks through the CE line then goto Realtime
                        if (priceAtHigh < provDoubleTip.getHigh() && isLong || priceAtHigh > provDoubleTip.getLow() && !isLong) {
                            stage = 1;
                            System.out.println("Realtime");
                            return true;
                        } else {
                            //If the graph passes the CE line, but not enough to breakthrough
                            //goto stage 3 and set boolean confirm to true
                            priceAtHigh = priceAtHigh + priceAtHigh * 0.001;
                            if (priceAtHigh < provDoubleTip.getHigh() && isLong || priceAtHigh > provDoubleTip.getLow() && !isLong) {
                                confirm = true;
                                stage = 1;
                                System.out.println("Realtime");
                                return true;
                            } else {
                                //if the line has not touched, we need to run method 2
                                BaseGraph<T> doubleRange = dataGraph.replicateGraph();
                                doubleRange.removeAll(dataGraph);
                                doubleRange.addAll(dataGraph.subSet(provDoubleTip, true, dataGraph.last(), true));
                                JsonRanges ranges = JsonRanges.loadRanges();
                                Double percent = ranges.getRanges().get("Method2").get(dataGraph.getPutup().getAtrClass());
                                if (abs(doubleMagnitude.getHigh() - line.getPriceAtTime(doubleRange.getLowestPointSoFar().getTime())) / line.getPriceAtTime(doubleRange.getLowestPointSoFar().getTime()) > percent) {
                                    methodTwo = true;
                                    stage = 1;
                                    BaseGraph<T> methodTwoRange = dataGraph.replicateGraph();
                                    methodTwoRange.removeAll(dataGraph);
                                    methodTwoRange.addAll(dataGraph.subSet((T) line.getCurrentE(), true, dataGraph.last(), true));
                                    counter += 1;
                                    TEntry tEntryRule = new TEntry();
                                    return stages(dataGraph, line.getCurrentE().getTime());
                                }
                            }
                        }
                    }
                    //todo make sure this is solid
                    return true;
                }
            }
        }
        return false;
    }

    private boolean magComp(List<PointsCEFormulaData> pointsCEFormulaDataList, PointsCEFormulaData single, LocalDateTime yesterdaysClose) {
        pointsCEFormulaDataList.sort(PointsCEFormulaData.HighComparator);
        if(pointsCEFormulaDataList.get(0).getPoint().getDateTime().isBefore(yesterdaysClose))
            pointsCEFormulaDataList.remove(0);

        for(int i = 0; i < pointsCEFormulaDataList.size(); i++) {
            if(pointsCEFormulaDataList.get(i).getPoint().getTime().isBefore(LocalTime.of(9,32))){
                pointsCEFormulaDataList.remove(i);
            }
        }

        pointsCEFormulaDataList.sort(PointsCEFormulaData.PBValueComparator);
        if (single.getPb3Value() > pointsCEFormulaDataList.get(0).getPb3Value()){
            pointsCEFormulaDataList.sort(PointsCEFormulaData.DurationComparator);
            if (single.getDuration().compareTo(pointsCEFormulaDataList.get(0).getDuration()) > 0) {
                return true;
            } else {
                pointsCEFormulaDataList.sort(PointsCEFormulaData.PBValueComparator);
                if (single.getPb3Value()/pointsCEFormulaDataList.get(0).getPb3Value() >= 4/3) {
                    return true;
                } else {
                    return false;
                }
            }
        } else {
            pointsCEFormulaDataList.sort(PointsCEFormulaData.DurationComparator);
            if (single.getDuration().compareTo(pointsCEFormulaDataList.get(0).getDuration()) > 0) {
                if (single.getDuration().getSeconds()/pointsCEFormulaDataList.get(0).getDuration().getSeconds() >= 4/3) {
                    pointsCEFormulaDataList.sort(PointsCEFormulaData.PBValueComparator);
                    if (single.getPb3Value()/pointsCEFormulaDataList.get(0).getPb3Value() >= 5/6) {
                        return true;
                    } else {
                        if (single.getPb3Value()/pointsCEFormulaDataList.get(1).getPb3Value() >= 4/3) {
                            return true;
                        } else {
                            pointsCEFormulaDataList.sort(PointsCEFormulaData.DurationComparator);
                            double ratio = single.getDuration().getSeconds()/pointsCEFormulaDataList.get(0).getDuration().getSeconds();
                            if (ratio < 3) {
                                return false;
                            } else {
                                pointsCEFormulaDataList.sort(PointsCEFormulaData.PBValueComparator);
                                if (ratio < 4) {
                                    return single.getPb3Value() / pointsCEFormulaDataList.get(0).getPb3Value() >= 4/3;
                                } else {
                                    return single.getPb3Value() / pointsCEFormulaDataList.get(1).getPb3Value() >= 2/3;
                                }
                            }
                        }
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }

    private boolean generateDurationTable(Putup putup) {
        if (durationTable == null) {
            JsonRanges jsonRanges = JsonRanges.loadRanges();
            HashMap<Integer, JsonRanges.ThreeMStage> singleDoubleDurations = jsonRanges.getSingleDoubleDurations();
            HashMap<LocalTime, Duration> durationHashMap = new HashMap<>();

            for (int i = 0; i < 4; i++) {
                LocalTime currentTime = singleDoubleDurations.get(i).getStartTime();
                double difference = singleDoubleDurations.get(i).getRanges().get(putup.getAtrClass()).getRange();

                Duration duration = Duration.between(singleDoubleDurations.get(i).getStartTime(), singleDoubleDurations.get(i).getEndTime());
                int units = Math.toIntExact(duration.getSeconds()) / 5;
                Duration currentDuration = convertDoubleToDuration(singleDoubleDurations.get(i).getRanges().get(putup.getAtrClass()).getMin());
                Duration increment = convertDoubleToDuration(difference / units);
                while (currentTime.isBefore(singleDoubleDurations.get(i).getEndTime())) {
                    durationHashMap.put(currentTime, currentDuration);
                    currentTime = currentTime.plusSeconds(5);
                    currentDuration = currentDuration.plus(increment);
                }
            }

            //Debug bit
            //SortedMap<LocalTime, Duration> debugMap = new TreeMap<>(durationHashMap);
            durationTable = durationHashMap;
            return true;
        } else {
            return true;
        }
    }

    private Duration convertDoubleToDuration(double time) {
        int seconds = (int) Math.floor(time);
        int nanos = (int) Math.round((time - seconds) * 1000000000);
        return Duration.ofSeconds(seconds, nanos);
    }

    class priceDiff {
        Double high = null;
        Double low = null;
        Double height = null;

        public priceDiff() {
        }

        public priceDiff(Double high, Double low) {
            this.high = high;
            this.low = low;
        }

        public Double getHeight() {
            if (high == null || low == null) {
                return 0d;
            }
            if (height == null) {
                height = abs(high - low);
            }
            return height;
        }

        public Double getHigh() {
            return high;
        }

        public void setHeight(Double high, Double low) {
            this.high = high;
            this.low = low;
            this.height = null;
        }

        public void setHigh(Double high) {
            this.high = high;
        }
    }
}
