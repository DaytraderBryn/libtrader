package rules;

import daytrader.datamodel.*;

import java.time.Duration;
import java.util.*;

/**
 * This class provides functions primarily used in determining whether a point should
 * be included as part of a CE Line
 *
 * @param <T> - Any Price / Time data point
 * @author Roy
 */
public class IdentifyPB3Points<T extends AbstractGraphPoint> {

    /**
     * Given a graph of data this function determines all points that could
     * POTENTIALLY be a C or E point on a C-E Line (or I-J line) and returns them as a list of
     * points (in time order)
     *
     * @param data - The Base data graph to examine for potential CE Points
     * @return A List of potential C-E points from the graph
     */
    public List<PointsCEFormulaData> performBPOperationOnData(BaseGraph<T> data) {
        //The operation is to retrieve a list of potential C and E points
        //Step 1 - Identify lowest price in the data set
        List<PointsCEFormulaData> result = new ArrayList<>();
        if (null != data) {
            //Now proceed as before
            TreeSet<T> tsTimeSorted = new TreeSet<>(T.TimeComparator);
            tsTimeSorted.addAll(data);
            //First Time point of the data
            T firstPoint = tsTimeSorted.first();
            TreeSet<T> tsPriceSorted;
            boolean isShort = data.getPutup().getPutupType().equals(PutupTypeEnum.SHORTS);
            if (isShort) {
                tsPriceSorted = new TreeSet<>(T.HighPriceComparator);
            } else {
                tsPriceSorted = new TreeSet<>(T.LowPriceComparator);
            }
            tsPriceSorted.addAll(data);
            T endPoint;
            if (isShort) {
                endPoint = tsPriceSorted.last();
            } else {
                endPoint = tsPriceSorted.first();
            }
            //Limit data to the lowest point no points after this are possible
            NavigableSet<T> setToLowestPrice = tsTimeSorted.subSet(firstPoint, true, endPoint, true);
            Iterator<T> descIterator = setToLowestPrice.descendingIterator();
            Stack<T> newHigh = new Stack<>();
            newHigh.add(setToLowestPrice.last());
            while (descIterator.hasNext()) {
                T currPoint = descIterator.next();
                if (isShort) {
                    if (newHigh.peek().getLow() > currPoint.getLow()) {
                        newHigh.push(currPoint);
                        PointsCEFormulaData ceData = ceDataCheck(data, currPoint, isShort);
                        if (ceData != null) {
                            result.add(ceData);
                        }
                    }
                } else {
                    if (newHigh.peek().getHigh() < currPoint.getHigh()) {
                        newHigh.push(currPoint);
                        PointsCEFormulaData ceData = ceDataCheck(data, currPoint, isShort);
                        if (ceData != null) {
                            result.add(ceData);
                        }
                    }
                }
            }
            //Sort list on a time basis
            Collections.sort(result, PointsCEFormulaData.HighComparator);
        }
        return result;
    }

    /**
     * Test a Price / Time point to determine if its PB Value is AT LEAST DTConstants.getScaledPBVALUE() / 100d
     *
     * @param data   - the data graph to use when testing the PB Value
     * @param aPoint - A Price / Time point for which the PB Value is required
     * @return integer being the BP Value if less than DTConstants.getScaledPBVALUE() / 100d or
     * the value of DTConstants.getScaledPBVALUE() / 100d if it is greater than or equal to this value
     */
    public int findPBValue(BaseGraph<T> data, T aPoint) {
        int result = 0;
        TreeSet<T> tsTime = new TreeSet<T>(T.TimeComparator);
        Iterator<T> descIter = tsTime.descendingIterator();
        double pbLimit = DTConstants.getScaledPBVALUE() / 100d;
        while (descIter.hasNext()) {
            T currPoint = descIter.next();
            if (currPoint.getTimestamp() < aPoint.getTimestamp()) {
                if (currPoint.getHigh() > aPoint.getHigh()) {
                    //We abort cannot be a potential C/E point function will return 0 (its default)
                    break;
                } else {
                    //Is this point less than the start point by PBValue
                    Double priceDiff = aPoint.getHigh() - currPoint.getHigh();
                    if (priceDiff >= pbLimit) {
                        //Point may be used calc and return the PB
                        priceDiff *= 100;
                        result = priceDiff.intValue();
                        break;
                    }
                }
            }
        }
        if (!descIter.hasNext()) {
            //If we did not abort from the above loop then we have failed to find a point higher
            //than the one requested and no point was lower by the required PB amount.
        }
        return result;
    }

    /**
     * This function finds the PBValue without using the PBLimit value
     *
     * @param data   - A BseGraph used to determine the PB Value of a point
     * @param aPoint - The Price / Time point for which the PB Value is required
     * @return double being the calculated PB value or 0 if no value can be determined
     */
    public double getPBValue(BaseGraph<T> data, T aPoint) {
        double result = 0d;
        //Find first point in history where the price is higher than the point value
        NavigableSet<T> subSet = data.subSet(data.first(), true, aPoint, true);
        BaseGraph<T> graphToUse = new BaseGraph<T>(subSet);
        graphToUse.setPutup(data.getPutup());
        graphToUse.setPrevDayClose(data.getPrevDayClose());
        Iterator<T> descIter = graphToUse.descendingIterator();
        T currPoint;
        T firstHigher = null;
        while (descIter.hasNext()) {
            currPoint = descIter.next();
            if (currPoint != aPoint) {
                if (currPoint.getLastPrice() > aPoint.getLastPrice()) {
                    firstHigher = currPoint;
                    break;
                }
            }
        }
        if (null == firstHigher) {
            //Base the result on the lowest point of the graphToUse
            T lowestPointSoFar = graphToUse.getLowestPointSoFar();
            result = aPoint.getLastPrice() - lowestPointSoFar.getLastPrice();
        } else {

            //BaseGraph<T> graphToUse2 = new BaseGraph<T>(subSet);
            NavigableSet<T> subSet1 = data.subSet(firstHigher, true, aPoint, true);
            BaseGraph<T> graphToUse2 = new BaseGraph<T>(subSet1);
            graphToUse2.setPutup(data.getPutup());
            graphToUse2.setPrevDayClose(data.getPrevDayClose());
            T lowestPointSoFar = graphToUse2.getLowestPointSoFar();
            result = aPoint.getLastPrice() - lowestPointSoFar.getLastPrice();
        }
        return result;
    }

    private PointsCEFormulaData ceDataCheck(BaseGraph<T> data, T point, boolean isShort) {
        PointsCEFormulaData result = null;
        if (null != data && null != point) {
            //Find first point in history where the price is higher than the point value
            NavigableSet<T> subSet = data.subSet(data.first(), true, point, true);
            BaseGraph<T> graphToUse = new BaseGraph<T>(subSet);
            graphToUse.setPutup(data.getPutup());
            graphToUse.setPrevDayClose(data.getPrevDayClose());
            Iterator<T> descIter = graphToUse.descendingIterator();
            T currPoint;
            T firstHigher = null;
            while (descIter.hasNext()) {
                currPoint = descIter.next();
                if (currPoint != point) {
                    if (isShort) {
                        if (currPoint.getLow() < point.getLow()) {
                            firstHigher = currPoint;
                            break;
                        }
                    } else {
                        if (currPoint.getHigh() > point.getHigh()) {
                            firstHigher = currPoint;
                            break;
                        }
                    }
                }
            }
            if (firstHigher == null) {
                firstHigher = data.first();
            }
            //Now work out PB Value and duration
            double pbVal;
            Duration duration;
            NavigableSet<T> subSet1 = data.subSet(firstHigher, true, point, true);
            BaseGraph<T> graphToUse2 = new BaseGraph<>(subSet1);
            graphToUse2.setPutup(data.getPutup());
            graphToUse2.setPrevDayClose(data.getPrevDayClose());

            if (isShort) {
                T highestPointSoFar = graphToUse2.getHighestPointSoFar();
                pbVal = point.getLow() * 1000 - highestPointSoFar.getHigh() * 1000;
                duration  = Duration.between(point.getTime(), firstHigher.getTime()).abs();
                result = new PointsCEFormulaData(point, pbVal * -1, duration);
            } else {
                T lowestPointSoFar = graphToUse2.getLowestPointSoFar();
                pbVal = point.getHigh() * 1000 - lowestPointSoFar.getLow() * 1000;
                duration = Duration.between(point.getTime(), firstHigher.getTime()).abs();
                result = new PointsCEFormulaData(point, pbVal, duration);
            }
        }
        return result;
    }
}